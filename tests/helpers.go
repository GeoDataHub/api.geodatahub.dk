package tests

import (
	"log"
	"os"
	"path/filepath"
	"testing"
	"io/ioutil"
	"strings"
	"fmt"
	"database/sql"

	_ "github.com/lib/pq"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

// MigrateDirection used to migrate the database layout for testing - Don't use in Production!
func MigrateDirection(direction string, path string) {
	sslMode := "require"
	if os.Getenv("DISABLE_SSL_FOR_TESTING") != "" {
		sslMode = "disable"
	}

	dbURL := "postgres://"+os.Getenv("DBNONADMUSR")+":"+os.Getenv("DBNONADMPASSWORD")+"@"+os.Getenv("DBHOST")+":5432/geodatahub?sslmode="+sslMode

	schemaDir, _ := filepath.Abs(path)
	m, err := migrate.New("file://"+schemaDir, dbURL)
	if err != nil {
		log.Fatalf("Unable to connect to database with error %s", err)
		panic("No connection")
	}

	if direction == "up" {
		err = m.Up()
	} else {
		err = m.Down()
	}

	if err != nil {
		log.Printf("Could not migrate: %v", err)
		//panic("Migration failed")
	}
}

// GetFakeGeocorpUsers Returns an organization with users for testing
func GetFakeGeocorpUsers() (string, string, string) {

	db := getTestingDB()
	var orgUUID string
	db.QueryRow("SELECT id FROM Organizations WHERE name = 'Fake Geocorp'").Scan(&orgUUID)

	return "00000000-0000-0000-0000-000000000001", "00000000-0000-0000-0000-000000000002", orgUUID
}

func getTestingDB() *sql.DB {

	// Disable SSL mode in testing environment
	sslMode := "sslmode=require"
	if os.Getenv("DISABLE_SSL_FOR_TESTING") != "" {
		sslMode = "sslmode=disable"
	}

	pgsqlconn := fmt.Sprintf("host=%s port=5432 user=%s password=%s dbname=%s %s", os.Getenv("DBHOST"), os.Getenv("DBNONADMUSR"), os.Getenv("DBNONADMPASSWORD"), "geodatahub", sslMode)
	db, err := sql.Open("postgres", pgsqlconn)

	if err != nil {
		log.Print("Unable to connect to database using standard lib")
	}

	return db
}

// LoadTestData Execute the sql statements store in datafiles against the database
func LoadTestData(t *testing.T, datafiles ...string) {
	for _, file := range datafiles {
		cwd, _ := os.Getwd()
		splitPath := strings.Split(cwd, "api.geodatahub.dk")
		fullPath := filepath.Join(splitPath[0], "api.geodatahub.dk/fixtures", file)

		if len(splitPath) == 1 {
			// Try to split on something else
			splitPath = strings.Split(cwd, "GeoDataHub-API")
			fullPath = filepath.Join(splitPath[0], "GeoDataHub-API/fixtures", file)
		}

		c, ioErr := ioutil.ReadFile(fullPath)
		if ioErr != nil {
			t.Fatalf("Unable to load file %s", fullPath)
		}
		sql := string(c)
		db := getTestingDB()
		_, dberr := db.Exec(sql)
		if dberr != nil {
			t.Fatalf("Unable to load SQL statement from %s into database %s", fullPath, dberr)
		}
	}
}
