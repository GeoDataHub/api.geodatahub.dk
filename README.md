# GeoDataHub RESTful API

**This repository is part of a larger experiment to build a geoscientific data catalog using modern cloud architectures with a special focus on collecting all data types within a single system to break down silos.**

**The project is stable but not production-ready and no longer maintained in its current form. It's released to inspire others and showcase how to build such a platform. See [getgeodata](getgeodata.com) for more.**

**If you have technical questions about the design (not the installation or setup) you are welcome to contact hello@getgeodata.com**

The GeoDataHub RESTful API is a golang application that allows users to discover and modify geoscientific datasets such as borehole logs, seismic surveys, geochemical samples, and much more. The API provides endpoints for the most common operations. See a complete list of endpoints in [the documentation](https://docs.geodatahub.dk/api-specs.html).

Features,
- Geospatial search
- Filter datasets by user-defined metadata
- Metadata schemas defined using JSON-schema through the [public metadata repository](https://gitlab.com/GeoDataHub/schemas.geodatahub.dk)
- Hierichal permission system to support private, internal, and public datasets

Once the API is running read [the documentation](https://docs.getgeodata.com/Web-API/basic-operations/) to get started.

You can also use the API in combination with [the interactive web map](https://gitlab.com/GeoDataHub/map.geodatahub.dk).

## Getting started

Install the AWS cli,

```
apt-get install awscli
```

This software requires go > 1.8. To install go 1.11 use the makefile,

```bash
make go
make deps
```

To build the code run,

```bash
make
```

Start the API against an empty database,

```bash
go run main.go
```

You should now see,

```bash
2021/05/21 10:46:31 Started
2021/05/21 10:46:31 Connecting to database using standard lib
2021/05/21 10:46:31 Connected to database
2021/05/21 10:46:31 Ready to continue
....
[GIN-debug] POST   /organizations/           --> gitlab.com/api.geodatahub.dk/common.CreateOrganizationRoute (6 handlers)
[GIN-debug] POST   /organizations/:orgId/users --> gitlab.com/api.geodatahub.dk/common.InviteUserToOrganizationRoute (6 handlers)
[GIN-debug] GET    /ping                     --> gitlab.com/api.geodatahub.dk/common.Ping (6 handlers)
[GIN-debug] OPTIONS /ping                     --> gitlab.com/api.geodatahub.dk/common.Return200 (6 handlers)
2021/05/21 10:46:31 handleGin took 96.347µs
[GIN-debug] Listening and serving HTTP on :8081
```

To access the API start your browser and visit `http://localhost:8081/ping`. If everything worked you should see the message `pong`. 

*If you see errors then ensure the local database is configured by running the test suite (see below).*

# Testing

## Setup the environment

The API code contains both unit and integration tests. The integration tests require access to a local postgresql database. The easiest way to get a running database for testing is to run it inside a container.

First add the local database variables to your terminal config file (ex. `baskrc` or `.zshrc`),

```bash
export DBHOST="127.0.0.1"
export DBUSR="postgres"
export DBPASSWORD="mylocaldbrootpassword"
export DBNONADMUSR="geodatahub"
export DBNONADMPASSWORD="mylocaldbnonrootpassword"
```

Then source your environment `source ~/.zshrc` and run the automatic script to start the container,

```bash
# From the API folder
./utils/init_postgres_container.sh
```

The empty database is now running. To setup all the correct tables and user permissions call the init script,

```bash
# From the API folder
cd utils/init_database
go run main.go

> 2020/01/29 09:15:17 Connecting to database
> 2020/01/29 09:15:17 Success: Connected to database
> 2020/01/29 09:15:17 Success: Created user
> 2020/01/29 09:15:18 Success: Created database
> 2020/01/29 09:15:18 Connecting to database
> 2020/01/29 09:15:18 Success: Connected to database
> 2020/01/29 09:15:18 Activated PostGIS!
> 2020/01/29 09:15:18 Activated uuid-ossp!
> 2020/01/29 09:15:18 Connecting to database
> 2020/01/29 09:15:18 Success: Connected to database
```

The database is now configured and ready to use. The final step is to run all the migration scripts to make sure the schema is up to date,

```bash
cd utils/migrate
go run main.go
```

## Running the test suite

Once the database is configured simply run the test suite,

```bash
cd ..  # From the root API folder
make test
```
