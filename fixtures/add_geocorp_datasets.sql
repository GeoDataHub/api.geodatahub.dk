INSERT INTO datasets (identifier, owner, organization, Datatype, Description, Distribution_Info, Result, Projected_Geometry, Roles)
VALUES
( '10000000-0000-0000-0000-000000000001',
  (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'),
  (SELECT id FROM organizations WHERE name = 'Fake Geocorp'),
  'https://schemas.geodatahub.dk/empty.json',
  'Dataset description',
  '{
      "Metadata": "One",
      "driller name": "John doe",
      "foo": "bar"
    }'::JSONB,
  '{}'::JSONB,
  ST_GeomFromGeoJSON('{
      "coordinates": [
        102.0,
        0.5
      ],
      "type": "Point"
    }'),
    (SELECT array_agg(id)
    FROM Roles
    WHERE organization = (select id FROM organizations WHERE name = 'Fake Geocorp')
    )
),
( '10000000-0000-0000-0000-000000000002',
  (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'),
  (SELECT id FROM organizations WHERE name = 'Fake Geocorp - office'),
  'https://schemas.geodatahub.dk/test_unique_constraint.json',
  'Dataset description',
  '{
      "foo": "One",
      "bar": "40"
    }'::JSONB,
  '{ "File": "d:" }'::JSONB,
  ST_GeomFromGeoJSON('{
      "coordinates": [
        8.0,
        55.5
      ],
      "type": "Point"
    }'),
    (SELECT array_agg(id)
    FROM Roles
    WHERE organization = (select id FROM organizations WHERE name = 'Fake Geocorp - office')
    )
),
( '10000000-0000-0000-0000-000000000003',
  (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'),
  (SELECT id FROM organizations WHERE name = 'Fake Geocorp - drillers'),
  'https://schemas.geodatahub.dk/test_unique_constraint.json',
  'Test #3 - Dataset in drillers org, shared with ext',
  '{
      "foo": "One",
      "bar": "42"
    }'::JSONB,
  '{ "File": "d:" }'::JSONB,
  ST_GeomFromGeoJSON('{
      "coordinates": [
        8.0,
        55.5
      ],
      "type": "Point"
    }'),
    (SELECT array_append(array_agg(id), '00000000-0000-0000-0000-000000000003')
    FROM Roles
    WHERE organization = (select id FROM organizations WHERE name = 'Fake Geocorp - drillers')
    )
),
( '10000000-0000-0000-0000-000000000004',
  (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'),
  (SELECT id FROM organizations WHERE name = 'Fake Geocorp - drillers'),
  'https://schemas.geodatahub.dk/test_unique_constraint.json',
  'Test #4 - Public dataset in drillers org to test duplicated keys',
  '{
      "foo": "One",
      "bar": "40"
    }'::JSONB,
  '{ "File": "d:" }'::JSONB,
  ST_GeomFromGeoJSON('{
      "coordinates": [
        8.0,
        55.5
      ],
      "type": "Point"
    }'),
    (SELECT array_append(array_agg(id), '55555555-5555-5555-5555-555555555555')
    FROM Roles
    WHERE organization = (select id FROM organizations WHERE name = 'Fake Geocorp - drillers')
    )
),
( '10000000-0000-0000-0000-000000000005',
  (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'),
  (SELECT id FROM organizations WHERE name = 'Fake Geocorp - drillers'),
  'https://schemas.geodatahub.dk/empty.json',
  'Test #5 - Dataset in drillers org, shared with multiple ext',
  '{"driller name": "Jane doe"}'::JSONB,
  '{}'::JSONB,
  ST_GeomFromGeoJSON('{
      "coordinates": [
        8.0,
        55.5
      ],
      "type": "Point"
    }'),
    (SELECT array_agg(id) || ARRAY['00000000-0000-0000-0000-000000000003'::UUID] || ARRAY['00000000-0000-0000-0000-000000000004'::UUID]
    FROM Roles
    WHERE organization = (select id FROM organizations WHERE name = 'Fake Geocorp - drillers')
    )
),

-- Janes personal dataset
( '10000000-1000-0000-0000-000000000001',
  (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'),
  Null,
  'https://schemas.geodatahub.dk/test.json',
  'Test #11 - Personal dataset owned by jane',
  '{
      "parameter1": "One",
      "parameter2": 42,
      "janes parameter": "private"
    }'::JSONB,
  '{ "File": "d:" }'::JSONB,
  ST_GeomFromGeoJSON('{
      "coordinates": [
        8.0,
        55.5
      ],
      "type": "Point"
    }'),
    -- Only Jane can access this
    (SELECT array_agg(id) FROM users WHERE email = 'jane@fakegeocorp.com')
);

-- Update uniqueKeys
UPDATE datasets
SET unique_keys = 'bar=40,foo=one'
WHERE identifier = '10000000-0000-0000-0000-000000000004'::UUID;

UPDATE datasets
SET unique_keys = 'bar=42,foo=one'
WHERE identifier = '10000000-0000-0000-0000-000000000003'::UUID;
