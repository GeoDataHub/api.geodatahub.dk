-- Create Jane as the admin of GeoCorp
INSERT INTO Users (id, email)
VALUES
('00000000-0000-0000-0000-000000000001', 'jane@fakegeocorp.com'),
('00000000-0000-0000-0000-000000000002', 'john@fakegeocorp.com'),
('00000000-0000-0000-0000-000000000003', 'john@fakeextcorp.com'),
('00000000-0000-0000-0000-000000000004', 'alice@fakegeocorp.com'),
('00000000-0000-0000-0000-000000000005', 'bob@fakegeocorp.com');

-- Add organization
--
--               Fake Geocorp
--               /            \
--          drillers          office
--                               \
--                             office dwellers
--
INSERT INTO organizations (id, name, owner, num_users, subscription_plan, plan_begins, plan_ends, org_hierarchy)
VALUES
('10000000-0000-0000-0000-000000000001', 'Fake Geocorp', (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'), 1, 1, '2000-01-01 00:00:00', '2100-01-01 00:00:00', text2ltree(replace('10000000-0000-0000-0000-000000000001', '-', '_'))),
('10000000-0000-0000-0000-000000000002', 'Fake Geocorp - drillers', (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'), 1, NULL, NULL, NULL, text2ltree(replace('10000000-0000-0000-0000-000000000001.10000000-0000-0000-0000-000000000002', '-', '_'))),
('10000000-0000-0000-0000-000000000003', 'Fake Geocorp - office', (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'), 1, NULL, NULL, NULL, text2ltree(replace('10000000-0000-0000-0000-000000000001.10000000-0000-0000-0000-000000000003', '-', '_'))),
('10000000-0000-0000-0000-000000000004', 'Fake Geocorp - office dwellers', (SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'), 1, NULL, NULL, NULL, text2ltree(replace('10000000-0000-0000-0000-000000000001.10000000-0000-0000-0000-000000000003.10000000-0000-0000-0000-000000000004', '-', '_')));

-- Add roles
INSERT INTO roles (name, organization, can_read, can_write, can_share, can_manage_users, can_manage_org)
VALUES
('Fake Geocorp admins', (SELECT id FROM organizations WHERE name = 'Fake Geocorp'), true, true, true, true, true),
('Fake Geocorp writers', (SELECT id FROM organizations WHERE name = 'Fake Geocorp'), true, true, false, false, false),
('Fake Geocorp office admins', (SELECT id FROM organizations WHERE name = 'Fake Geocorp - office'), true, true, true, true, false),
('Fake Geocorp office readers', (SELECT id FROM organizations WHERE name = 'Fake Geocorp - office'), true, false, false, false, false),
('Fake Geocorp driller readers', (SELECT id FROM organizations WHERE name = 'Fake Geocorp - drillers'), true, false, false, false, false);

-- Add Jane and John to roles
UPDATE Users SET roles = array_append(roles, (SELECT id FROM Roles WHERE name = 'Fake Geocorp admins')) WHERE email = 'jane@fakegeocorp.com';
UPDATE Users SET roles = array_append(roles, (SELECT id FROM Roles WHERE name = 'Fake Geocorp writers')) WHERE email = 'john@fakegeocorp.com';
UPDATE Users SET roles = array_append(roles, (SELECT id FROM Roles WHERE name = 'Fake Geocorp office admins')) WHERE email = 'john@fakegeocorp.com';
UPDATE Users SET roles = array_append(roles, (SELECT id FROM Roles WHERE name = 'Fake Geocorp office admins')) WHERE email = 'alice@fakegeocorp.com';
