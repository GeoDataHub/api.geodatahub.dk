DELETE FROM datasets WHERE organization IN (SELECT id FROM organizations WHERE name LIKE 'Fake Geo%');
DELETE FROM datasets WHERE owner IN (SELECT id FROM users WHERE email LIKE '%@fakegeocorp.com');
