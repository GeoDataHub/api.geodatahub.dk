module gitlab.com/api.geodatahub.dk

go 1.13

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/aws/aws-sdk-go v1.17.7
	github.com/awslabs/aws-lambda-go-api-proxy v0.8.1
	github.com/gin-contrib/gzip v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/golang-migrate/migrate/v4 v4.13.0
	github.com/jstemmer/go-junit-report v0.9.1 // indirect
	github.com/lestrrat-go/jwx v1.1.5
	github.com/lib/pq v1.3.0
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.3 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.6.1
	github.com/tj/go-pg-escape v1.1.0
	github.com/xeipuuv/gojsonschema v1.2.0
)
