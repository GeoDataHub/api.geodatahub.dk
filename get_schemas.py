#!/usr/bin/env python
import os
import requests
import json


def get_schemas():
    full_schemas = requests.get('https://schemas.getgeodata.com/full_schemas.json').json()
    for schema_name, schema in full_schemas.items():
        if "allOf" in schema:
            # Fill in the reference schema ($ref) in nested schemas
            for array_idx, subschema in enumerate(schema["allOf"]):
                if "$ref" in subschema:
                    # Overwrite the $ref with the actual schema
                    ref_schema_path = subschema["$ref"].replace("https://schemas.geodatahub.dk/", "").replace(".json", "")
                    ref_schema = {
                        "type": "object",
                        "properties": full_schemas[ref_schema_path]["properties"]
                    }
                    schema["allOf"][array_idx] = ref_schema

        dirname = os.path.dirname(schema_name)
        if dirname != '':
            os.makedirs(f"schemas/{dirname}", exist_ok=True)
        json.dump(schema, open(f"schemas/{schema_name}.json", "w"), indent=6)


get_schemas()
