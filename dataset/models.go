package dataset

import (
	"database/sql"
	"errors"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	pg "github.com/lib/pq"
	"github.com/satori/go.uuid"

	"gitlab.com/api.geodatahub.dk/common"
)

type Dataset struct {
	Identifier         uuid.UUID    `json:"identifier"`
	CreatedAt          time.Time    `json:"created_at"`
	UpdatedAt          time.Time    `json:"updated_at"`
	Owner              uuid.UUID    `json:"owner" validate="required, uuid"`
	Organization       uuid.UUID    `json:"organization" validate="uuid"`
	contact            RefLink      // This field is local and cannot marshal nor unmarshal
	Datatype           string       `json:"datatype" binding:"required" validate="uri"`
	Description        string       `json:"description" binding:"required"`
	DistributionInfo   common.JSONB `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB" json:"distribution_info" binding:"required"`
	Result             common.JSONB `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB" json:"result" binding:"required"`
	ProjectedGeometry  Geometry     `json:"projected_geometry" binding:"required"`
	roles              []uuid.UUID  // This field is local and cannot marshal nor unmarshal
	Public             bool            `json:"Publicize"`
	Sharing            []common.JSONB  `json:"Sharing"`
	Links              []ObjectLink    `json:"_links"`
}

//ObjectLink Struct used to define object references to other objects
type ObjectLink struct {
	Rel    string `json:"rel"`
	Href   string `json:"href"`
	Method string `json:"method"`
}

type RefLink struct {
	Name  string     `json:"name"`
	Links ObjectLink `json:"_links"`
}

// MarshalJSON converts the dataset object into the correct JSON.
// This fulfills the json.Marshaler interface.
func (d Dataset) MarshalJSON() ([]byte, error) {
	// defining a struct here lets us define the order of the JSON elements
	// without the fields we don't need
	type responseDataset struct {
		Identifier         uuid.UUID    `json:"identifier"`
		CreatedAt          time.Time    `json:"created_at"`
		UpdatedAt          time.Time    `json:"updated_at"`
		Owner              string       `json:"owner,omitempty"`
		Organization       string       `json:"organization,omitempty"`
		Contact            RefLink      `json:"contact"`
		Datatype           string       `json:"datatype" binding:"required" validate="uri"`
		Description        string       `json:"description" binding:"required"`
		DistributionInfo   common.JSONB `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB" json:"distribution_info" binding:"required"`
		Result             common.JSONB `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB" json:"result" binding:"required"`
		ProjectedGeometry  Geometry     `json:"projected_geometry" binding:"required"`
		Public             bool            `json:"Publicize"`
		Sharing            []common.JSONB  `json:"Sharing"`
		Links              []ObjectLink    `json:"_links"`
	}

	dset := &responseDataset{
		Identifier: d.Identifier,
		CreatedAt: d.CreatedAt,
		UpdatedAt: d.UpdatedAt,
		Owner: "",
		Organization: "",
		Contact: d.contact, // Forward private variable to JSON
		Datatype: d.Datatype,
		Description: d.Description,
		DistributionInfo: d.DistributionInfo,
		Result: d.Result,
		ProjectedGeometry: d.ProjectedGeometry,
		Public: d.Public,
		Sharing: d.Sharing,
		Links: d.Links,
	}

	// Only marshal Owner and Org if they are not empty
	// Otherwise they are omited from the response
	if d.Organization != uuid.Nil {
		dset.Organization = d.Organization.String()
	}
	if d.Owner != uuid.Nil {
		dset.Owner = d.Owner.String()
	}

	return json.Marshal(dset)
}

// createLink create a link to a dataset resource
func (d *Dataset) createLink(name string, method string, extension string) (ObjectLink) {
	url := common.APIBaseURL
	if common.Environment != "prod" {
		url = common.APIBaseDevURL
	}
	url = strings.Join([]string{url, "datasets/%s/%s"}, "/")

	return ObjectLink{
			name,
			fmt.Sprintf(url, d.Identifier, extension),
			method,
		}
}

// hasRole Check if the dataset has a given role
// TODO: Update to check Roles array when it is returned
func (d *Dataset) hasRole(role uuid.UUID) (bool, error) {
	db := common.GetStandardDB()
	var has bool = false
	err := db.QueryRow("SELECT roles && array[$1::uuid] AS hasRole FROM datasets WHERE identifier = $2",
		role, d.Identifier).Scan(&has)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, common.ReportInternalError(err)
	}
	return has, nil
}

func (d *Dataset) addRole(role uuid.UUID) error {
	db := common.GetStandardDB()
	if _, err := db.Exec("UPDATE datasets SET Roles = array_append(roles, $1) WHERE identifier = $2",
		role, d.Identifier); err != nil {
		return common.ReportInternalError(err)
	}
	return nil
}

func (d *Dataset) removeRole(role uuid.UUID) error {
	db := common.GetStandardDB()
	if _, err := db.Exec("UPDATE datasets SET Roles = array_remove(roles, $1) WHERE identifier = $2",
		role, d.Identifier); err != nil {
		return common.ReportInternalError(err)
	}
	return nil
}

func (d *Dataset) isPublic() (bool) {
	// strip the return code - this should only fail in case of
	// SQL syntax error
	public, _ := d.hasRole(common.PublicOrganization)
	return public
}

// geometryToJSON convert geometry column to JSON
func (d *Dataset) geometryToJSON() []byte {
	json, err := d.ProjectedGeometry.MarshalJSON()
	if err != nil {
		return []byte(``)
	}
	return json
}

// DeleteDatasetByID Delete a dataset by it's id
func DeleteDatasetByID(datasetID uuid.UUID) (error) {
	db := common.GetStandardDB()
	if db == nil {
		err := fmt.Errorf("Unable to connect to database")
		return common.ReportInternalError(err)
	}

	rows, err := db.Exec("DELETE FROM datasets WHERE Identifier = $1", datasetID)
	if err != nil {
		return common.ReportInternalError(err)
	}

	if affec, _ := rows.RowsAffected(); affec == 0 {
		return fmt.Errorf("The dataset (%s) either does not exist or you don't have permissions to delete it", datasetID.String())
	}

	return nil
}

// UpdateEntireDataset overwrite all parameters in the dataset
func UpdateEntireDataset(dataset *Dataset, uniqueKeys string) (error) {
	db := common.GetStandardDB()

	// Cast Org uuid to nullable type to support database format
	orgUUID := wrapUUIDToNil(dataset.Organization)

	_, err := db.Exec(`
    UPDATE datasets
    SET owner = $1,
        organization = $2,
		updated_at = NOW(),
		description = $3,
		distribution_info = $4,
		result = $5,
		projected_geometry = ST_GeomFromGeoJSON($6),
        unique_keys = $7
	WHERE identifier = $8
    `,
		dataset.Owner, orgUUID, dataset.Description,
		dataset.DistributionInfo, dataset.Result, dataset.geometryToJSON(),
		uniqueKeys, dataset.Identifier)

	if err != nil {
		if strings.Contains(err.Error(), "datasets_unique_keys_organization_datatype_key") {
			return fmt.Errorf("update would violate unique constraint - update failed")
		}
		return common.ReportInternalError(err)
	}
	return nil
}

// UpdateSingleParams change single parameters in a dataset
func UpdateSingleParams(datasetID uuid.UUID, params map[string]interface{}) (error) {
	db := common.GetStandardDB()

	tx, err := db.Begin()
	if err != nil {
		return common.ReportInternalError(fmt.Errorf("Unable to create database transaction with error: %s", err.Error()))
	}
	defer tx.Rollback()

	for k, v := range params {
		// Note: The keys have already been verified
		typecast := ""
		if common.Contains([]string{"result", "distribution_info"}, k) {
			// Typecast if required
			v, err = json.Marshal(&v)
			if err != nil {
				return fmt.Errorf("%s does not match the expected type", k)
			}
			typecast = "::JSONB"
		}
		statement := fmt.Sprintf("UPDATE datasets SET %s = $2%s WHERE identifier = $1", k, typecast)
		_, err := tx.Exec(statement, datasetID, v)
		if err != nil {
			if strings.Contains(err.Error(), "parse error") {
				return fmt.Errorf("%s does not match the expected type", k)
			}
			if strings.Contains(err.Error(), "datasets_unique_keys_organization_datatype_key") {
				return fmt.Errorf("update would violate unique constraint - update failed")
			}
			return common.ReportInternalError(fmt.Errorf("Unable to update dataset with error %s", err.Error()))
		}
	}

	err = tx.Commit()
	if err != nil {
		return common.ReportInternalError(err)
	}

	return nil
}

// Return a nul-able uuid
func wrapUUIDToNil(uid uuid.UUID) uuid.NullUUID {
	if uid == uuid.Nil {
		return uuid.NullUUID{
			UUID: uid,
			Valid: false,
		}
	}
	return uuid.NullUUID{
		UUID: uid,
		Valid: true,
	}
}

// createNewDataset create a new geodata dataset
func CreateNewDataset(DB *sql.DB, dataset *Dataset, uniqueKeys string) (uuid.UUID, error) {

	var uid uuid.UUID

	// Cast Org uuid to nullable type to support database format
	orgUUID := wrapUUIDToNil(dataset.Organization)

	var err error
	var msg string
	if strings.Compare(uniqueKeys, "") == 0 {
		// No unique Keys
		err = DB.QueryRow(`INSERT INTO datasets (
												created_at,
												updated_at,
												owner,
												organization,
												datatype,
												description,
												distribution_info,
												result,
                                                roles,
												projected_geometry)
						   VALUES (NOW(),
								   NOW(),
								   $1,
								   $2,
                                   $3,
								   $4,
								   $5,
								   $6,
                                   $7,
								   ST_GeomFromGeoJson($8))
						   RETURNING identifier`,
			dataset.Owner,
			orgUUID,
			dataset.Datatype,
			dataset.Description,
			dataset.DistributionInfo,
			dataset.Result,
			pg.Array(dataset.roles),
			dataset.geometryToJSON()).Scan(&uid)

		msg = fmt.Sprintf("Created new dataset with ID %s", uid)
	} else {
		// Updating an existing dataset based on unique keys
		err = DB.QueryRow(`INSERT INTO datasets (
												created_at,
												updated_at,
												owner,
												organization,
												datatype,
												description,
												distribution_info,
												result,
												projected_geometry,
												unique_keys,
                                                Roles)
						   VALUES (NOW(),
								   NOW(),
								   $1,
								   $2,
								   $3,
								   $4,
								   $5,
								   $6,
								   ST_GeomFromGeoJson($7),
								   $8,
                                   $9)
						   ON CONFLICT (unique_keys, organization, datatype) DO UPDATE
						   SET
							 updated_at = NOW(),
							 description = EXCLUDED.description,
							 distribution_info = EXCLUDED.distribution_info,
							 result = EXCLUDED.result,
							 projected_geometry = EXCLUDED.projected_geometry,
							 unique_keys = EXCLUDED.unique_keys
						   RETURNING identifier`,
			dataset.Owner,
			orgUUID,
			dataset.Datatype,
			dataset.Description,
			dataset.DistributionInfo,
			dataset.Result,
			dataset.geometryToJSON(),
			uniqueKeys,
			pg.Array(dataset.roles),
		).Scan(&uid)
		msg = fmt.Sprintf("Updated an existing dataset with ID %s", uid)
	}

	if err != nil {
		if strings.Contains(err.Error(), "datasets_unique_keys_organization_datatype_key") {
			return uuid.Nil, fmt.Errorf("dataset would violate unique constraint - cannot continue")
		}

		return uuid.Nil, common.ReportInternalError(err)
	}

	log.Printf(msg)
	return uid, nil
}

//GetDatasetByID Retrieve a dataset by it's id
func GetDatasetByID(ID uuid.UUID) (Dataset, error) {
	DB := common.GetStandardDB()

	var data Dataset
	var orgUUID uuid.NullUUID

	url := common.APIBaseURL
	if common.Environment != "prod" {
		url = common.APIBaseDevURL
	}

	err := DB.QueryRow(`
       SELECT identifier, created_at, updated_at,
              owner, organization,
              datatype, description, distribution_info, result,
              ST_AsGeoJSON(projected_geometry)::json as projected_geometry, roles::text[] as Roles,
              -- Return True if public role in roles
              ARRAY[$2::UUID] && datasets.roles AS public,
              COALESCE((
						SELECT array_agg(json_build_object('name', email, '_links', json_build_object(
								'rel', 'self',
								'href', concat($3::text, '/users/', id),
								'method', 'GET'
							)))
						FROM users
						 -- Find any overlap between roles on the dataset and users
						WHERE ARRAY [id] && datasets.roles
                        -- Remove the owner since the dataset is not really "shared" with him/her
                        AND id != datasets.Owner
					)
			   , ARRAY[]::JSON[]) AS sharing
       FROM datasets
       WHERE identifier = $1`,
		ID, common.PublicOrganization, url).Scan(&data.Identifier, &data.CreatedAt, &data.UpdatedAt, &data.Owner, &orgUUID,
		&data.Datatype, &data.Description, &data.DistributionInfo, &data.Result, &data.ProjectedGeometry,
		pg.Array(&data.roles), &data.Public, pg.Array(&data.Sharing))
	if err != nil {
		if err == sql.ErrNoRows {
			return data, fmt.Errorf("dataset not found")
		}
		return data, common.ReportInternalError(err)
	}

	if orgUUID.Valid {
		data.Organization = orgUUID.UUID
	}
	return data, nil
}

// GetDatasets retrieve dataset(s) by query parameters
func GetDatasetWithQuery(DB *sql.DB, query map[string][]string, requestingUser uuid.UUID, includePublic bool, includeOrgs []uuid.UUID) (*[]byte, error) {
	common.TimeTrack(time.Now(), "Search")

	var requestedData []byte

	pgQuery := `SELECT
                    CASE
                      WHEN count(query) > 15000 THEN
                        -- In case the user requested too much data an empty response is returned. This is handled further out
                        -- so the user sees a descriptive message.
                        NULL
                      ELSE
                        coalesce(json_agg(query), '[]'::json) -- If empty return an empty json array
                    END
                FROM (
                  SELECT
                    identifier, datatype, description, distribution_info,
                    result, projected_geometry
                  FROM datasets
                  WHERE ( %s ) -- Data query
                  AND ( %s )   -- Perm query
                 ) AS query
                LIMIT 15000`

	whereQuery, queryParameters, paramCount, err := buildSearchQuery(query["q"])
	if err != nil {
		return &requestedData, err
	}
	if len(queryParameters) == 0 {
		return &requestedData, errors.New("query did not contain any valid parameters")
	}

	// Add permission logic
	// Users can search datasets
	// - they own (their userID is in roles)
	// - dataset in orgs they belong to (their roles overlap with the dataset roles)
	// - public datasets (datasets have the public role)
	roles, err := common.RolesFromOrganizations(includeOrgs)
	if err != nil {
		return &requestedData, err
	}
	// Ensure the user can search in his/her own dataset and public dataset if requested
	roles = append(roles, requestingUser)
	if includePublic {
		roles = append(roles, common.PublicOrganization)
	}
	permQuery := fmt.Sprintf("(roles && $%d)", paramCount)

	// Execute query
	pgQuery = fmt.Sprintf(pgQuery, whereQuery, permQuery)
	queryParameters = append(queryParameters, pg.Array(roles))

	err = DB.QueryRow(pgQuery, queryParameters...).Scan(&requestedData)
	if err != nil {
		if strings.Contains(err.Error(), "invalid geometry") {
			return &requestedData, fmt.Errorf("invalid geometry: '%s' is not a supported WTK format", queryParameters[0])
		}
		return &requestedData, common.ReportInternalError(err)
	}

	return &requestedData, nil
}

//GenerateDatasetHeatmap return a heatmap of where data is located
func GenerateDatasetHeatmap(rqstRoles []uuid.UUID) ([]byte) {
	DB := common.GetStandardDB()

	// Example output
	// +------------------------------------------------+
	// | json_agg                                       |
	// |------------------------------------------------|
	// | [{"weight":6,"coordinates":[5.625,59.0625]},   |
	// |  {"weight":2,"coordinates":[106.875,2.8125]},  |
	// |  {"weight":3,"coordinates":[5.625,53.4375]}]   |
	// +------------------------------------------------+
	var heatmaps []byte
	errSQL := DB.QueryRow(`
                         SELECT json_agg(query)
                         FROM (
                           SELECT count(identifier) as weight,
                                  array[ST_X(
                                          ST_PointFromGeohash(
                                            ST_GeoHash(projected_geometry,4)
                                          )
                                         ),
                                         ST_Y(
                                           ST_PointFromGeohash(
                                             ST_GeoHash(projected_geometry,4)
                                           )
                                         )] as coordinates
                           FROM datasets
                           WHERE ST_GeometryType(projected_geometry) != 'ST_GeometryCollection'
                           AND roles && $1
                           GROUP BY ST_geohash((projected_geometry), 4)) as query
          `, pg.Array(rqstRoles)).Scan(&heatmaps)

	if errSQL != nil {
		log.Fatal(errSQL)
	}

	return heatmaps
}
