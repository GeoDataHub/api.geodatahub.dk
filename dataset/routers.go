package dataset

import (
	"fmt"
	"log"
	"os"
	"strings"
	"sort"

	"github.com/gin-gonic/gin"
	//	"github.com/twpayne/go-geom/encoding/ewkb"
	//	"github.com/paulmach/go.geojson"
	"github.com/satori/go.uuid"
	"github.com/xeipuuv/gojsonschema"
	"gitlab.com/api.geodatahub.dk/common"
)

var err error

func checkRequestAndPermissions(c *gin.Context, permission string, loginRequired bool) (bool, Dataset) {
	var dataset Dataset
	requestingUser, err := common.GetRequestingUser(c, loginRequired)
	if err != nil {
		// The GIN error is contained in the context
		return false, dataset
	}

	// Get ID and validate input
	id := c.Params.ByName("id")
	uid, err := uuid.FromString(id)
	if err != nil || uid == uuid.Nil {
		c.AbortWithStatusJSON(400, gin.H{"error": fmt.Sprintf("The dataset ID (%s) is empty or not a valid UUIDv4 format", id)})
		return false, dataset
	}

	dataset, err = GetDatasetByID(uid)
	if err != nil {
		code := 404
		if strings.Contains(err.Error(), "FATAL") {
			// This was a FATAL DB error - return correct HTTP status
			code = 500
		}
		c.JSON(code, gin.H{"error": err.Error()})
		return false, dataset
	}

	var isAllowed bool = false
	if dataset.Organization == uuid.Nil && requestingUser.ID == dataset.Owner {
		// The dataset is private and only belongs to this user
		isAllowed = true
	}

	if !isAllowed && dataset.Organization != uuid.Nil {
		// Dataset exists but is the user allowed to see it through the hierachy?
		isAllowed, err = requestingUser.Can(dataset.Organization, permission)
		if err != nil {
			c.AbortWithStatusJSON(400, gin.H{"Error": err.Error()}); return false, dataset
		}

		// When this is only a read request and the dataset is shared with the user
		// or public then it is allowed
		if !isAllowed && permission == "can_read" && (common.UUIDContains(dataset.roles, requestingUser.ID) || dataset.isPublic()) {
			isAllowed = true

			// All external users are not allowed to see sharing information
			dataset.Sharing = nil
		}
	}

	if !isAllowed {
		// Hide the fact that the dataset exists if the user does not have access
		c.AbortWithStatusJSON(403, gin.H{"Error": "not allowed"}); return false, dataset
	}

	// Get users permissions to append permission reference
	userRoles, err := requestingUser.OrganizationPermissions(dataset.Organization)
	if err != nil {
		return false, dataset
	}

	refPermissions := []ObjectLink{
		// The user can always read if a response is returned
		dataset.createLink("self", "GET", ""),
	}

	if userRoles.Write() {
		refPermissions = append(refPermissions,
			[]ObjectLink{
				// The user can always read if a response is returned
				dataset.createLink("delete", "DELETE", ""),
				dataset.createLink("update", "PATCH", ""),
				dataset.createLink("overwrite", "PUT", ""),
			}...
		)
	}

	if userRoles.Share() {
		refPermissions = append(refPermissions,
			[]ObjectLink{
				// The user can always read if a response is returned
				dataset.createLink("share",     "POST",   "share"),
				dataset.createLink("unshare",   "DELETE", "share"),
				dataset.createLink("publicize", "POST",   "publicize"),
				dataset.createLink("privatize", "DELETE", "publicize"),
			}...
		)
	}

	url := common.APIBaseURL
	if common.Environment != "prod" {
		url = common.APIBaseDevURL
	}

	// Create Contact reference to dataset owner/organization
	if dataset.Organization != uuid.Nil {
		// The organization is the main contact
		org, _ := common.GetOrganization(dataset.Organization)

		dataset.contact = RefLink{
			Name: org.Name,
			Links: ObjectLink{
				Rel: "self",
				Href: strings.Join([]string{url, "organizations", org.ID.String()}, "/"),
				Method: "GET",
			},
		}
	} else if dataset.Owner != uuid.Nil {
		// The owner is the main contact
		usr, _ := common.GetUser(dataset.Owner)

		dataset.contact = RefLink{
			Name: usr.Email,
			Links: ObjectLink{
				Rel: "self",
				Href: strings.Join([]string{url, "users", usr.ID.String()}, "/"),
				Method: "GET",
			},
		}
	} else {
		// Hide the fact that the dataset exists if the user does not have access
		c.AbortWithStatusJSON(500, gin.H{"Error": "The dataset has no contact - please contact support"}); return false, Dataset{}
	}

	// Assign links to allowed dataset endpoints
	dataset.Links = refPermissions

	return true, dataset

}

// GetDatasetByID Returnes a dataset given the UUIDv4 ID
func GetDatasetByIDRoute(c *gin.Context) {
	success, dataset := checkRequestAndPermissions(c, "can_read", false)
	if !success {
		return
	}

	// Users should not see the raw Owner and Org. Nil values are striped in the
	// JSON marshal function
	dataset.Owner = uuid.Nil
	dataset.Organization = uuid.Nil

	c.JSON(200, dataset); return
}

// DeleteDatasetByID Deletes a dataset from the storage backend given the UUIDv4 ID
func DeleteDatasetByIDRoute(c *gin.Context) {
	success, dataset := checkRequestAndPermissions(c, "can_write", true)
	if !success {
		return
	}

	if err = DeleteDatasetByID(dataset.Identifier); err != nil {
		if strings.Contains(err.Error(), "does not exist") {
			c.AbortWithStatusJSON(404, gin.H{"error": err.Error()})
			return
		}

		c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, "OK")
	return
}

// UpdateEntireDatasetRoute Overwrite all existing parameters in the dataset
func UpdateEntireDatasetRoute(c *gin.Context) {
	success, oldParameters := checkRequestAndPermissions(c, "can_write", true)
	if !success {
		return
	}

	var newParameters Dataset
	err := c.BindJSON(&newParameters)
	if err != nil || newParameters.Owner == uuid.Nil {
		c.AbortWithStatusJSON(400, "dataset does not match expected format"); return
	}

	// Check the new distribution info is valid
	success, schemaRoot, jsonRoot := validateDistributionInfo(c, &newParameters)
	if !success {
		return
	}

	// Find any unique constraints with the current schema. The unique constraint is checked
	// against the database to ensure no dataset is added twice.
	uniqueKeys := getSortedUniqueKeysFromSchema(schemaRoot, jsonRoot)

	// Update dataset
	newParameters.Identifier = oldParameters.Identifier
	if err := UpdateEntireDataset(&newParameters, uniqueKeys); err != nil {
		c.AbortWithStatusJSON(400, gin.H{"error": err.Error()}); return
	}
	c.JSON(200, "dataset updated")
}

// UpdateDatasetSingleParamRoute Update a dataset from the storage backend given the UUIDv4 ID
func UpdateDatasetSingleParamRoute(c *gin.Context) {
	success, dataset := checkRequestAndPermissions(c, "can_write", true)
	if !success {
		return
	}

	var newParams map[string]interface{}
	err = c.BindJSON(&newParams)
	if err != nil {
		fmt.Printf("User tries to update with invalid parameters. Error: %s\n", err.Error())
		c.AbortWithStatusJSON(400, gin.H{"error:": "unable to decode JSON payload"})
		return
	}

	if len(newParams) == 0 {
		c.AbortWithStatusJSON(400, gin.H{"error": "request was empty"}); return
	}

	expectedKeys := []string{"datatype", "description", "distribution_info", "result", "projected_geometry"}
	for k := range newParams {
		if !common.Contains(expectedKeys, k) {
			msg := fmt.Sprintf("unsupported parameter %s", k)
			if common.Contains([]string{"roles"}, k) {
				msg = fmt.Sprintf("cannot update the roles parameter. Modify owner or organization to change permissions")
			}
			if common.Contains([]string{"public"}, k) {
				msg = fmt.Sprintf("call publicize endpoint to make dataset public or private")
			}

			c.AbortWithStatusJSON(400, gin.H{"error": msg}); return
		}
	}

	// Updating datatype or distribution info
	newDist := newParams["distribution_info"]
	newType := newParams["datatype"]
	if newDist != nil {
		if newType != nil {
			dataset.Datatype = newType.(string)
		}
		dataset.DistributionInfo = common.JSONB(newDist.(map[string]interface{}))
		success, schemaRoot, jsonRoot := validateDistributionInfo(c, &dataset)
		if !success {
			return
		}

		// Find any unique constraints with the current schema. The unique constraint is checked
		// against the database to ensure no dataset is added twice.
		newParams["unique_keys"] = getSortedUniqueKeysFromSchema(schemaRoot, jsonRoot)
	}

	if err := UpdateSingleParams(dataset.Identifier, newParams); err != nil {
		code := 400
		if strings.Contains(err.Error(), "FATAL") {
			code = 500
		}
		c.AbortWithStatusJSON(code, gin.H{"error": err.Error()}); return
	}

	c.JSON(200, "dataset updated")
}

func getSortedUniqueKeysFromSchema(schemaRoot interface{}, jsonRoot interface{}) string {
	schemaMap := schemaRoot.(map[string]interface{})
	jsonMap   := jsonRoot.(map[string]interface{})
	var uniqueKeyArray []string

	var uniqueKeys string
	// Check each schema property for a unique constraint
	if schemaMap["uniqueProperties"] != nil {
		// Sort keys and turn the JSON response to a string
		schemaIface := schemaMap["uniqueProperties"].([]interface{})

		for itt := range schemaIface {
			uniqueKeyArray = append(uniqueKeyArray, schemaIface[itt].(string))
		}

		sort.Strings(uniqueKeyArray)
		for itt := range uniqueKeyArray {
			jsonKey := uniqueKeyArray[itt]  // Key from schema
			jsonValue := jsonMap[jsonKey]   // value from DistributionInfo
			if jsonValue != nil {
				uniqueKeys += fmt.Sprintf("%s=%s,", jsonKey, jsonMap[jsonKey])
			}
		}
		return strings.ToLower(strings.TrimRight(uniqueKeys, ","))
	}

	return ""
}

func validateDistributionInfo(c *gin.Context, newData *Dataset) (bool, interface{}, interface{}) {
	// Translate the datatype from a URL to the URI of the local files
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	schemaPath := []string{}
	schemaPath = append(schemaPath, "file://")
	if os.Getenv("DISABLE_SSL_FOR_TESTING") == "" {
		schemaPath = append(schemaPath, dir)
		schemaPath = append(schemaPath, "/schemas/")
	} else {
		// Change search path in testing
		schemaPath = append(schemaPath, strings.Split(dir, "api.geodatahub.dk")[0])
		schemaPath = append(schemaPath, "api.geodatahub.dk/testdata/schemas/")
	}

	uri := strings.Replace(newData.Datatype, "https://schemas.geodatahub.dk/", "", -1)
	schemaPath = append(schemaPath, uri)
	uri = strings.Join(schemaPath, "")

	fmt.Printf("Loading schema references before validation\n")

    _, err = os.Stat(strings.Join(schemaPath[1:], ""))
    if os.IsNotExist(err) {
        log.Printf("User requested %s. Error: %s", newData.Datatype, err)
		c.AbortWithStatusJSON(400, gin.H{"Error": "The requested schema does not exist"})
		return false, nil, nil
    }

	// Validate the content of distribution info
	schemaLoader := gojsonschema.NewReferenceLoader(uri)
	documentLoader := gojsonschema.NewGoLoader(newData.DistributionInfo)

	fmt.Printf("About to validate document\n")

	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"Error": err.Error()})
		return false, nil, nil
	}

	if result.Valid() {
		fmt.Printf("The document is valid\n")
	} else {
		combinedErrors := []string{}
		combinedErrors = append(combinedErrors, "The document is not valid. see errors :\n")
		for _, desc := range result.Errors() {
			combinedErrors = append(combinedErrors, fmt.Sprintf("- %s\n", desc))
		}
		c.AbortWithStatusJSON(400, gin.H{"Error": strings.Join(combinedErrors, "")})
		return false, nil, nil
	}

	schemaRoot, err := schemaLoader.LoadJSON()
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"Unable to load schema with error ": err.Error()})
		return false, nil, nil
	}
	jsonRoot, err := documentLoader.LoadJSON()
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"Unable to load json with error ": err.Error()})
		return false, nil, nil
	}

	return true, schemaRoot, jsonRoot
}

// CreateDataset handles all REST requests from the API
func CreateDatasetRoute(c *gin.Context) {
	user, err := common.GetRequestingUser(c, true)
	if err != nil {
		return
	}

	var new_data Dataset
	err = c.BindJSON(&new_data)
	if err != nil {
		log.Printf(err.Error())
		c.AbortWithStatusJSON(400, gin.H{"Error": err.Error()})
		return
	}

	db := common.GetStandardDB()
	new_data.Owner = user.ID

	// Check if user requested an organization
	if new_data.Organization != uuid.Nil {
		// Check if the user is allowed to perform this request
		isAllowed, err := user.Can(new_data.Organization, "can_write")

		if err != nil {
			c.AbortWithStatusJSON(400, gin.H{"Error": err.Error()})
			return
		}
		if !isAllowed {
			c.AbortWithStatusJSON(403, gin.H{"Error": "You are not authorized to create dataset in that organization"})
			return
		}

		// Add all roles from org to the dataset
		newRoles, err := common.RolesFromOrganizations([]uuid.UUID{new_data.Organization})
		if err != nil {
			c.AbortWithStatusJSON(500, gin.H{"error": err.Error()}); return
		}
		new_data.roles = append(newRoles, new_data.Owner)

	} else {
		// If the dataset isn't owned by an organization only the owner
		// has access to it - unless public
		new_data.roles = append(new_data.roles, new_data.Owner)
	}

	if new_data.Public {
		new_data.roles = append(new_data.roles, common.PublicOrganization)
	}

	success, schemaRoot, jsonRoot := validateDistributionInfo(c, &new_data)
	if !success {
		return
	}

	// Find any unique constraints with the current schema. The unique constraint is checked
	// against the database to ensure no dataset is added twice.
	uniqueKeys := getSortedUniqueKeysFromSchema(schemaRoot, jsonRoot)

	fmt.Printf("Unique constrants was: %s\n", uniqueKeys)
	fmt.Printf("Ready to store dataset in database\n")

	id, err := CreateNewDataset(db, &new_data, uniqueKeys)
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"Error": err.Error()})
		return
	}

	c.JSON(200, gin.H{"id": id})
}

// GetAndSearchDatasetRoute Get all avilable datasets or search by keys and geographical location
func GetAndSearchDatasetRoute(c *gin.Context) {
	rqstUser, err := common.GetRequestingUser(c, false)
	if err != nil {
		return
	}

	// Get ID and validate input
	db := common.GetStandardDB()

	query := c.Request.URL.Query()
	if query["q"] == nil {
		c.AbortWithStatusJSON(400, gin.H{"error": "query parameter missing"}); return
	}
	_, pubSearch := c.GetQuery("public")
	orgSearchRaw, orgExists := c.GetQuery("organizations")

	var orgSearch []uuid.UUID
	if rqstUser.ID == uuid.Nil {
		if orgExists {
			c.AbortWithStatusJSON(400, gin.H{"error": "Anonymous users cannot request organizational search"}); return
		}

		// Anonymous users cannot search within organizations since they don't belong to any. Simply ignore
		// the request. Simply force the public flag to ensure the user gets some valid response on such requests.
		pubSearch = true

	} else if !orgExists {
		// Default to search in all organizations the user belongs to
		orgType, err := rqstUser.Organizations()
		if err != nil {
			c.AbortWithStatusJSON(500, err.Error()); return
		}
		for _, org := range orgType {
			orgSearch = append(orgSearch, org.ID)
		}
	} else {
		uuids := strings.Split(orgSearchRaw, ",")
		for _, id := range uuids {
			uid, err  := uuid.FromString(id)
			if err != nil {
				c.AbortWithStatusJSON(400, gin.H{"error": "organization parameters is malformed. Expected org1,org2,org3.. "}); return
			}
			// Ensure requested orgs are valid UUID and user is allowed within orgs
			allowed, err := rqstUser.Can(uid, "can_read")
			if !allowed {
				c.AbortWithStatusJSON(403, gin.H{"error": "you are not allowed to search in the requested organizations"}); return
			}
			if err != nil {
				c.AbortWithStatusJSON(500, gin.H{"error": err.Error()}); return
			}
			orgSearch = append(orgSearch, uid)
		}
	}

	gds, err := GetDatasetWithQuery(db, query, rqstUser.ID, pubSearch, orgSearch)

	if err != nil {
		code := 400
		if strings.Contains(err.Error(), "FATAL") {
			code = 500
		}
		c.AbortWithStatusJSON(code, gin.H{"error": err.Error()}); return
	}

	if len(*gds) == 0 {
		// The SQL query returned a NULL response (i.e. the request was too large). In case of no records the
		// response is `[]` which has a length of 2.
		c.AbortWithStatusJSON(403, gin.H{"error": "The request was too large. Please, reduce the search area or query parameters and try again."}); return
	}

	c.Data(200, gin.MIMEJSON, *gds)
	return
}

// PublicizeDatasetByIDRoute Make the dataset available publicly
func HandlePublicizeOrShareDatasetByIDRoute(c *gin.Context) {
	requestingUser, err := common.GetRequestingUser(c, true)
	if err != nil {
		// The GIN error is contained in the context
		return
	}

	id := c.Params.ByName("id")
	datasetID, err := uuid.FromString(id)
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"error": fmt.Sprintf("datasetID (%s) is not a valid UUIDv4 format", id)})
		return
	}

	// Get dataset
	dataset, err := GetDatasetByID(datasetID)
	if err != nil {
		c.AbortWithStatusJSON(404, gin.H{"error": "The requested dataset does not exist."}); return
	}

	// Check permissions
	isAllowed, err := requestingUser.Can(dataset.Organization, "can_manage_users")
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"Error": err.Error()})
		return
	}
	if !isAllowed {
		c.AbortWithStatusJSON(403, gin.H{"Error": "You are not authorized to make this dataset public. That operation requires `can_manage_users` permissions."})
		return
	}

	if exists := c.GetString("private"); exists != "" {
		if dataset.removeRole(common.PublicOrganization) != nil {
			c.AbortWithStatusJSON(500, gin.H{"Error": err.Error()}); return
		}

		c.JSON(200, gin.H{"Success": "The dataset is now private"})
		return
	}

	if exists := c.GetString("public"); exists != "" {
		if dataset.addRole(common.PublicOrganization) != nil {
			c.AbortWithStatusJSON(500, gin.H{"Error": err.Error()}); return
		}

		c.JSON(200, gin.H{"Success": "The dataset is now public"})
		return
	}

	// Handle share logic
	usrAdd := c.GetString("addUser")
	usrDel := c.GetString("delUser")
	usr := usrAdd
	if usrAdd == "" {
		usr = usrDel
	}
	userID := uuid.Nil

	if usr != "" {
		if (strings.Contains(usr, "@")) {
			user, err := common.GetUserByEmail(usr)
			if err != nil {
				c.AbortWithStatusJSON(400, gin.H{"error": "the user you wish to share with does not exist"}); return
			}

			userID = user.ID
		} else {
			userID, err = uuid.FromString(usr)
			if err != nil {
				c.AbortWithStatusJSON(400, gin.H{"error": "No user exists with that email"}); return
			}

			_, err = common.GetUser(userID)
			if err != nil {
				c.AbortWithStatusJSON(400, gin.H{"error": "the user you wish to share with does not exist"}); return
			}

		}

	}
	if usrAdd != "" {
		if dataset.addRole(userID) != nil {
			c.AbortWithStatusJSON(500, gin.H{"error": err.Error()}); return
		}

		c.JSON(200, gin.H{"success": "The dataset is now shared with the user"})
		return
	}
	if usrDel != "" {
		if dataset.removeRole(userID) != nil {
			c.AbortWithStatusJSON(500, gin.H{"error": err.Error()}); return
		}

		c.JSON(200, gin.H{"success": "The dataset is no longer shared with the user"})
		return
	}

	err = common.ReportInternalError(fmt.Errorf("The publicize/share route was called without either the create or delete param. This should never happen! Dataset was %s", id))
	c.AbortWithStatusJSON(500, gin.H{"error": err.Error()})
	return
}

//GenerateUsersDataHeatmap Return a JSON encoded heatmap with the location of the users datasets
func GenerateUsersDataHeatmapRoute(c *gin.Context) {
	rqstUser, err := common.GetRequestingUser(c, false)
	if err != nil {
		// The GIN error is contained in the context
		return
	}

	// Create heatmap based on data from all the users organizations and datasets
	// shared directly with the user
	searchAsRoles := []uuid.UUID{}

	if rqstUser.ID == uuid.Nil {
		// The user is anonymous - only get datasets in the public domain
		searchAsRoles = append(searchAsRoles, common.PublicOrganization)
	} else {
		// If the user is registered get org and additional user roles
		searchAsRoles = append(searchAsRoles, rqstUser.ID)
		usersOrgs, err := rqstUser.Organizations()
		if err != nil {
			c.AbortWithStatusJSON(500, err.Error()); return
		}
		var usersOrgIds []uuid.UUID
		for _, org := range usersOrgs {
			usersOrgIds = append(usersOrgIds, org.ID)
		}
		usersRoles, err := common.RolesFromOrganizations(usersOrgIds)
		if err != nil {
			c.AbortWithStatusJSON(500, err.Error()); return
		}

		for _, role := range usersRoles {
			searchAsRoles = append(searchAsRoles, role)
		}
	}
	heatmap := GenerateDatasetHeatmap(searchAsRoles)

	if len(heatmap) > 0 {
		c.Data(200, gin.MIMEJSON, heatmap)
	} else {
		var emptyResponse []map[string]interface{}
		c.JSON(200, emptyResponse)
	}
	return

}
