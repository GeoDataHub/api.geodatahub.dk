package dataset

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"os"
	"fmt"
	//	"strings"
	require "github.com/stretchr/testify/require"

	gin "github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"

	"gitlab.com/api.geodatahub.dk/tests"
	"gitlab.com/api.geodatahub.dk/common"

)

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)

	// Setup database tables
	tests.MigrateDirection("up", "../utils/migrate")

	// Make initial connection
	_, err := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))

	if err != nil {
		fmt.Print("Unable to connect to database using standard lib")
	}
	// Run tests
	exitcode := m.Run()

	// Remove all tables once test are done
	tests.MigrateDirection("down", "../utils/migrate")

	// Run all test then Exit
	os.Exit(exitcode)
}

func TestPublicizeDatasetRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	realAdm, realUser, _ := tests.GetFakeGeocorpUsers()

	var testcases = []struct {
		// input
		name           string
		params         gin.Param
		requestingUser string
		url            string
		publicize      bool

		// expected
		code           int
		respMsg        string
	}{
		{
			"Not loggedin",
			gin.Param{},
			"",
			"",
			false,
			401,
			"User does not have valid UUID",
		},
		{
			"Not authorized",
			gin.Param{Key: "id", Value: "0e525ead-1a2-4510-b56-fb5d8085b7ba"},
			realAdm,
			fmt.Sprintf("datasets/%s/publicize", uuid.NewV4().String()),
			true,
			400,
			"is not a valid UUID",
		},
		{
			"Unknown dataset",
			gin.Param{Key: "id", Value: uuid.NewV4().String()},
			realAdm,
			fmt.Sprintf("datasets/%s/publicize", uuid.NewV4().String()),
			true,
			404,
			"does not exist",
		},
		{
			"Not allowed to make dataset public in root Fake Geocorp org",
			gin.Param{Key: "id", Value: "10000000-0000-0000-0000-000000000001"},
			realUser,
			"datasets/10000000-0000-0000-0000-000000000001/publicize",
			true,
			403,
			"You are not authorized to make this",
		},
		{
			"Allowed to make dataset public in Fake Geocorp office org",
			gin.Param{Key: "id", Value: "10000000-0000-0000-0000-000000000002"},
			realUser,
			"datasets/10000000-0000-0000-0000-000000000002/publicize",
			true,
			200,
			"The dataset is now public",
		},
		{
			"Making a dataset public twice should do nothing",
			gin.Param{Key: "id", Value: "10000000-0000-0000-0000-000000000002"},
			realUser,
			"datasets/10000000-0000-0000-0000-000000000002/publicize",
			true,
			200,
			"The dataset is now public",
		},
		{
			"Datasets can turn private",
			gin.Param{Key: "id", Value: "10000000-0000-0000-0000-000000000002"},
			realUser,
			"datasets/10000000-0000-0000-0000-000000000002/publicize",
			false,
			200,
			"The dataset is now private",
		},
		{
			"Making a dataset private twice should do nothing",
			gin.Param{Key: "id", Value: "10000000-0000-0000-0000-000000000002"},
			realUser,
			"datasets/10000000-0000-0000-0000-000000000002/publicize",
			false,
			200,
			"The dataset is now private",
		},
		{
			"Other users with permission can also change the status",
			gin.Param{Key: "id", Value: "10000000-0000-0000-0000-000000000002"},
			realAdm,
			"datasets/10000000-0000-0000-0000-000000000002/publicize",
			true,
			200,
			"The dataset is now public",
		},

	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		req, _ := http.NewRequest("GET", testcase.url, nil)
		c.Request = req
		c.Params = []gin.Param{testcase.params}
		if testcase.publicize {
			c.Set("public", "true")
		} else {
			c.Set("private", "true")
		}
		c.Set("UserUUID", testcase.requestingUser)

		HandlePublicizeOrShareDatasetByIDRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		require.Contains(t, writer.Body.String(), testcase.respMsg, "Case %s: Invalid body returned", testcase.name)

		// Check response if succesful
		if c.Writer.Status() == 200 {
			dataset, _ := GetDatasetByID(uuid.FromStringOrNil(testcase.params.Value))
			has, err := dataset.hasRole(common.PublicOrganization)
			require.Nil(t, err, "Case %s: Expected no error. Was %+v", testcase.name, err)

			if testcase.publicize {
				require.True(t, has, "Case %s: Expected a public dataset", testcase.name)
			} else {
				require.False(t, has, "Case %s: Expected a private dataset", testcase.name)
			}
		}
	}
}

func TestShareDatasetRoute(t *testing.T) {
	// Test sharing a dataset with a specific user
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	realAdm, _, _ := tests.GetFakeGeocorpUsers()

	var testcases = []struct {
		// input
		name           string
		params         gin.Param
		datasetID      string
		requestingUser string
		method         string

		// expected
		code           int
		respMsg        string
	}{
		{
			"Not loggedin",
			gin.Param{},
			"",
			"",
			"GET",
			401,
			"User does not have valid UUID",
		},
		{
			"Share an unknown dataset with unknown user",
			gin.Param{Key: "addUser", Value: uuid.NewV4().String()},
			"10000000-0000-0000-0000-000000000055",
			realAdm,
			"GET",
			404,
			"dataset does not exist",
		},
		{
			"Share a known dataset with unknown user",
			gin.Param{Key: "addUser", Value: uuid.NewV4().String()},
			"10000000-0000-0000-0000-000000000003",
			realAdm,
			"GET",
			400,
			"does not exist",
		},
		{
			"Unshare a known dataset with unknown user",
			gin.Param{Key: "delUser", Value: uuid.NewV4().String()},
			"10000000-0000-0000-0000-000000000003",
			realAdm,
			"DELETE",
			400,
			"",
		},
		{
			"Share a known dataset with a known user",
			gin.Param{Key: "addUser", Value: "00000000-0000-0000-0000-000000000004"},
			"10000000-0000-0000-0000-000000000003",
			realAdm,
			"GET",
			200,
			"shared with the user",
		},
		{
			"Unshare a known dataset with a known user",
			gin.Param{Key: "delUser", Value: "00000000-0000-0000-0000-000000000004"},
			"10000000-0000-0000-0000-000000000003",
			realAdm,
			"DELETE",
			200,
			"no longer shared",
		},
		{
			"Share a known dataset with a known user with yourself",
			gin.Param{Key: "addUser", Value: "00000000-0000-0000-0000-000000000004"},
			"10000000-0000-0000-0000-000000000003",
			"00000000-0000-0000-0000-000000000004",
			"GET",
			403,
			"not authorized",
		},
		{
			"Share a known dataset with a known user in an org where you do not have perm",
			gin.Param{Key: "addUser", Value: "00000000-0000-0000-0000-000000000003"},
			"10000000-0000-0000-0000-000000000003",
			"00000000-0000-0000-0000-000000000004",
			"GET",
			403,
			"not authorized",
		},
		{
			"Share a personal dataset you don't own with a known user",
			gin.Param{Key: "addUser", Value: "00000000-0000-0000-0000-000000000003"},
			"10000000-1000-0000-0000-000000000001",
			"00000000-0000-0000-0000-000000000002",
			"GET",
			403,
			"not authorized",
		},
		{
			"Share a personal dataset you own with a known user",
			gin.Param{Key: "addUser", Value: "00000000-0000-0000-0000-000000000003"},
			"10000000-1000-0000-0000-000000000001",
			"00000000-0000-0000-0000-000000000001",
			"GET",
			403,
			"not authorized",
		},
		{
			"Share a known dataset with a unknown user by email",
			gin.Param{Key: "addUser", Value: "non@nobody.com"},
			"10000000-0000-0000-0000-000000000003",
			realAdm,
			"POST",
			400,
			"does not exist",
		},
		{
			"Share a known dataset with a known user by email",
			gin.Param{Key: "addUser", Value: "john@fakeextcorp.com"},
			"10000000-0000-0000-0000-000000000003",
			realAdm,
			"POST",
			200,
			"is now shared",
		},
		{
			"Unshare a known dataset with a known user by email",
			gin.Param{Key: "delUser", Value: "john@fakeextcorp.com"},
			"10000000-0000-0000-0000-000000000003",
			realAdm,
			"DELETE",
			200,
			"is no longer shared",
		},
	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		url := fmt.Sprintf("/dataset/%s/share", testcase.datasetID)
		req, _ := http.NewRequest("GET", url, nil)
		c.Request = req
		c.Params = []gin.Param{gin.Param{Key: "id", Value: testcase.datasetID}}
		c.Set("UserUUID", testcase.requestingUser)
		c.Set(testcase.params.Key, testcase.params.Value)

		HandlePublicizeOrShareDatasetByIDRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		require.Contains(t, writer.Body.String(), testcase.respMsg, "Case %s: Invalid body returned", testcase.name)
	}
}

func TestUpdateDatasetParameterRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	fakeAdmUser, _, _ := tests.GetFakeGeocorpUsers()

	var testcases = []struct {
		name           string
		datasetID      string
		rqstUser       string
		payload        map[string]interface{}
		expectUpdate   bool

		// Expected
		code           int
		msg            string
	}{
		{
			"Anonymous user with valid request",
			"10000000-0000-0000-0000-000000000001",
			uuid.Nil.String(),
			map[string]interface{}{"description":"my desc"},
			false,
			401,
			"Unauthorized",
		},
		{
			"Empty parameters",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{},
			false,
			400,
			"empty",
		},
		{
			"Unknown parameter",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{"myParam": "val"},
			false,
			400,
			"unsupported parameter",
		},
		{
			"Injection attempt",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{"description; DROP TABLES": "val"},
			false,
			400,
			"unsupported parameter",
		},
		{
			"Update roles",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{"roles": "val"},
			false,
			400,
			"cannot update the roles",
		},
		{
			"Publicize not supported",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{"public": "val"},
			false,
			400,
			"call publicize",
		},
		{
			"Unknown dataset",
			uuid.NewV4().String(),
			fakeAdmUser,
			map[string]interface{}{"description": "my desc"},
			false,
			404,
			"not found",
		},
		{
			"Update with wrong param type",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{"projected_geometry":"my desc"},
			false,
			400,
			"does not match",
		},
		{
			"One good and one bad type",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{"description":"my desc", "projected_geometry": "no ok"},
			false,
			400,
			"does not match",
		},
		{
			"One good parameter",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{"description":"my desc"},
			true,
			200,
			"dataset updated",
		},
		{
			"Two good parameters",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			map[string]interface{}{
				"description":"my desc 2",
				"result": map[string]interface{}{"filepath": "d:/"},
			},
			true,
			200,
			"dataset updated",
		},
		{
			"Update dist info to force a unique key violation",
			"10000000-0000-0000-0000-000000000004",
			fakeAdmUser,
			map[string]interface{}{
				"distribution_info": map[string]interface{}{"foo": "One", "bar": 42},
			},
			false,
			400,
			"unique constraint",
		},

	}

	for _, testcase := range testcases {
		datasetUID := uuid.FromStringOrNil(testcase.datasetID)
		datasetBefore, _ := GetDatasetByID(datasetUID)

		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		url := fmt.Sprintf("/datasets/%s", testcase.datasetID)
		bytePayload, err := json.Marshal(testcase.payload)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("PATCH", url, bytes.NewBuffer(bytePayload))
		c.Request = req
		c.Params = []gin.Param{
			gin.Param{Key: "id", Value: testcase.datasetID},
		}
		c.Set("UserUUID", testcase.rqstUser)

		UpdateDatasetSingleParamRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		require.Contains(t, writer.Body.String(), testcase.msg, "Case %s: Invalid body returned", testcase.name)

		datasetAfter, _ := GetDatasetByID(datasetUID)
		if testcase.expectUpdate {
			require.NotEqual(t, datasetBefore, datasetAfter, "Case %s: The datasets should not match", testcase.name)
			for k, v := range testcase.payload {
				switch k {
				case "description":
					require.Equal(t, v, datasetAfter.Description, "Case %s: Expected updated description", testcase.name)
				}
			}
		} else {
			require.Equal(t, datasetBefore, datasetAfter, "Case %s: The datasets should match", testcase.name)
		}
	}
}

func TestUpdateEntireDatasetRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")
	fakeAdmUser, fakeNormUser, fakeOrg := tests.GetFakeGeocorpUsers()

	var testcases = []struct {
		name           string
		datasetID      string
		rqstUser       string
		dataset        Dataset

		// Expected
		code           int
		msg            string
	}{
		{
			"Empty parameters",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			Dataset{},
			400,
			"does not match",
		},
		{
			"Missing owner should fail",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			Dataset{
				Datatype: "https://schemas.geodatahub.dk/test.json",
				Description: "My desc",
				DistributionInfo: common.JSONB{},
				Result: common.JSONB{},
				ProjectedGeometry: *NewPointGeometry([]float64{101.0, 0.0}),
			},
			400,
			"does not match",
		},
		{
			"Missing owner should fail",
			"10000000-0000-0000-0000-000000000001",
			uuid.Nil.String(),
			Dataset{
				Datatype: "https://schemas.geodatahub.dk/test.json",
				Description: "My desc",
				DistributionInfo: common.JSONB{},
				Result: common.JSONB{},
				ProjectedGeometry: *NewPointGeometry([]float64{101.0, 0.0}),
			},
			401,
			"Unauthorized",
		},
		{
			"Make dataset private to a different user",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			Dataset{
				Owner: uuid.FromStringOrNil(fakeNormUser),
				Datatype: "https://schemas.geodatahub.dk/test.json",
				Description: "My desc",
				DistributionInfo: common.JSONB{},
				Result: common.JSONB{},
				ProjectedGeometry: *NewPointGeometry([]float64{101.0, 0.0}),
			},
			200,
			"dataset updated",
		},
		{
			"Moving dataset to org is not allowed",
			"10000000-0000-0000-0000-000000000001",
			fakeAdmUser,
			Dataset{
				Owner: uuid.FromStringOrNil(fakeAdmUser),
				Organization: uuid.FromStringOrNil(fakeOrg),
				Datatype: "https://schemas.geodatahub.dk/test.json",
				Description: "My desc",
				DistributionInfo: common.JSONB{},
				Result: common.JSONB{},
				ProjectedGeometry: *NewPointGeometry([]float64{101.0, 0.0}),
			},
			403,
			"not allowed",
		},
		{
			"Moving dataset to org is not allowed",
			"10000000-0000-0000-0000-000000000001",
			fakeNormUser,
			Dataset{
				Owner: uuid.FromStringOrNil(fakeAdmUser),
				Organization: uuid.FromStringOrNil(fakeOrg),
				Datatype: "https://schemas.geodatahub.dk/test.json",
				Description: "My desc",
				DistributionInfo: common.JSONB{},
				Result: common.JSONB{},
				ProjectedGeometry: *NewPointGeometry([]float64{101.0, 0.0}),
			},
			200,
			"dataset updated",
		},
		{
			"Update description and results",
			"10000000-0000-0000-0000-000000000001",
			fakeNormUser,
			Dataset{
				Owner: uuid.FromStringOrNil(fakeAdmUser),
				Organization: uuid.FromStringOrNil(fakeOrg),
				Datatype: "https://schemas.geodatahub.dk/test.json",
				Description: "My desc",
				DistributionInfo: common.JSONB{"Corp ID": uuid.NewV4(), "foo": "one", "bar": 43},
				Result: common.JSONB{"Filepath": "my files"},
				ProjectedGeometry: *NewPointGeometry([]float64{101.0, 0.0}),
			},
			200,
			"dataset updated",
		},
		{
			"Update dist info to force a unique key violation",
			"10000000-0000-0000-0000-000000000004",
			fakeAdmUser,
			Dataset{
				Owner: uuid.FromStringOrNil(fakeAdmUser),
				Organization: uuid.FromStringOrNil("10000000-0000-0000-0000-000000000002"),
				Datatype: "https://schemas.geodatahub.dk/test_unique_constraint.json",
				Description: "My desc",
				DistributionInfo: common.JSONB{"foo": "OnE", "bar": 42},
				Result: common.JSONB{"Filepath": "my files"},
				ProjectedGeometry: *NewPointGeometry([]float64{101.0, 0.0}),
			},
			400,
			"unique constraint",
		},

	}

	for _, testcase := range testcases {
		datasetUID := uuid.FromStringOrNil(testcase.datasetID)
		datasetBefore, _ := GetDatasetByID(datasetUID)

		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		url := fmt.Sprintf("/datasets/%s", testcase.datasetID)

		bytePayload, err := json.Marshal(testcase.dataset)
		if err != nil {
			panic(err)
		}
		req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(bytePayload))
		c.Request = req

		c.Params = []gin.Param{
			gin.Param{Key: "id", Value: testcase.datasetID},
		}
		c.Set("UserUUID", testcase.rqstUser)

		UpdateEntireDatasetRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		require.Contains(t, writer.Body.String(), testcase.msg, "Case %s: Invalid body returned", testcase.name)

		if testcase.code == 200 {
			// Test response
			datasetAfter, _ := GetDatasetByID(datasetUID)
			require.NotEqual(t, datasetBefore, datasetAfter, "Case %s: The datasets should not match", testcase.name)
			//require.Equal(t, testcase.dataset, datasetAfter, "Case %s: The datasets should match test example", testcase.name)
		}
	}
}

// TestDatasetSearchPerms Ensure the search returns the expected datasets given different
// users and permission levels
func TestDatasetSearchPerms(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	fakeAdmUser, fakeNormUser, _ := tests.GetFakeGeocorpUsers()
	anonUser := uuid.Nil.String()

	var testcases = []struct {
		name           string
		query          string
		rqstUser       string
		publicSearch   bool
		searchOrg      []string

		// Expected
		code           int
		numDatasets    int
		msg            string
	}{
		{
			"Empty search",
			"{}",
			fakeAdmUser,
			false,
			[]string{},
			400,
			-1,
			"did not contain any valid parameters",
		},

		// Anonymous cases
		{
			"Anonymous search with invalid parameters",
			"{}",
			anonUser,
			true,
			[]string{},
			400,
			-1,
			"did not contain any valid parameters",
		},
		{
			"Anonymous search requests orgs",
			"{\"https://schemas.geodatahub.dk/test_unique_constraint.json\":\"\"}",
			anonUser,
			false,
			[]string{"10000000-0000-0000-0000-000000000002"},
			400,
			-1,
			"Anonymous users cannot",
		},
		{
			"Anonymous search requests public role",
			"{\"https://schemas.geodatahub.dk/test_unique_constraint.json\":\"\"}",
			anonUser,
			false,
			[]string{common.PublicOrganization.String()},
			400,
			-1,
			"Anonymous users cannot",
		},
		{
			// Anonymous users have to search by schema not orgs since they don't belong to any orgs
			"Anonymous search limits to roles with public datasets",
			"{\"https://schemas.geodatahub.dk/test_unique_constraint.json\":\"\"}",
			anonUser,
			false,
			[]string{"10000000-0000-0000-0000-000000000002"}, // This org has a public dataset
			400,
			-1,
			"Anonymous users cannot",
		},
		{
			// No dataset in the public domain matches this query
			"Anonymous search with empty request",
			"{\"https://schemas.geodatahub.dk/empty.json\":\"\"}",
			anonUser,
			false,
			[]string{},
			200,
			0,
			"",
		},
		{
			"Anonymous search for only private data is ignored",
			"{\"https://schemas.geodatahub.dk/test_unique_constraint.json\":\"\"}",
			anonUser,
			false,
			[]string{},
			200,
			1,
			"",
		},
		{
			"Anonymous geographic search in area with no public datasets",
			"{\"geometry\":{\"$contains\":\"POLYGON((-84.364 76.92,-81.551 76.92,-81.551 77.298,-84.36 77.298,-84.364 76.92))\"}}",
			anonUser,
			true,
			[]string{},
			200,
			0,
			"",
		},
		{
			"Anonymous geographic search in area with mixed datasets",
			"{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}}",
			anonUser,
			true,
			[]string{},
			200,
			1,
			"",
		},

		// Real user cases
		{
			"The user is not a member of the requested org",
			"{\"https://schemas.geodatahub.dk/test_unique_constraint.json\":\"\"}",
			"00000000-0000-0000-0000-000000000004",
			false,
			[]string{"10000000-0000-0000-0000-000000000002"},
			403,
			0,
			"not allowed",
		},
		{
			"Search outside users own org but in a org with shared dataset",
			"{\"https://schemas.geodatahub.dk/test_unique_constraint.json\":\"\"}",
			"00000000-0000-0000-0000-000000000003",
			false,
			[]string{"10000000-0000-0000-0000-000000000002"},
			403,
			1,
			"not allowed",
		},
		{
			"Default search - only see private dataset",
			"{\"https://schemas.geodatahub.dk/test.json\":\"\"}",
			fakeAdmUser,
			false,
			[]string{},
			200,
			1,
			"",
		},
		{
			"Default search - norm user cannot see adm private datasets",
			"{\"https://schemas.geodatahub.dk/test.json\":\"\"}",
			fakeNormUser,
			false,
			[]string{},
			200,
			0,
			"",
		},
		{
			"Default search - can see datasets in child orgs",
			"{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}}",
			fakeNormUser,
			false,
			[]string{},
			200,
			4,
			"",
		},
		{
			"Default search - can see datasets in requested orgs only - not nested",
			"{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}}",
			fakeNormUser,
			false,
			[]string{"10000000-0000-0000-0000-000000000003"}, // Office org
			200,
			1,
			"",
		},
		{
			"Search by user outside org with a single shared dataset",
			"{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}}",
			"00000000-0000-0000-0000-000000000003",
			false,
			[]string{},
			200,
			2,
			"",
		},
		{
			"Public search by user outside org with a single shared dataset",
			"{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}}",
			"00000000-0000-0000-0000-000000000003",
			true,
			[]string{},
			200,
			3,
			"",
		},
		{
			"Limited organization search by user in non-root org",
			"{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}}",
			"00000000-0000-0000-0000-000000000003",
			true,
			[]string{},
			200,
			3,
			"",
		},

	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		url := fmt.Sprintf("/datasets?q=%s", testcase.query)
		if testcase.publicSearch {
			url = url + "&public=true"
		}
		if len(testcase.searchOrg) > 0 {
			url = url + fmt.Sprintf("&organizations=%s", testcase.searchOrg[0])
		}

		req, _ := http.NewRequest("GET", url, nil)
		c.Request = req
		c.Set("UserUUID", testcase.rqstUser)

		GetAndSearchDatasetRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		if testcase.code == 200 {
			var response []map[string]interface{}
			if err := json.Unmarshal([]byte(writer.Body.String()), &response); err != nil {
				panic(err)
			}

			require.Equal(t, testcase.numDatasets, len(response), "Case %s: number of datasets", testcase.name)
		} else {
			require.Contains(t, writer.Body.String(), testcase.msg, "Case %s: Invalid body returned", testcase.name)
		}

	}
}

func TestGenerateUsersDataHeatmapRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	var testcases = []struct {
		name           string
		rqstUser       string

		// Expected
		code           int
		numClusters    int      // Number of clusters
		maxWeight      float64  // Weight of the largest cluster
	}{
		{
			"Anonymous user",
			"",
			200,
			1,
			1,
		},
		{
			"Unknown user",
			"00000000-0000-0000-0000-000000000099",
			500,
			0,
			0,
		},
		{
			"User with no datasets",
			"00000000-0000-0000-0000-000000000005",
			200,
			0,
			0,
		},
		{
			"Query with known user",
			"00000000-0000-0000-0000-000000000004",
			200,
			1,
			2,
		},
	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		req, _ := http.NewRequest("GET", "/datasets/heatmap", nil)
		c.Set("UserUUID", testcase.rqstUser)
		c.Request = req

		GenerateUsersDataHeatmapRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		if c.Writer.Status() == 200 {
			// Test response
			var response []map[string]interface{}
			if err := json.Unmarshal([]byte(writer.Body.String()), &response); err != nil {
				panic(fmt.Sprintf("%s - Response was %s", err, writer.Body.String()))
			}

			require.Equal(t, testcase.numClusters, len(response), "Case %s: found %s number of clusters. Expected: %s", testcase.name, len(response), testcase.numClusters)
			var maxWeight float64 = 0
			for clusterNum := range response {
				weight := response[clusterNum]["weight"].(float64)
				if weight > maxWeight {
					maxWeight = weight
				}
			}
			require.Equal(t, testcase.maxWeight, maxWeight, "Case %s: found a max weight of %f. Expected: %f", testcase.name, maxWeight, testcase.maxWeight)
		}
	}
}
