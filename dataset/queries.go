package dataset

import (
	"encoding/json"
	"fmt"
	"strings"
	"reflect"

	pg_esp "github.com/tj/go-pg-escape"
)

func buildSearchQuery(query []string) (string, []interface{}, int, error) {
	whereQuery := ""
	var queryParameters []interface{} //[]sql.NamedArg
	var parameterCount = 1
	if len(query) == 0 {
		return "", queryParameters, 0, fmt.Errorf("Cannot search without parameters")
	}

	// Build up query

	// --- Search by key-value ----
	// Key-value search uses the distribution_info column
	// to search the JSON payload for a matching key-value
	// pair.
	// The interface is implementing following the design by
	// REST DB - https://restdb.io/docs/querying-with-the-api
	// The method should expect the q parameter to contain a JSON
	// payload to decode.
	if k := query; k != nil {
		whereQuery = ""
		var querystr map[string]interface{}
		rawstring := strings.Trim(k[0], `\`)
		err := json.Unmarshal([]byte(rawstring), &querystr)
		if err != nil {
			return "", queryParameters, 0, fmt.Errorf("The query string is not valid JSON. Please validate and retry. Received %v", rawstring)
		}

		var schemaNum int = 0

		// Process the geometry part of the query first since all other queries are bound with an AND to the
		// geometry - i.e. dataset within the geometry AND (other constraints)
		if querystr["geometry"] != nil {
			iface := querystr["geometry"].(map[string]interface{})
			if len(iface) > 1 {
				return "", queryParameters, 0, fmt.Errorf("Only one parameter is allowed for geometry search")
			}

			if iface["$contains"] != nil {
				fmt.Printf("Geometry Query string was %s\n", iface["$contains"])
				if strings.Contains(strings.ToLower(fmt.Sprintf("%v", iface["$contains"])), "polygon") {
					whereQuery = whereQuery + fmt.Sprintf("ST_Intersects(ST_GeomFromText( $%d, 4326), projected_geometry)", parameterCount)
					queryParameters = append(queryParameters, iface["$contains"])
					parameterCount++
					if (len(querystr) > 1) {
						// More queries are to come
						whereQuery += " AND ("
					}
				} else {
					return "", queryParameters, 0, fmt.Errorf("$contains only supports polygon geometries. Received %s", iface["$contains"])
				}
			} else {
				return "", queryParameters, 0, fmt.Errorf("Geographic search only supports the parameters: $contains")
			}
		}

		// Iterate all first level key-value pairs and create
		// the corresponding WHERE-clause
		// NOTE: The current solution does not support
		// querying with native types. This will
		// require introducing schemas to support.
		for schema, val := range querystr {
			// Removing the JSON structure and creating
			// it again might seem as going over the bridge
			// for water but when introduction the logical operators
			// it is the only way
			var s string

			if reflect.TypeOf(val).Kind() != reflect.Map {
				if strings.ToLower(fmt.Sprintf("%v", val)) == "" {
					// Is the search for all datasets in a given schema
					fmt.Printf("Single schema search with schema %s and val %s\n", schema, val)
					// Combine the current schema into the where statement
					// All queries after the first are added with OR logical
					// operators.
					// Ex. datatype='myschema1' OR datatype='myschema2'
					if schemaNum == 0 {
						// Ensure all the datatype specific logic is always wrapped in ()
						whereQuery += fmt.Sprintf(" datatype = $%d ", parameterCount)
						parameterCount++
						queryParameters = append(queryParameters, schema)
					} else {
						whereQuery += fmt.Sprintf(" OR datatype = $%d", parameterCount)
						parameterCount++
						queryParameters = append(queryParameters, schema)
					}
					schemaNum++
				} else {
					// The values is a nested object
					return "", queryParameters, 0, fmt.Errorf("The query must be on the form {'schema': {'parameter':'value'}.")
				}
			} else {

				switch strings.ToLower(schema) {
				// Process reserved schemas
				case "geometry":
					// The geometry case was handled previously
					continue
				default:

					// Convert query to native go map object
					curQueryMap := val.(map[string]interface{});

					if len(curQueryMap) == 0 {
						// User used `schema: {}` syntax and not `schema: ""` syntax. Handle special case
						// IMPORTANT: This code is copied from above
						if schemaNum == 0 {
							// Ensure all the datatype specific logic is always wrapped in ()
							whereQuery += fmt.Sprintf(" datatype = $%d ", parameterCount)
							parameterCount++
							queryParameters = append(queryParameters, schema)
						} else {
							whereQuery += fmt.Sprintf(" OR datatype = $%d", parameterCount)
							parameterCount++
							queryParameters = append(queryParameters, schema)
						}
					} else {
						// Expand query for this schema since the user query was `schema: {param1: val1, ..}`
						s, err = jsonToSQL(curQueryMap)
						if err != nil {
							return "", queryParameters, 0, err
						}

						// Combine the current schema into the where statement
						// All queries after the first are added with OR logical
						// operators.
						// Ex. datatype='myschema1' OR datatype='myschema2'
						if schemaNum == 0 {
							whereQuery += fmt.Sprintf(" (datatype = $%d AND %s)", parameterCount, s)
							parameterCount++
							queryParameters = append(queryParameters, schema)
						} else {
							whereQuery += fmt.Sprintf(" OR (datatype = $%d AND %s)", parameterCount, s)
							parameterCount++
							queryParameters = append(queryParameters, schema)

						}
					}
					schemaNum++
				}
			}

			if schemaNum >= len(querystr)-1 && querystr["geometry"] != nil {
				whereQuery += ")"
			}
		}

		//		delete(q, "q")
	}

	// Verify that all parameters from the URL
	// were used
	/*
	if len(q) > 0 {
		var keys string
		for k := range q {
			keys = keys + k + " "
		}
		return "", queryParameters, fmt.Errorf("the parameter(s) %sis not supported or requires additional parameters. Please read the documentation for all valid parameter combinations", keys)
	}
	*/
	return whereQuery, queryParameters, parameterCount, nil
}

// Convert nested query to string
func jsonToSQL(query map[string]interface{}) (string, error) {

	var fullQuery string = ""
	var jsonQuery string = ""
	var itt int = 0
	for key, val := range query {

		// The value is either the string to match of a reserved expression in the form {'$exp': 'value'}
		fmt.Printf("type of query %s\n", reflect.TypeOf(val).Kind())
		if reflect.TypeOf(val).Kind() == reflect.Map {
			for operator, operatorValue := range val.(map[string]interface{}) {
				switch strings.ToLower(operator) {
				case "$lt":
					jsonQuery = pg_esp.Escape(`(distribution_info->>'%s')::float < %s`, key, fmt.Sprintf("%v", operatorValue))
				case "$gt":
					jsonQuery = pg_esp.Escape(`(distribution_info->>'%s')::float > %s`, key, fmt.Sprintf("%v", operatorValue))
				default:
					return "", fmt.Errorf("the operator is not implemented yet")
				}
			}

		} else {
			// Simple AND logic expression
			jsonQuery = pg_esp.Escape(`distribution_info @> '{"%s":"%s"}'`, key, fmt.Sprintf("%v", val))
		}

		if itt == 0 {
			fullQuery = fmt.Sprintf("%s", jsonQuery)
		} else {
			fullQuery = fmt.Sprintf("%s AND %s", fullQuery, jsonQuery)
		}
		itt++
	}
	return fullQuery, nil
}
