package common

import (
	"errors"
	"fmt"
	"time"
	"database/sql"
    pg "github.com/lib/pq"

	"github.com/satori/go.uuid"
)

// AWS Context Header
type AWSContext struct {
	AccountId  string
	Authorizer AWSAuthorizer `json:"authorizer"`
}

type AWSAuthorizer struct {
	Claims AWSClaims `json:"claims"`
}

type AWSClaims struct {
	At_hash  string `json:"at_hash"`
	Username string `json:"cognito:username"`
	Email    string `json:"email"`
}

// Headers describing an authorized jwt token
type AuthHeader struct {
	// Custom header added by the API gateway proxy layer
	Context       AWSContext `header:"X-Golambdaproxy-Apigw-Context"`
	Authorization string     `header:"Authorization"`
}

type User struct {
	ID          uuid.UUID `json:"id"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	Email       string // Unique to user
	Roles       []uuid.UUID
}

// UserPermissions defines which permissions a given role has
type RolePermissions struct {
	id               uuid.UUID
	name             string
	organization     uuid.UUID
	canRead          bool
	canWrite         bool
	canShare         bool
	canManageUsers   bool
	canManageOrg     bool
}

func (r RolePermissions) Write() (bool) {
	return r.canWrite
}

func (r RolePermissions) Share() (bool) {
	return r.canShare
}

type Organization struct {
	ID      uuid.UUID
	Name    string
}

// CreateUser Create a new user in the database
func CreateUser(db *sql.DB, user User) (uuid.UUID, error) {
	var uid uuid.UUID
	fmt.Printf("Creating new user with email %s\n", user.Email)

	// Create user with predefined ID
	err := db.QueryRow("INSERT INTO users (id, email) VALUES ($1::UUID, $2) RETURNING id;", user.ID, user.Email).Scan(&uid)
	if err != nil {
		return uid, ReportInternalError(err)
	}

	return uid, nil
}

// GetUser Returns the user given the userId
func GetUser(uid uuid.UUID) (User, error) {
	var user User
	db := GetStandardDB()
	if db == nil {
		return User{}, ReportInternalError(err)
	}

	err := db.QueryRow(`
      SELECT id, created_at, updated_at, email, roles
      FROM users
      WHERE id = $1`,
		uid).Scan(&user.ID, &user.CreatedAt, &user.UpdatedAt, &user.Email, pg.Array(&user.Roles))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return User{}, fmt.Errorf("No user exists with ID %s", uid.String())
		}
		return User{}, ReportInternalError(err)
	}

	return user, nil
}

// GetUserByEmail Lookup the user by the email used during signup
func GetUserByEmail(email string) (User, error) {
	db := GetStandardDB()
	if db == nil {
		return User{}, ReportInternalError(err)
	}

	fmt.Printf("Get information for user %s\n", email)
	var user User
	err := db.QueryRow(`
      SELECT id, created_at, updated_at, email, roles
      FROM users
      WHERE email = $1`,
		email).Scan(&user.ID, &user.CreatedAt, &user.UpdatedAt, &user.Email, pg.Array(&user.Roles))

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			// Empty rows only indicates the user isn't allowed
			return User{}, fmt.Errorf("No user exists with email %s", email)
		}
		return User{}, ReportInternalError(err)
	}

	return user, nil
}

// Organizations

// GetOrganization Returns the organization given the uuid
func GetOrganization(oid uuid.UUID) (Organization, error) {
	var org Organization
	db := GetStandardDB()
	if db == nil {
		return org, ReportInternalError(err)
	}

	err := db.QueryRow(`
      SELECT id, name
      FROM organizations
      WHERE id = $1`,
		oid).Scan(&org.ID, &org.Name)
	if err != nil {
		return org, ReportInternalError(err)
	}

	return org, nil
}

// createOrganization Create a new organization with name and belonging to the owner uuid
func createOrganization(name string, ownerUUID uuid.UUID) (uuid.UUID, error) {
	db := GetStandardDB()
	if db == nil {
		return uuid.Nil, errors.New("Unable to connect to database")
	}

	tx, err := db.Begin()
	if err != nil {
		return uuid.Nil, errors.New("Unable to create database transaction")
	}
	defer tx.Rollback()


	var orgUUID uuid.UUID
	err = tx.QueryRow(`
           INSERT INTO organizations (name, owner, num_users, subscription_plan)
           VALUES ($1, $2::UUID, 1, NULL) RETURNING id;`,
		name, ownerUUID).Scan(&orgUUID)

	roleName :=  fmt.Sprintf("%s admins", name)
	var roleUUID uuid.UUID
	err = tx.QueryRow(`
           INSERT INTO roles (name, organization, can_read, can_write, can_share, can_manage_users, can_manage_org)
           VALUES ($1, $2, true, true, true, true, true) RETURNING id;`,
		roleName, orgUUID).Scan(&roleUUID)
	if err != nil {
		return uuid.Nil, err
	}

	_, err = tx.Exec(`
           UPDATE users
           SET roles = array_append(roles, $1)
           WHERE id = $2;`,
		roleUUID, ownerUUID)
	if err != nil {
		return uuid.Nil, err
	}

	err = tx.Commit()
	if err != nil {
		return uuid.Nil, err
	}

	return orgUUID, nil
}

// addUserToRole If this function doesn't return a error the user was added with success
func (u User) addToRole(orgID uuid.UUID, roleID uuid.UUID) error {
	db := GetStandardDB()
	if db == nil {
		return ReportInternalError(errors.New("Unable to connect to database"))
	}

	tx, err := db.Begin()
	if err != nil {
		return ReportInternalError(errors.New("Unable to create database transaction"))
	}
	defer tx.Rollback()

	// Ensure the role exists and that it belongs to the organization that
	// is requested - The users permissions within that Org has already been
	// verified
	var	roleOrgUUID uuid.UUID
	err = tx.QueryRow(`
        SELECT organization
        FROM Roles
        WHERE id = $1;
    `, roleID).Scan(&roleOrgUUID)
	if err !=  nil {
		if errors.Is(err, sql.ErrNoRows) {
			return fmt.Errorf("The role %s does not exist", roleID)
		}
		return ReportInternalError(err)
	}
	if roleOrgUUID != orgID || roleOrgUUID == uuid.Nil {
		return fmt.Errorf("The role %s does not exist or belongs to a different organization", roleID)
	}

	// Remove user from any existing role in organization
	var existingRole uuid.UUID
	err = tx.QueryRow(`
            SELECT id
            FROM roles
            WHERE organization = $1
            AND ARRAY[id] && (SELECT roles FROM users WHERE id = $2)`,
		orgID, u.ID).Scan(&existingRole)
	if err != nil {
		return ReportInternalError(err)
	}

	// Remove the user from any existing roles in the org and add them to the new role
	_, err = tx.Exec(`
        UPDATE Users
        SET roles = array_append(array_remove(roles, $3), $1)
        WHERE id = $2`,
		roleID, u.ID, existingRole)
	if err != nil {
		return ReportInternalError(err)
	}

	// If the user didn't belong to the organization before make sure they
	// count now
	if existingRole != uuid.Nil {
		if _, err = tx.Exec(`
        UPDATE Organizations
        SET num_users = num_users + 1
        WHERE id = $1`,
			orgID); err != nil {
			return ReportInternalError(err)
		}
	}

	err = tx.Commit()
	if err != nil {
		return ReportInternalError(err)
	}
	return nil
}

// Roles

// RolesFromOrganizations Returns all roles assoicated with the list of organizations
func RolesFromOrganizations(orgs []uuid.UUID) ([]uuid.UUID, error) {
	db := GetStandardDB()

	roles := []uuid.UUID{}
	if len(orgs) == 0 {
		return roles, nil
	}

	rows, err := db.Query(`
        SELECT r.id
        FROM roles AS r
        WHERE ARRAY[r.organization] && $1
        `, pg.Array(orgs))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return roles, nil
		}
		return roles, ReportInternalError(err)
	}

	defer rows.Close()
	for rows.Next() {
		var role uuid.UUID
		err := rows.Scan(&role)
		if err != nil {
			return roles, ReportInternalError(err)
		}
		roles = append(roles, role)
	}
	if rows.Err() != nil {
		return roles, ReportInternalError(err)
	}

	return roles, nil
}

//Organizations Get all organizations the user belongs to
func (u User) Organizations() ([]Organization, error) {
	db := GetStandardDB()

	organizations := []Organization{}
	rows, err := db.Query(`
        -- Direct and inherited organizations
        SELECT oo.id, oo.name
        FROM roles AS r
        -- Direct org membership
        JOIN organizations AS o ON r.organization = o.id
        -- Inherited org memberships
        INNER JOIN organizations AS oo ON oo.org_hierarchy <@ o.org_hierarchy
        WHERE ARRAY[r.id] && (
              SELECT roles
              FROM users
              WHERE id = $1)
        `, u.ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return organizations, nil
		}
		return organizations, ReportInternalError(err)
	}

	defer rows.Close()
	for rows.Next() {
		var org Organization
		err := rows.Scan(&org.ID, &org.Name)
		if err != nil {
			return organizations, ReportInternalError(err)
		}
		organizations = append(organizations, org)
	}
	if rows.Err() != nil {
		return organizations, ReportInternalError(err)
	}

	return organizations, nil
}

//GetRoles returns all roles a user belongs to including inherited from organizational relationships
//and the users own ID. All errors from this function are safe to show to the end-user. Anonymous
//users are ignored.
func (u User) GetRoles() ([]uuid.UUID, error) {
	searchAsRoles := []uuid.UUID{}

	// If the user is anonymous they don't belong to any groups and are ignored
	if u.ID != uuid.Nil {
		// If the user is registered get org and additional user roles
		searchAsRoles = append(searchAsRoles, u.ID)
		usersOrgs, err := u.Organizations()
		if err != nil {
			return searchAsRoles, err
		}
		var usersOrgIds []uuid.UUID
		for _, org := range usersOrgs {
			usersOrgIds = append(usersOrgIds, org.ID)
		}
		usersRoles, err := RolesFromOrganizations(usersOrgIds)
		if err != nil {
			return searchAsRoles, err
		}

		for _, role := range usersRoles {
			searchAsRoles = append(searchAsRoles, role)
		}
	}

	return searchAsRoles, nil
}

//Can Returns true if the user has the permission within an organization
func (u User) Can(org uuid.UUID, permission string) (bool, error) {
	knownPermissions := []string{
		"can_read", "can_write", "can_share",
		"can_see_results", "can_see_users",
		"can_manage_users", "can_manage_org"}
	if !Contains(knownPermissions, permission) {
		return false, fmt.Errorf("No permission named: %s", permission)
	}

	role, err := u.OrganizationPermissions(org)
	if err != nil {
		return false, err
	}

	// Validate permission
	switch permission {
	case "can_read":
		return role.canRead, nil
	case "can_write":
		return role.canWrite, nil
	case "can_share":
		return role.canShare, nil
	case "can_manage_users":
		return role.canManageUsers, nil
	case "can_manage_org":
		return role.canManageOrg, nil
	default:
		return false, fmt.Errorf("Unknown permission %s", permission)
	}

	return false, nil
}

//OrganizationPermissions Returns all the permissions a user has within an organization
func (u User) OrganizationPermissions(org uuid.UUID) (RolePermissions, error) {
	var role RolePermissions

	db := GetStandardDB()
	if db == nil {
		return role, errors.New("Unable to connect to database")
	}

	err = db.QueryRow(`
             SELECT COALESCE(bool_or(can_read), false)         AS can_read,
                    COALESCE(bool_or(can_write), false)        AS can_write,
                    COALESCE(bool_or(can_share), false)        AS can_share,
                    COALESCE(bool_or(can_manage_users), false) AS can_manage_users,
                    COALESCE(bool_or(can_manage_org), false)   AS can_manage_org
             FROM roles as r
             INNER JOIN (
               SELECT r.id as id
               FROM organizations AS o
               JOIN roles AS r ON r.organization = o.id
               WHERE org_hierarchy @> (
                 SELECT org_hierarchy
                 FROM organizations
                 WHERE id = $1)
               ) AS orgs ON orgs.id = r.id
            WHERE ARRAY[r.id] && (SELECT roles FROM users WHERE id = $2)`,
		org, u.ID).Scan(
		&role.canRead, &role.canWrite, &role.canShare, &role.canManageUsers, &role.canManageOrg)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			// Empty rows only indicates the user isn't allowed
			return role, nil
		}
		return role, ReportInternalError(err)
	}
	return role, nil
}
