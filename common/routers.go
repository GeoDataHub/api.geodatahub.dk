package common

import (
	"log"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
)

func Ping(c *gin.Context) {
	c.String(200, "pong")
}

// Always return a 200 response
func Return200(c *gin.Context) {
	c.String(200, "success")
}

func getRequestingUser(c *gin.Context) uuid.UUID {
	requestingUserUUID, err := uuid.FromString(c.GetString("UserUUID")) // Get requesting user
	if err != nil {
		log.Printf("A user made a request without a valid UUID. Recieved: %s", err)
		c.AbortWithStatusJSON(401, gin.H{"Error": "User does not have valid UUID. Please contact support@geodatahub.dk"})
		return uuid.Nil
	}
	return requestingUserUUID
}

func GetUserByIDRoute(c *gin.Context) {
	var requestingUserUUID uuid.UUID
	if requestingUserUUID = getRequestingUser(c); requestingUserUUID == uuid.Nil {
		return
	}

	var userUUID uuid.UUID
	if c.Param("id") == "" {
		// The user wants to see his/her own data
		log.Printf("No param")
		userUUID = requestingUserUUID
	} else {
		userUUID, err = uuid.FromString(c.Param("id"))
		if err != nil {
			log.Printf("Unable to convert string %s to UUID", c.Param("id"))
			c.AbortWithStatusJSON(400, gin.H{"Error": "Invalid user identifier - Must be UUIDv4 format"})
			return
		}

		if userUUID != requestingUserUUID {
			// TODO: Check permissions - is the user allowed to lookup this other user?
			c.AbortWithStatusJSON(501, gin.H{"Error": "Viewing other users data is not supported yet."})
			return
		}
	}


	usr, err := GetUser(userUUID)
	if err != nil {
		log.Printf("Failed to get user data with %s", err)
		c.AbortWithStatusJSON(400, gin.H{"error": "Unable to get user"})
		return
	}

	c.JSON(200, usr)
	return
}


// Organizations
func CreateOrganizationRoute(c *gin.Context) {
	var requestingUserUUID uuid.UUID
	if requestingUserUUID = getRequestingUser(c); requestingUserUUID == uuid.Nil {
		return
	}

	organizationName, exists := c.GetQuery("name")
	if !exists {
		c.AbortWithStatusJSON(400, gin.H{"Error": "An organization must have a name. None was supplied."})
		return
	}

	organizationParrent := c.Query("parrent")
	if organizationParrent != "" {
		c.AbortWithStatusJSON(501, gin.H{"Error": "The organizational parrent feature is not implemented yet."})
		return
	}

	newOrgID, err := createOrganization(organizationName, requestingUserUUID)
	if err != nil {
		log.Printf("Request to create a new organization %s failed with error: %s", organizationName, err)
		c.AbortWithStatusJSON(500, gin.H{"Error": "Database error. Please contact support@geodatahub.dk with the time you made the request."})
		return
	}
	c.JSON(200, newOrgID)
	return
}

//GetuserOrganizationsRoute Get all organizations the user is a member of
func GetUserOrganizationsRoute(c *gin.Context) {
	rqstUser, err := GetRequestingUser(c, true)
	if err != nil {
		return
	}

	orgs, err := rqstUser.Organizations()
	if err != nil {
		c.AbortWithStatusJSON(500, err.Error()); return
	}

	c.JSON(200, orgs)
}

// InviteUserToOrganizationRoute This route is called under /organization/uuid/users to invite a user by id
func InviteUserToOrganizationRoute(c *gin.Context) {
	var requestingUserUUID uuid.UUID
	if requestingUserUUID = getRequestingUser(c); requestingUserUUID == uuid.Nil {
		c.AbortWithStatusJSON(401, gin.H{"Error": "Request contains no user information. Please login."}); return
	}
	requestingUser := User{ID:requestingUserUUID}

	orgUUID, err := uuid.FromString(c.Params.ByName("orgId"))
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"Error": "The organization ID wasn't valid."}); return
	}

	canDo, err := requestingUser.Can(orgUUID, "can_manage_users")
	if err != nil {
		c.AbortWithStatusJSON(403, gin.H{"Error": err.Error()}); return
	}

	if !canDo {
		c.AbortWithStatusJSON(403, gin.H{"Error": "You don't have permission to add users to that organization or the organization doesn't exist."}); return
	}

	userEmail, exists := c.GetQuery("email")
	if !exists {
		c.AbortWithStatusJSON(400, gin.H{"Error": "Missing email of user to invite."}); return
	}
	roleStr, exists := c.GetQuery("roleID"); roleUUID, err := uuid.FromString(roleStr)
	if !exists || err != nil {
		c.AbortWithStatusJSON(400, gin.H{"Error": fmt.Sprintf("Missing role to give the user or role isn't a valid UUID v4. Got: '%s'", roleStr)}); return
	}

	grantUser, err := GetUserByEmail(userEmail)
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"Error": err.Error()}); return
	}

	if requestingUser.ID == grantUser.ID {
		c.AbortWithStatusJSON(400, gin.H{"Error": "You cannot add yourself to an organization"}); return
	}

	if grantUser.ID == uuid.Nil {
		// TODO: Will this give problems? In this way you are debug if a given user exists - We are leaking metadata
		c.AbortWithStatusJSON(400, gin.H{"error": fmt.Sprintf("No user exists with email %s", userEmail)}); return
	}

	err = grantUser.addToRole(orgUUID, roleUUID)
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"error": err.Error()}); return
	}

	c.JSON(200, gin.H{"success": fmt.Sprintf("The user %s was added with success.", userEmail)})
}
