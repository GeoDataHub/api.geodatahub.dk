package common

import (
	"os"
	"database/sql"
	_ "github.com/lib/pq"
	"log"
	"fmt"
)

var DB *sql.DB
var err error

// Opening a database and save the reference to `Database` struct.
func Init(host string, database string, user string, password string) (*sql.DB, error) {
	// Disable SSL mode in testing environment
	sslMode := "require"
	if os.Getenv("DISABLE_SSL_FOR_TESTING") != "" {
		sslMode = "disable"
	}

	log.Print("Connecting to database using standard lib")

	pgsqlconn := fmt.Sprintf("host=%s port=5432 user=%s password=%s dbname=%s sslmode=%s", host, user, password, database, sslMode)
	sdb, err := sql.Open("postgres", pgsqlconn)

	if err != nil {
		log.Printf("Unable to connect to database using standard lib: %s", err)
	}
	DB = sdb

	// Optimize connection settings for Lambda execution
	DB.SetMaxOpenConns(2)
	DB.SetMaxIdleConns(1)

	return DB, err
}

// GetStandardDB Return reference to a sql connection using the standard library
func GetStandardDB() *sql.DB {
	if DB == nil {
		log.Fatalf("The database isn't initialized")
	}
	return DB
}
