package common

import (
	"os"
	"encoding/json"
	"log"
	"time"
	"fmt"
	"runtime"

	"github.com/satori/go.uuid"
	"github.com/gin-gonic/gin"

	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
)

// ContextToAuthorizer Convert raw HTTP context header to struct
func ContextToAuthorizer(raw_context string) (*AWSContext, error) {
	context := &AWSContext{}
	err := json.Unmarshal([]byte(raw_context), context)
	if err != nil {
		return context, err
	}

	return context, nil
}

// ValidateUser Middelware to extract user information
//
// If successful the user ID is added to the UserUUID parameter on the request
// context
func ValidateUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := AuthHeader{}
		if c.BindHeader(&auth) != nil {
			// Abort and return 400 if bind fails
			return
		}

		// First priority is to get the requesting user from the API Gateway context. This information is added by the
		// gateway and ensures the user information is valid
		user := auth.Context.Authorizer.Claims.Username

		// Second priority is to decode and validate the JWT token by hand
		// The signing keys are obtained from
		if user == "" && auth.Authorization != "" {
			// Load the AWS Cognito key set used to validate the token was supplied by Cognito.
			// This is unique to the userpool and will not change.
			// https://cognito-idp.eu-central-1.amazonaws.com/eu-central-1_142k8OSKR/.well-known/jwks.json
			keyset, err := jwk.Parse(COGNITOKEYSPROD)
			if err != nil {
				log.Printf("Failed to validate keyset with error: %s", err.Error())
				c.AbortWithStatusJSON(500, gin.H{"error": "Failed to validate keyset. Please contact support@geodatahub.dk"}); return
			}

			// Decode the JWT token and verify the token was signed with the above keyset
			token, err := jwt.Parse([]byte(auth.Authorization),
				// Ensure the token was signed by Cognito and is still valid
				jwt.WithKeySet(keyset), jwt.WithValidate(true),
				// Ensure the token aud and iss matches the Cognito user pool and client app
				jwt.WithIssuer(COGNITOISSPROD), jwt.WithAudience(COGNITOIAUDPROD),
				// Ensure the user sent the ID token and not an access or refresh token
				jwt.WithClaimValue("token_use", "id"))
			if err != nil {
				log.Printf("Failed to valid token: %s\n", err.Error())
				c.AbortWithStatusJSON(401, gin.H{"error": "Invalid token"}); return
			}

			// Token was valid - the token subject is the user making the request
			user = token.Subject()
		}

		// If no user information was available then the user continues as an anonymous request
		if user == "" {
			c.Set("UserUUID", uuid.Nil.String())
			fmt.Printf("\nWelcome user \tAnonymous")
		} else {
			// Add the verified user information to context
			c.Set("UserUUID", user)
			fmt.Printf("\nWelcome user with ID \t%s", user)
		}

		c.Next()
	}
}

// Track the time of a given function execution
func TimeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed)
}

// Middleware to add CORS headers to all responses
func AddCORSHeaders() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Allow Cross-site requests
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		c.Header("Access-Control-Allow-Headers", "Accept,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token")
		c.Next()
	}
}

// contains Check if a array contains a given string
func Contains(arr []string, str string) bool {
   for _, a := range arr {
      if a == str {
         return true
      }
   }
   return false
}

//UUIDContains Check if a array contains a given UUID
func UUIDContains(arr []uuid.UUID, id uuid.UUID) bool {
   for _, a := range arr {
      if a == id {
         return true
      }
   }
   return false
}

// ReportInternalError Take an error that exposes internal information about the platform and transform it into a generic
// error we can show to the user
func ReportInternalError(err error) error {
	// TODO: Create Gitlab incident directly on these type of errors
	incidentUUID := uuid.NewV4().String()
	_, file, lineno, _ := runtime.Caller(1)
	fmt.Printf("FATAL %s: Call from %s:%d failed with error: %s", incidentUUID, file, lineno, err)
	return fmt.Errorf("FATAL ERROR - We are looking into it. Please contact support with the ticket number %s", incidentUUID)
}

// GetRequestingUser Return the user model that is making the request
// It is important to always check this call for errors and return to ensure users never gets beyond this check.
func GetRequestingUser(c *gin.Context, userRequired bool) (User, error) {
	var user User
	var err error = nil

	// Get user who is requesting information from an endpoint
	userUUID, err := uuid.FromString(c.GetString("UserUUID"))
	if err != nil && userRequired {
		c.AbortWithStatusJSON(401, gin.H{"error": "User does not have valid UUID. Please contact support@geodatahub.dk"})
	} else {
		// Clear error if a user isn't required
		err = nil
	}

	// The request actually contains a valid UUID - Check who it is
	if userUUID != uuid.Nil {
		user, err =  GetUser(userUUID)
		if err != nil {
			c.AbortWithStatusJSON(500, gin.H{"error": "Your user ID is not registered in our database. Please contact support@geodatahub.dk"})
		}
	}

	if userUUID == uuid.Nil && userRequired && err == nil {
		c.AbortWithStatusJSON(401, gin.H{"error": "Unauthorized"})
		err = fmt.Errorf("Not allowed")
	}

	return user, err
}

//getenv Get an environment variable with default
func getenv(key, fallback string) string {
    value := os.Getenv(key)
    if len(value) == 0 {
        return fallback
    }
    return value
}
