package common

import (
	"net/http/httptest"
	"net/http"
	"log"
	"testing"
	gin "github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/api.geodatahub.dk/tests"
)

func TestValidateUser(t *testing.T) {

	var testcases = []struct {
		// input
		name           string
		context        string
		authorization  string

		// expected
		code           int
		resp           string
	}{
		{
			"Empty headers - Anonymous user",
			"",
			"",
			200,
			uuid.Nil.String(),
		},
		{
			"Invalid formatted context - Request aborted",
			`{"accountId": "627632750936", "resourceId": "4kg6yv", "stage": "dev"`,
			"",
			400,
			"",
		},
		{
			"Invalid formatted authorization - Request aborted",
			"",
			"eyJraWQiOiJWQjNzZkQrNkR0TTZrZkJwNkorKzMxXC85RmNoM0pmbjQ2TW82cW5EY3FIbz0iLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiRGF2eXMwT3BNN0tWU0tNQUlFaURaQSIsInN1YiI6ImI1MTgyYjU2LTcxZTQtNDFjNS04OWZhLTgxYTE3OGZjODI2YiIsImF1ZCI6IjVucXBpMzJvMWk1c203b2VwOWFnb2QzY25zIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImV2ZW50X2lkIjoiZmFmODRlY2ItYzBkMi00ZmZhLWJlMWEtOThjYmM0Njk4NzVlIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MTUxMDU0NjAsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xXzE0Mms4T1NLUiIsImNvZ25pdG86dXNlcm5hbWUiOiJiNTE4MmI1Ni03MWU0LTQxYzUtODlmYS04MWExNzhmYzgyNmIiLCJleHAiOjE2MTU3NDY1OTcsImlhdCI6MTYxNTc0Mjk5NywiZW1haWwiOiJjaHJpc3RpYW5AZ2VvZGF0YWh1Yi5kayJ9.Au7NgVX7eOxKw-OqaP3_Src50WouDXU-ot48d9ezEhP72HazY41JbGUo0Ew-oZduVuYKTxhYmsYBbBHDYqNvbGka_OG7UrC7IEEITyF9FUMIQjAY0LBwM-opovRRUp6MUla8dcAMycluaMhyRSPSKxpXAr5mcFth3C2neSR5THHyVfGD10bBTbBJJAkIsqBpDoRTaUybc7qaleQ6lHDtENv15tVGaBlpvthdU_tUcxxAL3f2w631pvQ",
			401,
			"Invalid token",
		},
		{
			"Debug token from jwt.io - Request aborted",
			"",
			"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			401,
			"Invalid token",
		},
		{
			"Expired token - Request aborted",
			"",
			"eyJraWQiOiJWQjNzZkQrNkR0TTZrZkJwNkorKzMxXC85RmNoM0pmbjQ2TW82cW5EY3FIbz0iLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiRGF2eXMwT3BNN0tWU0tNQUlFaURaQSIsInN1YiI6ImI1MTgyYjU2LTcxZTQtNDFjNS04OWZhLTgxYTE3OGZjODI2YiIsImF1ZCI6IjVucXBpMzJvMWk1c203b2VwOWFnb2QzY25zIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImV2ZW50X2lkIjoiZmFmODRlY2ItYzBkMi00ZmZhLWJlMWEtOThjYmM0Njk4NzVlIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MTUxMDU0NjAsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xXzE0Mms4T1NLUiIsImNvZ25pdG86dXNlcm5hbWUiOiJiNTE4MmI1Ni03MWU0LTQxYzUtODlmYS04MWExNzhmYzgyNmIiLCJleHAiOjE2MTU3NDY1OTcsImlhdCI6MTYxNTc0Mjk5NywiZW1haWwiOiJjaHJpc3RpYW5AZ2VvZGF0YWh1Yi5kayJ9.Au7NgVX7eOxKw-OqaP3_Src50WouDXU-ot48d9ezEhP72HazY41JbGUo0Ew-oZduVuYKTxhYmsYBbBHDYqNvbGka_OG7UrC7IEEITyF9FUMIQjAY0LBwM-opovRRUp6MUla8dcAMycluaMhyRSPSzzsE9BvqcNgWoSaCaoKedcghCu4LM2NPpiZHXEYmwGZ0kAApFeJ7SrTdmggdCISI9HKxpXAr5mcFth3C2neSR5THHyVfGD10bBTbBJJAkIsqBpDoRTaUybc7qaleQ6lHDtENv15tVGaBlpvthdU_tUcxxAL3f2w631pvQLhb0ZBfNI53FTN9d6LjMaVbEgT7kg",
			401,
			"Invalid token",
		},
		{
			"Valid formatted context - Gateway user",
			`{
              "accountId": "627632750936",
              "authorizer": {
                "claims": {
                  "aud": "15934a8jitnojg4thofvr8d0io",
                  "cognito:username": "926b54eb-b247-44a8-9e7b-a7824ea54b29",
                  "email": "christian@geodatahub.dk",
                  "exp": "Fri May 31 07:28:59 UTC 2019",
                  "iat": "Fri May 31 06:28:59 UTC 2019",
                  "iss": "https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_uFZ00HSxo",
                  "sub": "926b54eb-b247-44a8-9e7b-a7824ea54b29"
                  }
                }
              }`,
			"",
			200,
			"926b54eb-b247-44a8-9e7b-a7824ea54b29",
		},
		{
			"Valid formatted context and expired token - Gateway user since we trust the gw context",
			`{
              "accountId": "627632750936",
              "authorizer": {
                "claims": {
                  "aud": "15934a8jitnojg4thofvr8d0io",
                  "cognito:username": "926b54eb-b247-44a8-9e7b-a7824ea54b29",
                  "email": "christian@geodatahub.dk",
                  "exp": "Fri May 31 07:28:59 UTC 2019",
                  "iat": "Fri May 31 06:28:59 UTC 2019",
                  "iss": "https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_uFZ00HSxo",
                  "sub": "926b54eb-b247-44a8-9e7b-a7824ea54b29"
                  }
                }
              }`,
			"eyJraWQiOiJWQjNzZkQrNkR0TTZrZkJwNkorKzMxXC85RmNoM0pmbjQ2TW82cW5EY3FIbz0iLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiRGF2eXMwT3BNN0tWU0tNQUlFaURaQSIsInN1YiI6ImI1MTgyYjU2LTcxZTQtNDFjNS04OWZhLTgxYTE3OGZjODI2YiIsImF1ZCI6IjVucXBpMzJvMWk1c203b2VwOWFnb2QzY25zIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImV2ZW50X2lkIjoiZmFmODRlY2ItYzBkMi00ZmZhLWJlMWEtOThjYmM0Njk4NzVlIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MTUxMDU0NjAsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xXzE0Mms4T1NLUiIsImNvZ25pdG86dXNlcm5hbWUiOiJiNTE4MmI1Ni03MWU0LTQxYzUtODlmYS04MWExNzhmYzgyNmIiLCJleHAiOjE2MTU3NDY1OTcsImlhdCI6MTYxNTc0Mjk5NywiZW1haWwiOiJjaHJpc3RpYW5AZ2VvZGF0YWh1Yi5kayJ9.Au7NgVX7eOxKw-OqaP3_Src50WouDXU-ot48d9ezEhP72HazY41JbGUo0Ew-oZduVuYKTxhYmsYBbBHDYqNvbGka_OG7UrC7IEEITyF9FUMIQjAY0LBwM-opovRRUp6MUla8dcAMycluaMhyRSPSzzsE9BvqcNgWoSaCaoKedcghCu4LM2NPpiZHXEYmwGZ0kAApFeJ7SrTdmggdCISI9HKxpXAr5mcFth3C2neSR5THHyVfGD10bBTbBJJAkIsqBpDoRTaUybc7qaleQ6lHDtENv15tVGaBlpvthdU_tUcxxAL3f2w631pvQLhb0ZBfNI53FTN9d6LjMaVbEgT7kg",
			200,
			"926b54eb-b247-44a8-9e7b-a7824ea54b29",
		},
	}

	for _, testcase := range testcases {
		validateFunc := ValidateUser()

		// Create test request
		writer := httptest.NewRecorder()
		req, _ := http.NewRequest("Get", "/", nil)
		if testcase.context != "" {
			req.Header.Set("X-Golambdaproxy-Apigw-Context", testcase.context)
		}
		if testcase.authorization != "" {
			req.Header.Set("Authorization", testcase.authorization)
		}


		c, _ := gin.CreateTestContext(writer)
		c.Request = req
		validateFunc(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Wrong status code", testcase.name)
		if testcase.code == 200 {
			require.Equal(t, testcase.resp, c.GetString("UserUUID"), "Case %s: User UUID didn't match the expected value", testcase.name)
		} else {
			require.Contains(t, writer.Body.String(), testcase.resp, "Case %s: Expected different response from request", testcase.name)
		}
	}
}

func TestDecodeOfAWSContext(t *testing.T) {
	rawContext := `{"accountId": "627632750936", "resourceId": "4kg6yv", "stage": "dev", "requestId": "ff318f3f-836e-11e9-b522-49ed039646b5", "identity": { "cognitoIdentityPoolId": "", "accountId": "", "cognitoIdentityId": "", "caller": "", "apiKey": "", "accessKey": "", "sourceIp": "79.142.232.134", "cognitoAuthenticationType": "", "cognitoAuthenticationProvider": "", "userArn": "", "userAgent": "python-requests/2.21.0", "user": "" }, "resourcePath": "/{proxy+}", "authorizer": { "claims": { "at_hash": "jPvKHwCPb4QaoEEo_aW10A", "aud": "15934a8jitnojg4thofvr8d0io", "auth_time": "1559284139", "cognito:username": "926b54eb-b247-44a8-9e7b-a7824ea54b29", "email": "christian@geodatahub.dk", "email_verified": "true", "exp": "Fri May 31 07:28:59 UTC 2019", "iat": "Fri May 31 06:28:59 UTC 2019", "iss": "https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_uFZ00HSxo", "sub": "926b54eb-b247-44a8-9e7b-a7824ea54b29", "token_use": "id" } }, "httpMethod": "GET", "apiId": "cq6nowxuf4"}`
	authorizer, _ := ContextToAuthorizer(rawContext)

	log.Printf("\t%s", authorizer.Authorizer.Claims.Email)
	assert.Equal(t, authorizer.AccountId, "627632750936")
	assert.Equal(t, authorizer.Authorizer.Claims.Email, "christian@geodatahub.dk")
	assert.Equal(t, authorizer.Authorizer.Claims.Username, "926b54eb-b247-44a8-9e7b-a7824ea54b29")
}

func TestGetRequestingUser(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")

	var testcases = []struct {
		// input
		name           string
		requestingUser string
		userRequired   bool

		// expected
		code           int
	}{
		{
			"Login required and user is anon",
			"",
			true,
			401,
		},
		{
			"Login required and user is anon with uuid.Nil",
			uuid.Nil.String(),
			true,
			401,
		},
		{
			"Login required and user doesn't exist",
			"97ec9121-7068-4a12-b440-cb2fe1c54997",
			true,
			500,
		},
		{
			"Login required and user does exist",
			"00000000-0000-0000-0000-000000000001",
			true,
			200,
		},
		{
			"Login not required and user is anon",
			"",
			false,
			200,
		},
		{
			"Login not required and user doesn't exist",
			"97ec9121-7068-4a12-b440-cb2fe1c54997",
			false,
			500,
		},
		{
			"Login not required and user does exist",
			"00000000-0000-0000-0000-000000000001",
			false,
			200,
		},
	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		c.Set("UserUUID", testcase.requestingUser)
		GetRequestingUser(c, testcase.userRequired)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Response didn't contain expected status code", testcase.name)
	}

}
