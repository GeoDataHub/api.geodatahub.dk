package common

import (
	"github.com/satori/go.uuid"
)

// PublicOrganization UUID for the public organization used to publicize datasets
var /* const */ PublicOrganization uuid.UUID = uuid.FromStringOrNil("55555555-5555-5555-5555-555555555555")
//Environment Which environment is the API running in (prod, staging, dev, local)
var /* const */ Environment string = getenv("ENVIRONMENT", "prod")
//APIBaseURL URL that the API responds to
var /* const */ APIBaseURL string = "https://api.geodatahub.dk"
//APIBaseDevURL URL that the development API responds to
var /* const */ APIBaseDevURL string = "https://api-dev.geodatahub.dk"
//COGNITOKEYSPROD Production keys used to sign cognito tokens
var COGNITOKEYSPROD []byte = []byte(`
{
	"keys": [{
		"alg": "RS256",
		"e": "AQAB",
		"kid": "VB3sfD+6DtM6kfBp6J++31/9Fch3Jfn46Mo6qnDcqHo=",
		"kty": "RSA",
		"n": "gEJzYbCWxWB6R_0NkePR9t6h2OOxz5X6PX1QwmVwxPP1g2fRRzflOb4Hk1RKKGpADco1OFANjaaHwinM4TDXlWepixRztIbBGJKMADQlQg_13ui8fc_LO0weN8NnXyQUYgU4qQ47hv_SZggRdCyHFoX7RsVVzwMkdD7BJkN0izLaUBfMwbLcpP_C-8blmcDL-xnSWmCs-Cap6ojnuOBQhZ-CGQQOSOf6WE3PzASeAn93eaA7oXStwbZPDyNWG_1CDosWb5kODg7YnNndbSPzjoBdGr6A2v27ynsHO1G3iH36ZSXqx_u6j3hNi-10JBcEea10826g2U3Ec2TTrXo5uw",
		"use": "sig"
	}, {
		"alg": "RS256",
		"e": "AQAB",
		"kid": "FzrplERhh+/8/APB/JUOm3+j5HM9YY6fTowtJvB8/K0=",
		"kty": "RSA",
		"n": "vMeBFHvaVCgLCUthjIJ0dXwvzRF_hXd12Aau15ouyiCQBuPfX7wBf7piQYPer6v5N5teUrZOlrPD7vUT5n8ZqclWXjiECe3PM-GuNiMv4YgUb5ZQgFNHJF59tCSEEVr1H6Y0dOmsDX-L4PAhVm1GIBvWUeepPRGjw9O8_Ce4IpqkbxO-cD-EmJG2joQTr9zlpi4zUDFXozjmwcnUtMIAmy8Bwf8BwFagQW-aYXg5TeqRnmjmoQM05IHQRjZS3U2G237zEfO49jU5qdEjWL59FHNfginGR7kiMo4Js2SOYufSSTANOlJ1hLf9OeNJ9vbZ-Gj-6MPCf8fC_62M_2DDkQ",
		"use": "sig"
	}]
}`)
//COGNITOISSPROD Issuer of the cognito token
var COGNITOISSPROD string = "https://cognito-idp.eu-central-1.amazonaws.com/eu-central-1_142k8OSKR"
//COGNITOAUDPROD Audience of the cognito token. This is the APP id of the cognito client app
var COGNITOIAUDPROD string = "5nqpi32o1i5sm7oep9agod3cns"
