package common

import (
	"testing"
	"errors"

	"github.com/satori/go.uuid"
	require "github.com/stretchr/testify/require"

	"gitlab.com/api.geodatahub.dk/tests"
)

func TestUserCanInOrg(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")
	unknownUser := uuid.NewV4()

	var realAdminUser uuid.UUID
	var realUser uuid.UUID
	var realOrg uuid.UUID

	normNonRootUser, _ := uuid.FromString("00000000-0000-0000-0000-000000000004")
	drillerOrg, _ := uuid.FromString("10000000-0000-0000-0000-000000000002")
	officeDwellerOrg, _ := uuid.FromString("10000000-0000-0000-0000-000000000004")
	DB := GetStandardDB()
	if DB == nil {
		t.Fatalf("The DB connection is closed")
	}

	err := DB.QueryRow("SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'").Scan(&realAdminUser)
	if err != nil {
		t.Logf("Unable to find user janedoe for testing: %s", err)
	}
	err = DB.QueryRow("SELECT id FROM users WHERE email = 'john@fakegeocorp.com'").Scan(&realUser)
	if err != nil {
		t.Logf("Unable to find user john for testing: %s", err)
	}

	err = DB.QueryRow("SELECT organization FROM roles WHERE ARRAY[id] && (SELECT roles FROM users WHERE email = 'jane@fakegeocorp.com') LIMIT 1").Scan(&realOrg)
	if err != nil {
		t.Logf("Unable to find organization for testing: %s", err)
	}

	var testcases = []struct {
		// input
		name           string
		requestingUser uuid.UUID
		requestingOrg  uuid.UUID
		requestingPerm string

		// expected
		allowed        bool
		error          error
	}{
		{
			"Empty user, Empty org, Unknown permission",
			uuid.Nil,
			uuid.Nil,
			"can_dance",
			false,
			errors.New("No permission named: can_dance"),
		},
		{
			"Empty user, Empty org, Known permission",
			uuid.Nil,
			uuid.Nil,
			"can_read",
			false,
			nil,
		},
		{
			"Unknown user, Empty org, Known permission",
			unknownUser,
			uuid.Nil,
			"can_read",
			false,
			nil,
		},
		{
			"Unknown user, Unknown org, Known permission",
			unknownUser,
			unknownUser,
			"can_read",
			false,
			nil,
		},
		{
			"Known user, Unknown org, Known permission",
			realAdminUser,
			unknownUser,
			"can_read",
			false,
			nil,
		},
		{
			"Known user, Known org, Known permission",
			realAdminUser,
			realOrg,
			"can_manage_users",
			true,
			nil,
		},
		{
			"Known user, Known org, Known denied permission",
			realUser,
			realOrg,
			"can_share",
			false,
			nil,
		},
		{
			"Hierarchy tests: Has permissions in organization below",
			realUser,
			officeDwellerOrg,
			"can_share",
			true,
			nil,
		},
		{
			"Hierarchy tests: Does not have permissions in sibling org",
			realUser,
			drillerOrg,
			"can_share",
			false,
			nil,
		},
		{
			"Hierarchy tests: Does not have permissions in root org",
			realUser,
			realOrg,
			"can_share",
			false,
			nil,
		},
		{
			"Hierarchy tests: Adm users have permissions in all child orgs",
			realAdminUser,
			officeDwellerOrg,
			"can_manage_org",
			true,
			nil,
		},
		{
			"Hierarchy tests: Norm users, not in root org, have permissions in child orgs",
			normNonRootUser,
			officeDwellerOrg,
			"can_manage_users",
			true,
			nil,
		},
	}

	var owner User
	for _, testcase := range testcases {
		owner.ID = testcase.requestingUser

		isAllowed, err := owner.Can(testcase.requestingOrg, testcase.requestingPerm)

		require.Equalf(t, testcase.error, err, "Testcase %s: Didn't expect this error", testcase.name)
		require.Equalf(t,
			testcase.allowed, isAllowed,
			"Testcase %s: Didn't expect user (%s) to have or miss this permission (%s) in %s",
			testcase.name, testcase.requestingUser, testcase.requestingPerm, testcase.requestingOrg)
	}
}

func TestUserOrganizations(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")

	var testcases = []struct {
		// input
		name           string
		requestingUser uuid.UUID

		// expected
		numOrgs        int
		error          error
	}{
		{
			"Non-existing user",
			uuid.NewV4(),
			0,
			nil,
		},
		{
			"User without org or roles",
			uuid.FromStringOrNil("00000000-0000-0000-0000-000000000003"),
			0,
			nil,
		},
		{
			"User with nested roles",
			uuid.FromStringOrNil("00000000-0000-0000-0000-000000000001"),
			4,
			nil,
		},
		{
			"User who is not in root org",
			uuid.FromStringOrNil("00000000-0000-0000-0000-000000000004"),
			2,
			nil,
		},
	}

	var owner User
	for _, testcase := range testcases {
		owner.ID = testcase.requestingUser

		orgs, err := owner.Organizations()

		require.Equalf(t, testcase.error, err, "Testcase %s: Didn't expect this error", testcase.name)
		require.Lenf(t,
			orgs, testcase.numOrgs,
			"Testcase %s: User is not in the correct number of organizations", testcase.name)
	}
}

func TestRolesFromOrganizations(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")

	var testcases = []struct {
		// input
		name      string
		orgs      []uuid.UUID

		// expected
		numRoles  int
		error     error
	}{
		{
			"Empty org list",
			[]uuid.UUID{},
			0,
			nil,
		},
		{
			"Single org - one role - drillers",
			[]uuid.UUID{uuid.FromStringOrNil("10000000-0000-0000-0000-000000000002")},
			1,
			nil,
		},
		{
			"Single org - multiple role - root org",
			[]uuid.UUID{uuid.FromStringOrNil("10000000-0000-0000-0000-000000000001")},
			2,
			nil,
		},
		{
			"Multiple org - multiple role",
			[]uuid.UUID{
				uuid.FromStringOrNil("10000000-0000-0000-0000-000000000001"),
				uuid.FromStringOrNil("10000000-0000-0000-0000-000000000003"),
			},
			4,
			nil,
		},

	}

	for _, testcase := range testcases {
		roles, err := RolesFromOrganizations(testcase.orgs)

		require.Equalf(t, testcase.error, err, "Testcase %s: Didn't expect this error", testcase.name)
		require.Lenf(t,
			roles, testcase.numRoles,
			"Testcase %s: Expected the organizations to return a different set of roles - Actual roles: %+v", testcase.name, roles)
	}
}

func TestUserGetRoles(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")

	DB := GetStandardDB()
	if DB == nil {
		t.Fatalf("The DB connection is closed")
	}

	// Get all roles to translate between roles UUID and role name
	var allRoles []RolePermissions
	rows, err := DB.Query("SELECT id, name FROM roles")
	if err != nil {
		t.Fatalf("Unable to get all roles for test: %s", err)
	}

	defer rows.Close()
	for rows.Next() {
		var role RolePermissions
		err := rows.Scan(&role.id, &role.name)
		if err != nil {
			t.Fatalf("Failed to scan role from rows: %s", err.Error())
		}
		allRoles = append(allRoles, role)
	}
	if rows.Err() != nil {
		t.Fatalf("Rows contains error: %s", err.Error())
	}

	var testcases = []struct {
		// input
		name           string
		requestingUser uuid.UUID

		// expected
		numRoles       int
		roles          []string
		error          error
	}{
		{
			// UUIDs are not verified at this stage
			"Non-existing user",
			uuid.NewV4(),
			1,
			[]string{},
			nil,
		},
		{
			"Anonymous user",
			uuid.Nil,
			0,
			[]string{},
			nil,
		},
		{
			"User without org or roles",
			uuid.FromStringOrNil("00000000-0000-0000-0000-000000000003"),
			1, // The user can only access datasets with his/her own datasets
			[]string{},
			nil,
		},
		{
			"User with nested roles",
			uuid.FromStringOrNil("00000000-0000-0000-0000-000000000001"),
			6,
			[]string{"Fake Geocorp admins", "Fake Geocorp writers", "Fake Geocorp office admins",
				"Fake Geocorp office readers", "Fake Geocorp driller readers"},
			nil,
		},
		{
			"User who is not in root org",
			uuid.FromStringOrNil("00000000-0000-0000-0000-000000000004"),
			3,
			[]string{"Fake Geocorp office admins", "Fake Geocorp office readers"},
			nil,
		},
	}

	var requester User
	for _, testcase := range testcases {
		requester.ID = testcase.requestingUser

		roles, err := requester.GetRoles()

		require.Equalf(t, testcase.error, err, "Testcase %s: Didn't expect this error", testcase.name)
		require.Len(t, roles, testcase.numRoles, "Testcase %s: Wrong number of roles returned", testcase.name)

		// Find matching role and match on the name
		for _, returnRole := range roles {
			for _, dbRole := range allRoles {
				if returnRole == dbRole.id {
					require.Contains(t, testcase.roles, dbRole.name,
						"Testcase %s: Expected user to have a different set of roles", testcase.name)
				}
			}
		}
	}
}
