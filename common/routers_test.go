package common

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"os"
	"fmt"
	"github.com/satori/go.uuid"
	require "github.com/stretchr/testify/require"
	gin "github.com/gin-gonic/gin"

	"gitlab.com/api.geodatahub.dk/tests"

)

func TestMain(m *testing.M) {
	// Setup database tables
	tests.MigrateDirection("up", "../utils/migrate")

	// Make initial connection
	_, err := Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))

	if err != nil {
		fmt.Print("Unable to connect to database using standard lib")
	}
	// Run tests
	exitcode := m.Run()

	// Remove all tables once test are done
	tests.MigrateDirection("down", "../utils/migrate")

	// Run all test then Exit
	os.Exit(exitcode)
}

func TestGetUserRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")

	DB := GetStandardDB()
	var realUUID string
	err := DB.QueryRow("SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'").Scan(&realUUID)
	if err != nil {
		t.Fatalf("Unable to find user janedoe for testing: %s", err)
	}
	t.Logf("Testing user UUID: %s", realUUID)

	var testcases = []struct {
		// input
		name           string
		params         gin.Param
		requestingUser string

		// expected
		code           int
		respMsg        string
	}{
		{
			"NoParamsNoUser",
			gin.Param{},
			"",
			401,
			"does not have valid UUID",
		},
		{
			// Note: Should this raise a - unknown parameter error?
			"DifferentParams",
			gin.Param{Key: "identifier", Value: "7495b1d2-eff4-4118-8e52-14660cb2c126"},
			realUUID,
			200,
			"Email",
		},
		{
			"InvalidParams",
			gin.Param{Key: "id", Value: "7495b1d2-eff4-8e52-14660cb2c126"},
			realUUID,
			400,
			"Invalid user",
		},
		{
			"RealUser",
			gin.Param{Key: "id", Value: realUUID},
			realUUID,
			200,
			"",
		},
		{
			"Real user modifying someone else",
			gin.Param{Key: "id", Value: "7495b1d2-eff4-4118-8e52-14660cb2c126"},
			realUUID,
			501,
			"Viewing other users data",
		},
		{
			"Lookup self",
			gin.Param{},
			realUUID,
			200,
			"",
		},

	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		c.Params = []gin.Param{testcase.params}
		c.Set("UserUUID", testcase.requestingUser)
		GetUserByIDRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		require.Contains(t, writer.Body.String(), testcase.respMsg, "Case %s: Invalid body returned", testcase.name)
	}
}


func TestCreateOrganizationRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")

	DB := GetStandardDB()
	if DB == nil {
		t.Fatalf("The DB connection is closed")
	}
	var realUUID string
	err := DB.QueryRow("SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'").Scan(&realUUID)
	if err != nil {
		t.Fatalf("Unable to find user janedoe for testing: %s", err)
	}
	t.Logf("Testing user UUID: %s", realUUID)

	var testcases = []struct {
		// input
		name           string
		url            string
		requestingUser string

		// expected
		code           int
		respMsg        string
	}{
		{
			"No user logged-in",
			"/organizations",
			"",
			401,
			"does not have valid UUID",
		},
		{
			"Organization has no name",
			"/organizations?userUUID=Fake%20Geocorp%20New",
			realUUID,
			400,
			"must have a name",
		},
		{
			"Organization with name",
			"/organizations?name=Fake%20Geocorp%20New",
			realUUID,
			200,
			"-",
		},
		{
			"Organization with name and parrent",
			"/organizations?name=Fake%20Geocorp%20New&parrent=myParrent",
			realUUID,
			501,
			"is not implemented",
		},
	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		req, _ := http.NewRequest("POST", testcase.url, nil)
		c.Request = req
		c.Set("UserUUID", testcase.requestingUser)
		CreateOrganizationRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		if testcase.respMsg == "UUID" {
			_, err := uuid.FromString(writer.Body.String())
			require.Nilf(t, err, "Case %s: Response didn't contain a valid UUID - Got %s", testcase.name, writer.Body.String())
		} else {
			require.Contains(t, writer.Body.String(), testcase.respMsg, "Case %s: Invalid body returned", testcase.name)
		}
	}
}




func TestInviteUserToOrgRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")
	admUsrUUID, normUserUUID, fakeOrg := tests.GetFakeGeocorpUsers()

	admUsr, _ := GetUser(uuid.FromStringOrNil(admUsrUUID))
	normUser, _ := GetUser(uuid.FromStringOrNil(normUserUUID))

	var testcases = []struct {
		// input
		name             string
		url              string
		requestingUser   string
		requestingOrg    gin.Param

		// expected
		code             int
		respMsg          string
	}{
		{
			"No user logged-in",
			fmt.Sprintf("/organizations/%s/users", fakeOrg),
			"",
			gin.Param{Key: "Unk1", Value: ""},
			401,
			"Request contains no user information",
		},
		{
			"Missing organization",
			fmt.Sprintf("/organizations/%s/users", fakeOrg),
			normUser.ID.String(),
			gin.Param{Key: "Unk1", Value: ""},
			400,
			"organization ID",
		},
		{
			"Missing email",
			fmt.Sprintf("/organizations/%s/users", fakeOrg),
			admUsr.ID.String(),
			gin.Param{Key: "orgId", Value: fakeOrg},
			400,
			"Missing email",
		},
		{
			"Missing role",
			fmt.Sprintf("/organizations/%s/users?email=%s", fakeOrg, fakeOrg),
			admUsr.ID.String(),
			gin.Param{Key: "orgId", Value: fakeOrg},
			400,
			"Missing role",
		},
		{
			"Unknown User",
			fmt.Sprintf("/organizations/%s/users?email=%s&roleID=%s",
				fakeOrg,
				"unknown@user.com",
				fakeOrg),
			normUser.ID.String(),
			gin.Param{Key: "orgId", Value: fakeOrg},
			403,
			"don't have permission",
		},
		{
			"Adding yourself",
			fmt.Sprintf("/organizations/%s/users?email=%s&roleID=%s",
				fakeOrg,
				admUsr.Email,
				fakeOrg),
			admUsr.ID.String(),
			gin.Param{Key: "orgId", Value: fakeOrg},
			400,
			"cannot add yourself",
		},
		{
			"Adm requests unknown role",
			fmt.Sprintf("/organizations/%s/users/?email=%s&roleID=%s",
				fakeOrg,
				normUser.Email,
				fakeOrg),
			admUsr.ID.String(),
			gin.Param{Key: "orgId", Value: fakeOrg},
			400,
			"does not exist",
		},
		{
			"Adm requests user added",
			fmt.Sprintf("/organizations/%s/users/?email=%s&roleID=%s",
				fakeOrg,
				normUser.Email,
				admUsr.Roles[0]),
			admUsr.ID.String(),
			gin.Param{Key: "orgId", Value: fakeOrg},
			200,
			"added with success",
		},
	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		req, _ := http.NewRequest("POST", testcase.url, nil)
		t.Log(testcase.url)
		c.Request = req
		c.Params = []gin.Param{testcase.requestingOrg,}
		c.Set("UserUUID", testcase.requestingUser)

		// Call endpoint
		InviteUserToOrganizationRoute(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		if testcase.respMsg == "UUID" {
			_, err := uuid.FromString(writer.Body.String())
			require.Nilf(t, err, "Case %s: Response didn't contain a valid UUID - Got %s", testcase.name, writer.Body.String())
		} else {
			require.Contains(t, writer.Body.String(), testcase.respMsg, "Case %s: Invalid body returned", testcase.name)
		}
	}

	// Check that the normal user now belong to the admin role
	checkUser, _ := GetUserByEmail(normUser.Email)
	require.NotSubset(t, checkUser.Roles, []uuid.UUID{normUser.Roles[0]}, "The user still belongs to his old group")
	require.Subset(t, checkUser.Roles, []uuid.UUID{admUsr.Roles[0]}, "The user didn't get the new role")
}

func TestGetUserOrganizationsRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")

	var testcases = []struct {
		// input
		name             string
		rqstUser         string

		// expected
		code             int
	}{
		{
			"User with no organizations",
			"00000000-0000-0000-0000-000000000003",
			200,
		},
		{
			"User in child organizations only",
			"00000000-0000-0000-0000-000000000004",
			200,
		},
		{
			"User in root organization",
			"00000000-0000-0000-0000-000000000001", // ADM
			200,
		},

	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)
		url := fmt.Sprintf("/users/%s/organizations", testcase.rqstUser)
		req, _ := http.NewRequest("GET", url, nil)
		c.Request = req
		c.Set("UserUUID", testcase.rqstUser)

		GetUserOrganizationsRoute(c)
		require.Equalf(t, testcase.code, c.Writer.Status(), "Case %s: HTTP code does not match expected value. Resp: %s", testcase.name, writer.Body.String())

	}
}
