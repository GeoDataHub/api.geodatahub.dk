PROJECT_NAME := "api.geodatahub.dk"
PKG := "gitlab.com/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	DISABLE_SSL_FOR_TESTING=y go test -v -short -p 1 ${PKG_LIST} 2>&1 | go-junit-report > report.xml

race: dep ## Run data race detector
	DISABLE_SSL_FOR_TESTING=y go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	DISABLE_SSL_FOR_TESTING=y go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	DISABLE_SSL_FOR_TESTING=y go test -p 1 -coverprofile=coverage.out -covermode=count ${PKG_LIST}
#	DISABLE_SSL_FOR_TESTING=y ./utils/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	DISABLE_SSL_FOR_TESTING=y ./utils/coverage.sh html;

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)/bin

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build:
	env GOOS=linux go build  -o bin/main main.go
	env GOOS=linux go build  -o bin/init_database utils/init_database/main.go
	env GOOS=linux go build  -o bin/migrate utils/migrate/main.go
	env GOOS=linux go build  -o bin/cognito-confirm-trigger utils/cognito-confirm-trigger/main.go
	env GOOS=linux go build  -o bin/create-heatmap utils/create-data-heatmap/main.go

package: build
	zip -r bin/api.zip bin/main schemas
	zip -r bin/init_database.zip bin/init_database

    # Copy migration SQL scripts to zip
	cp -r utils/migrate/*.sql .
	zip -r bin/migrate_database.zip bin/migrate *.sql
	rm *.sql

	zip -r bin/cognito-triggers.zip bin/cognito*
	zip -r bin/create-heatmap.zip bin/create-heatmap

package-cognito: build
	zip -r bin/handler-cognito.zip bin/cognito-confirm-trigger

go:
  # Install golang from source
	wget -nc -p -O /tmp/go1.11.5.tar.gz https://dl.google.com/go/go1.11.5.linux-amd64.tar.gz
	sudo tar zxvf /tmp/go1.11.5.tar.gz -C /usr/local/
	echo "Please add /usr/local/go/bin to PATH"

dep:
	go mod download

deploy: package
	aws s3 cp bin/handler.zip s3://geodatahub-code-${AWS_ENV}-${AWS_REGION}/api-geodatahub-dk/${AWS_ENV}/api-geodatahub-dk.zip

deploy-cognito: package-cognito
	aws s3 cp bin/handler-cognito.zip s3://geodatahub-code-${AWS_ENV}-${AWS_REGION}/api-geodatahub-dk/${AWS_ENV}/api-geodatahub-dk.zip

dev: package
	aws lambda update-function-code \
	--profile Dev \
	--function-name gdhfunction \
	--zip-file fileb://bin/api.zip \

dev-utils:
	aws lambda update-function-code \
	--function-name gdhdbsetupfunction \
	--zip-file fileb://bin/init_database.zip \
	--region ${AWS_REGION}

dev-cognito-trigger:
	aws lambda update-function-code \
	--function-name cognito-confirmation-trigger \
	--zip-file fileb://bin/cognito-triggers.zip \
	--region eu-west-1

dev-migrate: package
	aws lambda update-function-code \
    --profile Dev \
	--function-name gdhdbmigtatefunction \
	--zip-file fileb://bin/migrate_database.zip

dev-heatmap: package
	aws lambda update-function-code \
    --profile Dev \
	--function-name geodatahub-create-heatmap-function \
	--zip-file fileb://bin/create-heatmap.zip

run:
	docker run --network host -e AWS_LAMBDA_FUNCTION_TIMEOUT=10 \
	-e DBHOST=192.168.1.100 \
	-e DBUSR=geodatahub \
	-e DBPASSWORD=mysecretpassword \
	-e DBTABLE=geodatahub \
	--rm -v "${PWD}"/bin:/var/task lambci/lambda:go1.x main

migrate:
	cd utils/migrate; go run main.go

database:
	go run utils/init_database/main.go
	cd utils/migrate; go run main.go
