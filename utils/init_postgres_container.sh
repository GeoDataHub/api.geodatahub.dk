#!/bin/bash

set -euo pipefail

mkdir -p /tmp/pg-data
sudo chown -R $(id -u):$(id -g) /tmp/pg-data

# Generate keys
if [ ! -f server.key ]; then
    echo "Generating SSL files"
    openssl req -new -text -passout pass:abcd -subj /CN=localhost -out server.req
    openssl rsa -in privkey.pem -passin pass:abcd -out server.key
    openssl req -x509 -in server.req -text -key server.key -out server.crt
    chmod 600 server.key
    # Make key belong to current user who shares the uid with the postgres user in the
    # container
    sudo chown $(id -u) server.key
fi

# Setup container - using old image
#docker run -d --rm --user "$(id -u):$(id -g)" -v /etc/passwd:/etc/passwd:ro -v /tmp/pg-data/:/var/lib/postgresql/data --name pg-geodatahub -e POSTGRES_PASSWORD=$DBPASSWORD -p 5432:5432 -v $PWD/server.crt:/var/lib/postgresql/server.crt:ro -v $PWD/server.key:/var/lib/postgresql/server.key:ro mdillon/postgis -c ssl=on -c ssl_cert_file=/var/lib/postgresql/server.crt -c ssl_key_file=/var/lib/postgresql/server.key

## Start PostGIS with ssl disabled
docker run -d --rm --user "$(id -u):$(id -g)" -v /etc/passwd:/etc/passwd:ro -v /tmp/pg-data/:/var/lib/postgresql/data --name pg-geodatahub -e POSTGRES_PASSWORD=$DBPASSWORD -p 5432:5432 postgis/postgis:12-master

# Make sure the database is ready
sleep 10

cd init_database
DISABLE_SSL_FOR_TESTINT=y go run main.go

cd ../migrate
DISABLE_SSL_FOR_TESTING=y go run main.go
