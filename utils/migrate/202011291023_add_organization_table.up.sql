CREATE EXTENSION IF NOT EXISTS ltree;

CREATE TABLE organizations (
  id                 uuid default uuid_generate_v4() not null primary key,
  name               text not null,
  owner              uuid NOT NULL,
  num_users          int DEFAULT 0,
  num_datasets       int DEFAULT 0,
  subscription_plan  int DEFAULT 0,
  plan_begins        timestamp NULL,
  plan_ends          timestamp NULL,
  org_hierarchy      LTREE NULL,
  CONSTRAINT fk_subscriptions FOREIGN KEY (subscription_plan) REFERENCES subscription_plans(id),
  CONSTRAINT fk_owner FOREIGN KEY (owner) REFERENCES users(id)
  );

-- ADD NGG ORG
INSERT INTO organizations (id, name, owner, num_users, subscription_plan, plan_begins, plan_ends)
VALUES (uuid_generate_v4(), 'NextGen Geophysics',
        '11111111-1111-1111-1111-111111111111', 1, 1,
        '2000-01-01 00:00:00', '2100-01-01 00:00:00'),
       ('55555555-5555-5555-5555-555555555555', 'Public',
       '11111111-1111-1111-1111-111111111111', 1, 1,
       '2000-01-01 00:00:00', '2100-01-01 00:00:00');

-- Update hierarchy
UPDATE organizations
SET org_hierarchy = text2ltree(replace(id::TEXT, '-', '_'))
WHERE id IS NOT NULL;
