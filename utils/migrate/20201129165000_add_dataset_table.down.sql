DROP INDEX dataset_distinfo_idx;
DROP INDEX dataset_results_idx;
DROP INDEX dataset_roles_idx;
DROP INDEX dataset_geo_spatial_idx;

DROP TABLE datasets;
