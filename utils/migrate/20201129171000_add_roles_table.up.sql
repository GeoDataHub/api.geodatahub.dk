CREATE TABLE roles (
  id                 uuid default uuid_generate_v4() not null primary key,
  name               text not null,
  organization       uuid NOT NULL,
  can_read           bool default true,
  can_write          bool default false,
  can_share          bool default false,
  can_manage_users   bool default false,
  can_manage_org     bool default false,

  CONSTRAINT fk_organization FOREIGN KEY (organization) REFERENCES organizations(id)
);

-- Add role to allow reading public dataset
INSERT INTO roles (name, organization, can_read, can_write, can_share, can_manage_users, can_manage_org)
VALUES ('Public reader', '55555555-5555-5555-5555-555555555555', true, false, false, false, false);
