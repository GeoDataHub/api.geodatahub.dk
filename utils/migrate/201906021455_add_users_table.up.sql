CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users (
    Id         uuid      PRIMARY KEY  DEFAULT uuid_generate_v4(),
    Created_at timestamp  DEFAULT NOW(),
    Updated_at timestamp  DEFAULT NOW(),
    Email      text      NOT NULL,
    Roles      uuid[]    DEFAULT ARRAY[]::uuid[]
);

create unique index users_unique_lower_email_idx on users (lower(Email));

INSERT INTO users (Id, Email)
VALUES ('11111111-1111-1111-1111-111111111111', 'christian@nextgengeophysics.com');
