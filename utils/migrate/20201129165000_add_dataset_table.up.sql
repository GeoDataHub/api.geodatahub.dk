CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE datasets (
    Identifier         uuid     PRIMARY KEY DEFAULT uuid_generate_v4(),
	Created_at         timestamp DEFAULT NOW(),
	Updated_at         timestamp DEFAULT NOW(),
	Owner              uuid     NOT NULL,
	Organization       uuid     NULL,
	Datatype           text,
	Description        text,
	Distribution_info  jsonb    NOT NULL DEFAULT '{}'::JSONB,
	Result             jsonb    NOT NULL DEFAULT '{}'::JSONB,
	Projected_geometry Geometry NOT NULL,
    -- The UniqueKeys is a sorted string with key/value pairs of JSON
    -- parameters that must remain unique. Note: NULL does not count
    -- towards the uniqueness
    Unique_keys       text     NULL,
    Roles             uuid[]   NOT NULL,
    UNIQUE(Unique_keys, Organization, Datatype),

    CONSTRAINT fk_organization FOREIGN KEY (Organization) REFERENCES organizations(id),
    CONSTRAINT fk_owner FOREIGN KEY (Owner) REFERENCES users(id)
);

CREATE INDEX dataset_distinfo_idx ON datasets USING btree(Distribution_info);
CREATE INDEX dataset_results_idx ON datasets USING btree(Result);
CREATE INDEX dataset_roles_idx ON datasets USING gin(Roles);
CREATE INDEX dataset_geo_spatial_idx ON datasets USING GIST (Projected_geometry);
