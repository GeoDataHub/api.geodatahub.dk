package main

import (
	"log"
	"os"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func main() {
	sslMode := "require"
	if os.Getenv("DISABLE_SSL_FOR_TESTING") != "" {
		sslMode = "disable"
	}

	dbURL := "postgres://"+os.Getenv("DBNONADMUSR")+":"+os.Getenv("DBNONADMPASSWORD")+"@"+os.Getenv("DBHOST")+":5432/geodatahub?sslmode="+sslMode
	curDir, err := os.Getwd()

	m, err := migrate.New("file://"+curDir, dbURL)
	if err != nil {
		log.Fatalf("Unable to connect to database with error %s", err)
	}
	if err = m.Up(); err != nil {
		log.Fatalf("Could not migrate: %v", err)
	}
	log.Printf("Migration successfully")
}
