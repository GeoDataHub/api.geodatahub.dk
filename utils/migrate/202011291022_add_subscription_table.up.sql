CREATE TABLE subscription_plans (
  id                 INT GENERATED ALWAYS AS IDENTITY,
  name               text not null,
  num_users          INT DEFAULT 0,
  num_datasets       INT DEFAULT 0,
  price_usd          FLOAT,
  public_plan        BOOL DEFAULT False,  -- Show the plan as a public option
  PRIMARY KEY (id)
);


INSERT INTO subscription_plans (name, num_users, num_datasets, price_usd)
VALUES ('Free plan', 0, 0, 0.0),
       ('Unlimited', 999999999, 999999999, 0.0);
