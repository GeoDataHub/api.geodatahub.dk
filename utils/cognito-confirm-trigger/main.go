package main

// The solution was found here: https://github.com/aws/aws-lambda-go/issues/44

import (
	"context"
	"fmt"
	"os"
	"errors"
	"strings"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/satori/go.uuid"

	"gitlab.com/api.geodatahub.dk/common"
)

type CognitoEventRequest struct {
	DatasetName    string                          `json:"datasetName"`
	DatasetRecords map[string]CognitoDatasetRecord `json:"datasetRecords"`
	TriggerSource  string                          `json:"triggerSource"`
	IdentityID     string                          `json:"identityId"`
	UserPoolID     string                          `json:"userPoolId"`
	Region         string                          `json:"region"`
	Version        string                          `json:"version"`
	Request        interface{}                     `json:"request"`
	Username       string                          `json:"userName"`
}

type CognitoDatasetRecord struct {
	NewValue string `json:"newValue"`
	OldValue string `json:"oldValue"`
	Op       string `json:"op"`
}

type CognitoEventResponse struct {
	TriggerSource string   `json:"triggerSource"`
	Username      string   `json:"userName"`
	UserPoolID    string   `json:"userPoolId"`
	Region        string   `json:"region"`
	Version       int      `json:"version"`
	Response      struct{} `json:"response"`
}

func Handler(ctx context.Context, cognitoEvent CognitoEventRequest) (interface{}, error) {
	// Validate input
	if cognitoEvent.Request == nil {
		return nil, errors.New("Request was empty.")
	}
	// Convert AWS interface into userAttribute
	request := cognitoEvent.Request.(map[string]interface{})
	if request["userAttributes"] == nil {
		return nil, fmt.Errorf("userAttributes was not part of the request. Received: %+v", request)
	}
	userAttributes := request["userAttributes"].(map[string]interface{})
	if userAttributes["email"] == nil || !strings.Contains(userAttributes["email"].(string), "@") {
		return nil, fmt.Errorf("Email is missing or has a wrong format. Received: %s", userAttributes["email"].(string))
	}

	if cognitoEvent.Username == "" {
		return nil, fmt.Errorf("Username is missing. Received: %s", cognitoEvent.Username)
	}

	// Connect to database as geodatahub user
	_, err := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub",
		"geodatahub",
		os.Getenv("DBNONADMPASSWORD"))
	db := common.GetStandardDB()
	defer db.Close()

	if err != nil && db != nil {
		return nil, err
	}
	fmt.Printf("Success: Connected to database")

	for datasetName, datasetRecord := range cognitoEvent.DatasetRecords {
		fmt.Printf("[%s] %s -> %s -> %s \n",
			datasetName,
			datasetRecord.OldValue,
			datasetRecord.Op,
			datasetRecord.NewValue)
	}

	fmt.Printf("\nEmail: %s", userAttributes["email"])
	fmt.Printf("\nUsername: %s", cognitoEvent.Username)

	user_uuid, err := uuid.FromString(cognitoEvent.Username)
	if err != nil {
		return nil, fmt.Errorf("Username was not a valid UUID. Received: %s", cognitoEvent.Username)
	}

	// Create user object
	new_usr := common.User{
		ID:        user_uuid,
		Email:     userAttributes["email"].(string),
	}

	_, err = common.CreateUser(db, new_usr)
	if err != nil {
		return nil, err
	}

	res := CognitoEventResponse{
		Version: 1,
		TriggerSource: cognitoEvent.TriggerSource,
		Region: cognitoEvent.Region,
		UserPoolID: cognitoEvent.UserPoolID,
		Username: cognitoEvent.Username,
	}
	return res, nil
}

func main() {
	lambda.Start(Handler)
}
