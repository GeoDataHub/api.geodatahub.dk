package main

import (
	"os"
	"fmt"
	"context"
	"testing"
	"errors"

	"gitlab.com/api.geodatahub.dk/common"
	"gitlab.com/api.geodatahub.dk/tests"
	require "github.com/stretchr/testify/require"
)


func TestMain(m *testing.M) {
	// Setup database tables
	tests.MigrateDirection("up", "../migrate")

	// Make initial connection
	_, err := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))

	if err != nil {
		fmt.Print("Unable to connect to database using standard lib")
	}
	db := common.GetStandardDB()

	if _, err := db.Exec("DELETE FROM users;"); err != nil {
		fmt.Printf("Unable to delete all users")
	}

	// Run tests
	exitcode := m.Run()

	// Remove all tables once test are done
	tests.MigrateDirection("down", "../migrate")

	// Run all test then Exit
	os.Exit(exitcode)
}

// Test that sending the correct information to the handler works
func TestHandler(t *testing.T) {

	var testcases = []struct {
		// input
		name           string
		request        interface{}
		username       string

		// expected
		returnValue    interface{}
		returnError    error
	}{
		{
			"Empty request",
			nil,
			"",
			nil,
			errors.New("Request was empty."),
		},
		{
			"Empty request param",
			map[string]interface{}{},
			"",
			nil,
			errors.New("userAttributes was not part of the request. Received: map[]"),
		},
		{
			"Misformatted email",
			map[string]interface{}{
				"userAttributes": map[string]interface{}{
					"email": "me",
				},
			},
			"",
			nil,
			errors.New("Email is missing or has a wrong format. Received: me"),
		},
		{
			"Missing username",
			map[string]interface{}{
				"userAttributes": map[string]interface{}{
					"email": "me@hello.com",
				},
			},
			"",
			nil,
			errors.New("Username is missing. Received: "),
		},
		{
			"Username not uuidv4",
			map[string]interface{}{
				"userAttributes": map[string]interface{}{
					"email": "me@hello.com",
				},
			},
			"7495b1d2-eff4-8e52-14660cb2c126",
			nil,
			errors.New("Username was not a valid UUID. Received: 7495b1d2-eff4-8e52-14660cb2c126"),
		},
		{
			"Valid request",
			map[string]interface{}{
				"userAttributes": map[string]interface{}{
					"email": "me@hello.com",
				},
			},
			"7495b1d2-eff4-4118-8e52-14660cb2c126",
			CognitoEventResponse{
				Username:"7495b1d2-eff4-4118-8e52-14660cb2c126",
				Version:1,
			},
			nil,
		},
	}

	ctx := context.Background()
	for _, testcase := range testcases {
		req := CognitoEventRequest{
			Request: testcase.request,
			Username: testcase.username,
		}
		val, err := Handler(ctx, req)
		require.Equal(t, testcase.returnError, err,
			"Subtest %s failed because errors don't match", testcase.name)
		require.Equal(t, testcase.returnValue, val,
			"Subtest %s failed because values don't match", testcase.name)
	}
}
