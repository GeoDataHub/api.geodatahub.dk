package main

import (
	"log"
	"os"

	"gitlab.com/api.geodatahub.dk/common"
)

func main() {
	db, err := common.Init(
		os.Getenv("DBHOST"),
		"postgres",         // database
		os.Getenv("DBUSR"), // user
		os.Getenv("DBPASSWORD"))
	if err != nil {
		log.Fatal(err)
	} else {
		log.Print("Success: Connected to database")
	}

	// Create database user
	_, err = db.Exec("CREATE USER geodatahub WITH PASSWORD'" + os.Getenv("DBNONADMPASSWORD") + "'")
	if err != nil {
		_, err = db.Exec("ALTER USER geodatahub WITH PASSWORD '" + os.Getenv("DBNONADMPASSWORD") + "'")
		if err != nil {
			log.Print("Unable to change user password")
		} else {
			log.Print("Set password for geodatahub user")
		}
	} else {
		log.Print("Success: Created user")
	}

	// Create database
	_, err = db.Exec("CREATE DATABASE geodatahub")
	if err != nil {
		log.Print("Failure: Unable to create database ", err)
	} else {
		log.Print("Success: Created database")
	}

	// Set permissions
	_, err = db.Exec("GRANT CONNECT ON DATABASE geodatahub TO geodatahub;")
	if err != nil {
		log.Print("Failure: To enable connect on database ", err)
	}
	db.Close()

	// Connect as postgres to new database
	db, err = common.Init(
		os.Getenv("DBHOST"),
		"geodatahub",       // database
		os.Getenv("DBUSR"), // user
		os.Getenv("DBPASSWORD"))
	if err != nil {
		log.Fatal(err)
	} else {
		log.Print("Success: Connected to database")
	}

	// Enable postgis
	db.Exec("CREATE EXTENSION IF NOT EXISTS postgis")
	log.Print("Activated PostGIS!")

	db.Exec("CREATE EXTENSION IF NOT EXISTS ltree")
	log.Print("Activated ltree")

	db.Exec("ALTER TABLE spatial_ref_sys OWNER TO geodatahub")

	db.Exec("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\" WITH SCHEMA public")
	log.Print("Activated uuid-ossp!")

	db.Close()

	// Connect as the new user
	db, err = common.Init(
		os.Getenv("DBHOST"),
		"geodatahub",
		"geodatahub",
		os.Getenv("DBNONADMPASSWORD"))
	if err != nil {
		log.Fatal(err)
	} else {
		log.Print("Success: Connected to database")
	}
}
