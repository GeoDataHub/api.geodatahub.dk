package main

import (
	"testing"

	"gitlab.com/api.geodatahub.dk/tests"
)

func TestMainFunc(t *testing.T) {
	// Setup database tables
	tests.MigrateDirection("up", "../migrate")
	defer tests.MigrateDirection("down", "../migrate")
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	handler()
}
