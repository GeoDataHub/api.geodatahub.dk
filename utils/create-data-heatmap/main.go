package main

import (
	"bytes"
	"log"
	"os"

	"gitlab.com/api.geodatahub.dk/dataset"
	"gitlab.com/api.geodatahub.dk/common"

	"github.com/satori/go.uuid"
	"github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/service/s3"
)

type heatmap struct {
	weight int
	coordinates []int
}

func handler() {
	S3Region  := os.Getenv("S3REGION")
	S3Bucket  := os.Getenv("S3BUCKET")

	_, err := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub",         // database
		os.Getenv("DBUSR"), // user
		os.Getenv("DBPASSWORD"))
	if err != nil {
		log.Fatal(err)
	} else {
		log.Print("Success: Connected to database")
	}

	roles := []uuid.UUID{common.PublicOrganization}
	heatmaps := dataset.GenerateDatasetHeatmap(roles)

	log.Printf("Finished reading from database. Creating session.")

	// Upload result to S3
	//
	// Create a single AWS session (we can re use this if we're uploading many files)
	// To run locally add, Profile: "Dev",
    s, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: aws.String(S3Region),
			S3ForcePathStyle: aws.Bool(true),
		},
	})
    if err != nil {
        log.Fatal(err)
    }

	size := int64(len(heatmaps))

    // Config settings: this is where you choose the bucket, filename, content-type etc.
    // of the file you're uploading.
	if S3Region != "" && S3Bucket != "" {
		log.Printf("Uploading to S3 now.")
		_, err = s3.New(s).PutObject(&s3.PutObjectInput{
			Bucket:               aws.String(S3Bucket),
			Key:                  aws.String("data-density.json"),
			ACL:                  aws.String("public-read"),
			Body:                 bytes.NewReader(heatmaps),
			ContentLength:        aws.Int64(size),
			ContentType:          aws.String("application/json"),
			ContentDisposition:   aws.String("attachment"),
			ServerSideEncryption: aws.String("AES256"),
		})
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Printf("Missing S3Region and/or S3Bucket. Skiping upload!")
	}

	log.Printf("All done")
}

func main() {
	handler()
	os.Exit(0)
}
