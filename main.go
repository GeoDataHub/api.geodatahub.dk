package main

import (
	"log"
	"os"
	"strconv"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/awslabs/aws-lambda-go-api-proxy/gin"
	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"

	// GIS import to be used later
	//	"github.com/twpayne/go-geom"
	//	"github.com/twpayne/go-geom/encoding/ewkb"
	//	"github.com/paulmach/go.geo"

	"gitlab.com/api.geodatahub.dk/common"
	"gitlab.com/api.geodatahub.dk/dataset"
	"gitlab.com/api.geodatahub.dk/schemas"
)

var err error
var initialized = false
var ginLambda *ginadapter.GinLambda
var doLambda bool

func handleGin() (ginLambda *ginadapter.GinLambda, ginEngine *gin.Engine) {
	defer common.TimeTrack(time.Now(), "handleGin")

	// stdout and stderr are sent to AWS CloudWatch Logs
	r := gin.Default()

	r.Use(common.ValidateUser())
	r.Use(gzip.Gzip(gzip.DefaultCompression))
	r.Use(common.AddCORSHeaders())

	r.GET("/datasets/:id", dataset.GetDatasetByIDRoute)

	r.POST("/datasets/:id/publicize", func(c *gin.Context) {
		c.Set("public", "true")
		dataset.HandlePublicizeOrShareDatasetByIDRoute(c)
	})
	r.DELETE("/datasets/:id/publicize", func(c *gin.Context) {
		c.Set("private", "true")
		dataset.HandlePublicizeOrShareDatasetByIDRoute(c)
	})

	r.POST("/datasets/:id/share", func (c *gin.Context) {
		usr, exists := c.GetQuery("user")
		if !exists {
			c.AbortWithStatusJSON(400, gin.H{"error": "missing user to share dataset with"}); return
		}
		c.Set("addUser", usr)
		dataset.HandlePublicizeOrShareDatasetByIDRoute(c)
	})
	r.DELETE("/datasets/:id/share", func (c *gin.Context) {
		usr, exists := c.GetQuery("user")
		if !exists {
			c.AbortWithStatusJSON(400, gin.H{"error": "missing user to share dataset with"}); return
		}
		c.Set("delUser", usr)
		dataset.HandlePublicizeOrShareDatasetByIDRoute(c)
	})

	r.DELETE("/datasets/:id", dataset.DeleteDatasetByIDRoute)
	r.PUT("/datasets/:id", dataset.UpdateEntireDatasetRoute)
	r.PATCH("/datasets/:id", dataset.UpdateDatasetSingleParamRoute)
	r.OPTIONS("/datasets/:id", common.Return200)

	// r.POST("/datasets/:id/share", common.Return200)

	r.GET("/datasets", dataset.GetAndSearchDatasetRoute)
	r.POST("/datasets", dataset.CreateDatasetRoute)
	r.OPTIONS("/datasets", common.Return200)

	r.GET("/datasets-heatmap", dataset.GenerateUsersDataHeatmapRoute)

	// Schemas endpoints
	r.GET("/schemas/autocomplete", schemas.GetSchemaAutocomplete)
	r.OPTIONS("/schemas/autocomplete", common.Return200)

	// User endpoints
	r.GET("/users/:id", common.GetUserByIDRoute)
	r.GET("/users/:id/organizations", common.GetUserOrganizationsRoute) // Returns all organizations the user has access to
	r.GET("users/", common.GetUserByIDRoute) // Returns the user that makes the call

	// Organization endpoints
	r.POST("/organizations/", common.CreateOrganizationRoute)
	r.POST("/organizations/:orgId/users", common.InviteUserToOrganizationRoute)

	// Helper endpoints
	r.GET("/ping", common.Ping)
	r.OPTIONS("/ping", common.Return200)

	if doLambda {
		ginLambda = ginadapter.New(r)
		return ginLambda, nil
	} else {
		return nil, r
	}
}

func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	if !initialized {
		ginLambda, _ = handleGin()
		initialized = true
	}

	// If no name is provided in the HTTP request body, throw an error
	return ginLambda.Proxy(req)
}

func main() {
	log.Print("Started")
	doLambda, err = strconv.ParseBool(os.Getenv("DO_LAMBDA"))

	db, err := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()

	log.Print("Connected to database")
	if err != nil {
		log.Print("Unable to connect to database")
	}
	log.Print("Ready to continue")

	if doLambda {
		lambda.Start(Handler)
	} else {
		// Ignore return values
		_, r := handleGin()
		r.Run(":8081")
	}
}
