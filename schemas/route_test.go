package schemas

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"os"
	"fmt"
	require "github.com/stretchr/testify/require"
	gin "github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"

	"gitlab.com/api.geodatahub.dk/common"
	"gitlab.com/api.geodatahub.dk/tests"
)

func TestMain(m *testing.M) {
	// Setup database tables
	tests.MigrateDirection("up", "../utils/migrate")

	// Make initial connection
	_, err := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))

	if err != nil {
		fmt.Print("Unable to connect to database using standard lib")
	}
	// Run tests
	exitcode := m.Run()

	// Remove all tables once test are done
	tests.MigrateDirection("down", "../utils/migrate")

	// Run all test then Exit
	os.Exit(exitcode)
}

//TestGetSchemaAutocomplete This tests ensures calling the route works as expected.
//The actual values returned are tested in the model_test.go file.
func TestGetSchemaAutocompleteRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	DB := common.GetStandardDB()
	var realUUID string
	err := DB.QueryRow("SELECT id FROM users WHERE email = 'jane@fakegeocorp.com'").Scan(&realUUID)
	if err != nil {
		t.Fatalf("Unable to find user janedoe for testing: %s", err)
	}
	t.Logf("Testing user UUID: %s", realUUID)

	var testcases = []struct {
		name           string
		schema         string
		key            string
		requestingUser string

		// Expected
		code           int
		response       string
	}{
		{
			"Empty anonymous request",
			"",
			"",
			uuid.Nil.String(),
			400,
			"Invalid values",
		},
		{
			"Anonymous non-existing request",
			"https://schemas.geodatahub.dk/empty.json",
			"Does not exist",
			uuid.Nil.String(),
			200,
			"",
		},
		{
			"Unknown user with non-existing keys",
			"https://schemas.geodatahub.dk/empty.json",
			"Does not exist",
			"614fe69b-2f42-4cd1-a827-aa7c53ef16a8", // Random user id
			500,
			"Your user ID is not registered",
		},
		{
			"Known user with non-existing keys",
			"https://schemas.geodatahub.dk/empty.json",
			"Does not exist",
			realUUID,
			200,
			"[",
		},
		{
			"Known user with autocomplete options",
			"https://schemas.geodatahub.dk/empty.json",
			"driller name",
			realUUID,
			200,
			"Jane",
		},
	}

	for _, testcase := range testcases {
		writer := httptest.NewRecorder()
		c, _ := gin.CreateTestContext(writer)

		req, _ := http.NewRequest("GET", "/schemas/autocomplete", nil)

		q := req.URL.Query()
		q.Add("schema", testcase.schema)
		q.Add("key", testcase.key)
		req.URL.RawQuery = q.Encode()

		c.Request = req
		c.Set("UserUUID", testcase.requestingUser)
		GetSchemaAutocomplete(c)

		require.Equal(t, testcase.code, c.Writer.Status(), "Case %s: Invalid code returned - API resp: %s", testcase.name, writer.Body.String())
		require.Contains(t, writer.Body.String(), testcase.response, "Case %s: Unexpected response", testcase.name)
	}
}
