package schemas

import (
	"log"
	"fmt"

	"gitlab.com/api.geodatahub.dk/common"
	"github.com/gin-gonic/gin"
)

// GetSchemaAutocomplete returns list of auto-complete options for the schemas key
func GetSchemaAutocomplete(c *gin.Context) {
	requestingUser, err := common.GetRequestingUser(c, false)
	if err != nil {
		// The GIN error is contained in the context
		return
	}

	// Validate input
	schema := c.Query("schema")
	key := c.Query("key")
	if schema == "" || key == "" {
		log.Printf("Schema (%s) or schema key (%s) was not found in URL", schema, key)
		c.AbortWithStatusJSON(400, gin.H{"error": "Invalid values for parameters key and/or schema"}); return
	}

	// Get users roles to know which dataset are allowed to return
	roles, err := requestingUser.GetRoles()
	if err != nil {
		c.AbortWithStatusJSON(500, err.Error()); return
	}

	// Include public datasets in autocomplete
	roles = append(roles, common.PublicOrganization)

	options, err := GetSchemaKeyOptions(schema, key, roles)
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"error": fmt.Sprintf("Unable to retrieve auto-complete options for key=%s and schema=%s", key, schema)}); return
	}

	if len(options) > 0 {
		c.JSON(200, options)
	} else {
		c.JSON(200, `[]`)
	}
}
