package schemas

import (
	"fmt"
	"gitlab.com/api.geodatahub.dk/common"
	"github.com/satori/go.uuid"
	pg "github.com/lib/pq"
)

// GetSchemaKeyOptions Get auto-complete options for schema key. Only options that are valid
// to the users current access level (given roles) are included in the response.
func GetSchemaKeyOptions(schema string, key string, roles []uuid.UUID) ([]string, error) {
	var dataset []string

	fmt.Printf("User requested information on schema %s with key %s\n", schema, key)

	// Get distinct auto-complete options for key
	db := common.GetStandardDB()
	rows, err := db.Query(`
      SELECT DISTINCT distribution_info->>$1 as value
      FROM datasets
      WHERE datatype = $2
      AND distribution_info ? $1 -- Remove any rows that doesn't contain the key
      AND roles && $3 -- Only include roles the user belongs to
    `, key, schema, pg.Array(roles))
	if err != nil {
		return dataset, common.ReportInternalError(err)
	}
	defer rows.Close()

	var option string
	for rows.Next() {
		err := rows.Scan(&option)
		if err != nil {
			return dataset, common.ReportInternalError(err)
		}

		if option != "" {
			dataset = append(dataset, option)
		}
	}
	err = rows.Err()
	if err != nil {
		common.ReportInternalError(err)
	}

	return dataset, nil
}
