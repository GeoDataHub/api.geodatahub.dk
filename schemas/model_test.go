package schemas

import (
	"testing"
	require "github.com/stretchr/testify/require"
	"github.com/satori/go.uuid"

	"gitlab.com/api.geodatahub.dk/common"
	"gitlab.com/api.geodatahub.dk/tests"
)


// Test schema autocomplte logic
func TestGetSchemaAutocomplete(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	// Ensure a DB connection is active
	DB := common.GetStandardDB()
	var geoCorpRWRole uuid.UUID
	if err := DB.QueryRow("SELECT id FROM roles WHERE name = 'Fake Geocorp writers'").Scan(&geoCorpRWRole); err != nil {
		t.Fatalf("Unable to read roles from DB: %s", err.Error())
	}

	var empty []string
	var testcases = []struct {
		name           string
		schema         string
		key            string
		roles          []uuid.UUID

		// Expected
		result         []string
		resultLen      int
	}{
		{
			"Empty Key",
			"https://schemas.geodatahub.dk/empty.json",
			"Does not exist",
			[]uuid.UUID{},
			empty,
			0,
		},
		{
			"Unknown schema",
			"https://schemas.geodatahub.dk/not-empty.json",
			"Metadata",
			[]uuid.UUID{},
			empty,
			0,
		},
		{
			"User belongs to no roles",
			"https://schemas.geodatahub.dk/empty.json",
			"Metadata",
			[]uuid.UUID{},
			empty,
			0,
		},
		// Permission checks
		{
			"Only public datasets",
			"https://schemas.geodatahub.dk/test_unique_constraint.json",
			"bar",
			[]uuid.UUID{common.PublicOrganization},
			[]string{"40"},
			1,
		},
		{
			"Private dataset - Not Jane asking",
			"https://schemas.geodatahub.dk/test.json",
			"janes parameter",
			[]uuid.UUID{geoCorpRWRole},
			empty,
			0,
		},
		{
			"Private dataset - Jane asking",
			"https://schemas.geodatahub.dk/test.json",
			"janes parameter",
			[]uuid.UUID{uuid.FromStringOrNil("00000000-0000-0000-0000-000000000001")},
			[]string{"private"},
			1,
		},
		{
			"Shared dataset",
			"https://schemas.geodatahub.dk/empty.json",
			"driller name",
			[]uuid.UUID{uuid.FromStringOrNil("00000000-0000-0000-0000-000000000003")},
			[]string{"Jane doe"},
			1,
		},
		// Input checks
		{
			"Key without space",
			"https://schemas.geodatahub.dk/empty.json",
			"Metadata",
			[]uuid.UUID{geoCorpRWRole},
			[]string{"One"},
			1,
		},
		{
			// There exists two datasets with empty.json and driller name but only
			// one is selected since the other dataset belongs in the Fake Geocorp - Driller
			// org
			"Key with space",
			"https://schemas.geodatahub.dk/empty.json",
			"driller name",
			[]uuid.UUID{geoCorpRWRole},
			[]string{"John doe"},
			1,
		},
		{
			// The ...003 user has dataset 10...05 shared with him.
			"Query using multiple roles",
			"https://schemas.geodatahub.dk/empty.json",
			"driller name",
			[]uuid.UUID{geoCorpRWRole, uuid.FromStringOrNil("00000000-0000-0000-0000-000000000003")},
			[]string{"Jane doe", "John doe"},
			2,
		},
	}

	for _, testcase := range testcases {
		res, err := GetSchemaKeyOptions(testcase.schema, testcase.key, testcase.roles)
		require.Nil(t, err, "Test failed with error ", err)
		require.Equal(t, testcase.result, res, "Case %s: Expected results to match", testcase.name)
		require.Len(t, res, testcase.resultLen, "Case %s: Expected results to have lenght", testcase.name)
	}
}
