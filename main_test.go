package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"database/sql"
	"os"
	"regexp"
	"testing"

	pg "github.com/lib/pq"
	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/api.geodatahub.dk/common"
	"gitlab.com/api.geodatahub.dk/dataset"
	"gitlab.com/api.geodatahub.dk/tests"

)

var (
	testdata    []dataset.Dataset
	testdataids []uuid.UUID
	testcontext common.AWSContext
)

func generateHeader() string {
	jstr, _ := json.Marshal(testcontext)
	return string(jstr)
}

func loadFixtures() {
	file, _ := ioutil.ReadFile("fixtures/geodata.json")
	if err := json.Unmarshal(file, &testdata); err != nil {
		fmt.Println(err)
	}
}

func connectToDB() *sql.DB {
	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))

	return db
}

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)

	connectToDB()

	// Setup database tables
	tests.MigrateDirection("up", "utils/migrate")

	// Load test datasets from file
	loadFixtures()

	// Fixture of an example user
	testcontext = common.AWSContext{
		AccountId: "1234",
		Authorizer: common.AWSAuthorizer{
			Claims: common.AWSClaims{
				At_hash:  "foobar",
				Username: "2cf38c6f-6821-438f-b809-90db23751fa7",
				Email:    "John@doe.com",
			},
		},
	}

	// Run tests
	exitcode := m.Run()

	// clean up database
	tests.MigrateDirection("down", "utils/migrate")

	// Run all test then Exit
	os.Exit(exitcode)
}

func TestPingRoute(t *testing.T) {
	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()

	_, router := handleGin()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)

	req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
	router.ServeHTTP(w, req)

	require.Equal(t, 200, w.Code)
	require.Equal(t, "pong", w.Body.String())
}

func TestPublicizeRoute(t *testing.T) {
	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()

	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")
	fakeAdmUser, _, _ := tests.GetFakeGeocorpUsers()
	testcontext.Authorizer.Claims.Username = fakeAdmUser

	_, router := handleGin()

	var testcases = []struct {
		name    string
		dataUID string
		method  string
		code    int
	}{
		{
			"Unknown dataset",
			uuid.NewV4().String(),
			"POST",
			404,
		},
		{
			"Publicize dataset",
			"10000000-0000-0000-0000-000000000001",
			"POST",
			200,
		},

	}

	for _, testcase := range testcases {
		w := httptest.NewRecorder()
		URL := fmt.Sprintf("/datasets/%s/publicize", testcase.dataUID)
		req, _ := http.NewRequest(testcase.method, URL, nil)
		req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
		router.ServeHTTP(w, req)

		require.Equal(t, testcase.code, w.Code)
	}
}

func TestDatasetValidation(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")
	fakeAdmUser, _, fakeGeoCorpOrg := tests.GetFakeGeocorpUsers()
	orgID, _ := uuid.FromString(fakeGeoCorpOrg)
	testcontext.Authorizer.Claims.Username = fakeAdmUser

	g := dataset.NewPointGeometry([]float64{101.0, 0.0})
	var testcases = []struct {
		name string
		data dataset.Dataset
		code int
		msg  string
	}{
		{
			"Empty Dataset",
			dataset.Dataset{},
			400,
			"Error",
		},
		{
			"Only description",
			dataset.Dataset{
				Description: "My borehole",
			},
			400,
			"Error",
		},
		{
			"Only geojson",
			dataset.Dataset{
				ProjectedGeometry: *g,
			},
			400,
			"Error",
		},
		{
			"Only description, Datatype",
			dataset.Dataset{
				Description: "My borehole",
				Datatype:    "https://schemas.geodatahub.dk/empty.json",
			},
			400,
			"Error",
		},
		{
			"Only description, Datatype",
			dataset.Dataset{
				Description: "My borehole",
				Datatype:    "https://schemas.geodatahub.dk/doesnotexist.json",
			},
			400,
			"Error",
		},
		{
			"Description as json",
			dataset.Dataset{
				Description:       "\"Hello\":\"world\"",
				Datatype:          "https://schemas.geodatahub.dk/empty.json",
				ProjectedGeometry: *g,
				Result:            common.JSONB{},
				DistributionInfo:  common.JSONB{"hello":"boy"},
				Organization: orgID,
			},
			200,
			"id\"",
		},
	}

	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()

	_, router := handleGin()

	for _, tt := range testcases {
		t.Log(tt.name)
		new_dataset, err := json.Marshal(tt.data)
		if err != nil {
			t.Errorf("%s - %s", tt.name, err.Error())
		}

		w := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/datasets", bytes.NewBuffer(new_dataset))
		req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
		router.ServeHTTP(w, req)

		// Validate response - Response should contain UUIDv4
		require.Equal(t, tt.code, w.Code)
		require.Contains(t, w.Body.String(), tt.msg)
	}
}

func TestCreateDatasetRouteWithoutOrg(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")
	fakeAdmUser, _, _ := tests.GetFakeGeocorpUsers()
	testcontext.Authorizer.Claims.Username = fakeAdmUser

	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()
	standardDB := common.GetStandardDB()

	_, router := handleGin()

	// Create payload
	for _, tt := range testdata {
		// Remove organization ownership
		tt.Organization, err = uuid.FromString("")

		new_dataset, _ := json.Marshal(tt)

		w := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/datasets", bytes.NewBuffer(new_dataset))
		req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
		router.ServeHTTP(w, req)

		// Validate response - Response should contain UUIDv4
		if w.Code != 200 {
			fmt.Println(w.Body.String())
		}
		require.Equal(t, 200, w.Code)

		// Process the JSON encoded response
		var raw_resp map[string]interface{}
		json.Unmarshal(w.Body.Bytes(), &raw_resp)
		_, ok := raw_resp["id"]
		require.Equal(t, true, ok)

		r, _ := regexp.Compile("(\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12})")
		require.Equal(t, r.MatchString(w.Body.String()), true)

		// Validate dataset
		uid, _ := uuid.FromString(r.FindString(w.Body.String()))
		dset, _ := dataset.GetDatasetByID(uid)

		require.NotEqual(t, dset.Owner, tt.Owner)
		users_uuid, _ := uuid.FromString(testcontext.Authorizer.Claims.Username)
		require.Equal(t, dset.Owner, users_uuid)
		require.Equal(t, dset.Organization, tt.Organization)
		require.Equal(t, dset.Description, tt.Description)

		// Ensure the dataset is in the correct sharing state
		publicArray := []uuid.UUID{common.PublicOrganization}
		if tt.Public {
			rows, err := standardDB.Exec(
				`SELECT true
                 FROM datasets
                 WHERE identifier = $1::UUID
                 AND roles @> $2`,
				uid, pg.Array(publicArray))
			if err != nil {
				t.Logf("Query failed with %s", err)
			}
			isPublic, _ := rows.RowsAffected()
			require.NotZero(t, isPublic, "The dataset isn't public as expected")
		} else {
			rows, err := standardDB.Exec(
				`SELECT false
                 FROM datasets
                 WHERE identifier = $1::UUID
                 AND roles @> $2`,
				uid, pg.Array(publicArray))
			if err != nil {
				t.Logf("Query failed with %s", err)
			}
			isPublic, _ := rows.RowsAffected()
			require.Zero(t, isPublic, "The dataset is public -  that was not expected")
		}

		require.Equal(t, tt.DistributionInfo, dset.DistributionInfo, "Distribution info does not match for test: %s", dset.Description)
	}

	// Cleanup any leftover datasets
	_, err := standardDB.Exec(
		`DELETE
         FROM datasets
         WHERE organization IS NULL`)
	if err != nil {
		t.Logf("Query failed with %s", err)
	}

}

// TestCreateDatasetDuplicatedRoute Test that creating multiple datasets with the same data
func TestCreateDatasetDuplicatedRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")
	fakeAdmUser, _, _ := tests.GetFakeGeocorpUsers()
	testcontext.Authorizer.Claims.Username = fakeAdmUser

	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()

	_, router := handleGin()

	// Read the test JSON file to understand the different tests
	var tests = []struct {
		idEqualToPrevious  bool // Should the ID be equal to the previous id?
		databaseId string
		orgID      uuid.UUID
	}{
		{false, "", uuid.FromStringOrNil("10000000-0000-0000-0000-000000000001")},
		{true, "", uuid.FromStringOrNil("10000000-0000-0000-0000-000000000001")},
		{false, "", uuid.FromStringOrNil("10000000-0000-0000-0000-000000000001")},
		{false, "", uuid.FromStringOrNil("10000000-0000-0000-0000-000000000002")},
		{false, "", uuid.FromStringOrNil("10000000-0000-0000-0000-000000000002")},
		{true, "", uuid.FromStringOrNil("10000000-0000-0000-0000-000000000002")},
		{true, "", uuid.FromStringOrNil("10000000-0000-0000-0000-000000000002")},
		{false, "", uuid.FromStringOrNil("10000000-0000-0000-0000-000000000002")},
	}

	var uniquetestdata []dataset.Dataset
	file, _ := ioutil.ReadFile("fixtures/test_upsert_data.json")
	if err := json.Unmarshal(file, &uniquetestdata); err != nil {
		fmt.Println(err)
	}

	for testNo, testdata := range tests {
		// Create payload - Dataset 12 has an unique constraint
		uniquetestdata[testNo].Organization = tests[testNo].orgID
		newDataset, _ := json.Marshal(uniquetestdata[testNo])
		//r, _ := regexp.Compile("(\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12})")

		w1 := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/datasets", bytes.NewBuffer(newDataset))
		req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
		router.ServeHTTP(w1, req)

		// Validate response - Response should contain UUIDv4
		if w1.Code != 200 {
			fmt.Println(w1.Body.String())
		}
		require.Equal(t, 200, w1.Code)
		// Process the JSON encoded response
		var rawResp1 map[string]interface{}
		json.Unmarshal(w1.Body.Bytes(), &rawResp1)
		tests[testNo].databaseId, _ = rawResp1["id"].(string)

		//uid1, _ := uuid.FromString(r.FindString(testdata.databaseId))
		//dset1, _ := dataset.GetDatasetByID(db, uid1)

		if testdata.idEqualToPrevious && testNo > 0 {
			require.Equal(t, tests[testNo].databaseId, tests[testNo-1].databaseId)
		} else if testNo > 0 {
			require.NotEqual(t, tests[testNo].databaseId, tests[testNo-1].databaseId)

		}
	}
}

func TestGetDatasetRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	fakeAdmUser, fakeNormUser, _ := tests.GetFakeGeocorpUsers()
	anonUser := uuid.Nil.String()

	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()

	var testcases = []struct {
		name           string
		dataID         string
		rqstUser       string
		isPublic       bool
		numSharedWith  int // Number of users the dataset is shared with

		// expected
		code int
		msg  string
	}{
		{
			"Empty Dataset",
			uuid.Nil.String(),
			fakeNormUser,
			false,
			-1, // Not in reponse
			400,
			"not a valid",
		},
		{
			"Invalid dataset uuid",
			"34224-34356-ssdd",
			fakeNormUser,
			false,
			-1,
			400,
			"not a valid",
		},
		{
			"Random dataset uuid",
			uuid.NewV4().String(),
			fakeNormUser,
			false,
			-1,
			404,
			"not found",
		},
		{
			"Dataset in forbidden group",
			"10000000-0000-0000-0000-000000000002",
			"00000000-0000-0000-0000-000000000003", // Ext user
			false,
			-1,
			403,
			"not allowed",
		},
		{
			"Valid request",
			"10000000-0000-0000-0000-000000000003",
			fakeAdmUser,
			false,
			1,
			200,
			"success",
		},
		{
			"Jane can access her own datadet",
			"10000000-1000-0000-0000-000000000001",
			fakeAdmUser,
			false,
			0, // Shared with no one
			200,
			"success",
		},
		{
			"John cannot access janes private dataset",
			"10000000-1000-0000-0000-000000000001",
			fakeNormUser,
			false,
			-1,
			403,
			"not allowed",
		},
		{
			"Ext user can access a dataset shared with him",
			"10000000-0000-0000-0000-000000000003",
			"00000000-0000-0000-0000-000000000003", // Ext user
			false,
			-1, // Ext user cannot see sharing
			200,
			"success",
		},
		{
			"Ext user can access a public dataset",
			"10000000-0000-0000-0000-000000000004",
			"00000000-0000-0000-0000-000000000004", // Ext user
			true,
			-1, // Cannot see sharing
			200,
			"success",
		},
		{
			"Anonymous user access to public dataset",
			"10000000-0000-0000-0000-000000000004",
			anonUser,
			true,
			-1, // Not in reponse
			200,
			"success",
		},
		{
			"Anonymous user denied access to private dataset",
			"10000000-0000-0000-0000-000000000001",
			anonUser,
			false,
			-1, // Not in reponse
			403,
			"not allowed",
		},
		{
			"Anonymous user denied access to shared dataset",
			"10000000-0000-0000-0000-000000000003",
			anonUser,
			false,
			-1, // Not in reponse
			403,
			"not allowed",
		},
	}

	_, router := handleGin()

	for _, tt := range testcases {
		// Ensure the correct user makes the request
		testcontext.Authorizer.Claims.Username = tt.rqstUser

		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", fmt.Sprintf("/datasets/%s", tt.dataID), nil)
		req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
		router.ServeHTTP(w, req)

		require.Equal(t, tt.code, w.Code, "Case %s: API response %s", tt.name, w.Body.String())

		if tt.code == 200 {
			var dataset dataset.Dataset
			var rawInterface map[string]interface{}
			err := json.Unmarshal([]byte(w.Body.String()), &dataset)
			require.Nil(t, err, "Case %s: JSON response should marshal.", tt.name)
			err = json.Unmarshal([]byte(w.Body.String()), &rawInterface)
			require.Nil(t, err, "Case %s: JSON response should marshal.", tt.name)

			err = json.Unmarshal([]byte(w.Body.String()), &rawInterface)
			require.Nil(t, err, "Case %s: JSON response should marshal.", tt.name)

			require.Equal(t, 200, w.Code, "Case %s: API response %s", tt.name, w.Body.String())
			require.NotEqual(t, dataset.Identifier, uuid.Nil.String(), "Case %s: Should not return empty uuid", tt.name)
			require.Equal(t, tt.dataID, dataset.Identifier.String(), "Case %s: Should match uuid from test.", tt.name)
			require.NotEmpty(t, dataset.Links, "Case %s: Links should not be empty on success. Body was, \n%s", tt.name, w.Body.String())
			require.Equal(t, dataset.Owner, uuid.Nil, "Case %s: API should only return contact but raw owner was returned. Body was, \n%s", tt.name, w.Body.String())
			require.Equal(t, dataset.Organization, uuid.Nil, "Case %s: API should only return contact but raw org was returned. Body was, \n%s", tt.name, w.Body.String())
			require.Contains(t, w.Body.String(), "contact", "Case %s: API should return contact in the response. Body was, \n%s", tt.name, w.Body.String())
			require.Contains(t, w.Body.String(), common.APIBaseURL, "Case %s: API should return prod URL. Body was, \n%s", tt.name, w.Body.String())
			require.NotEmptyf(t, rawInterface["contact"], "Case %s: API should return contact in the response. Body was, \n%s", tt.name, w.Body.String())
			require.NotEmptyf(t, rawInterface["contact"].(map[string]interface{})["name"], "Case %s: API should return contact in the response. Body was, \n%s", tt.name, w.Body.String())

			//Add check for refs
			require.Equal(t, tt.isPublic, dataset.Public)
			if tt.numSharedWith < 0 {
				// Sharing should not exist in response
				require.Nil(t, dataset.Sharing, "Case %s - response should not contain sharing info", tt.name)
			} else {
				require.Len(t, dataset.Sharing, tt.numSharedWith, "Case %s - dataset not shared with the correct number of users", tt.name)
			}
		} else {
			require.Contains(t, w.Body.String(), tt.msg, "Case %s", tt.name)
			require.NotContains(t, w.Body.String(), "_links", "Case %s - should not contain _links", tt.name)
		}
	}
}

func TestDatasetGetAndSearchRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql")

	fakeAdmUser, _, fakeGeoCorpOrg := tests.GetFakeGeocorpUsers()
	orgID, _ := uuid.FromString(fakeGeoCorpOrg)
	testcontext.Authorizer.Claims.Username = fakeAdmUser

	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()

	// Prefill database
	_, router := handleGin()
	for _, tt := range testdata {
		tt.Owner = uuid.FromStringOrNil(fakeAdmUser)
		tt.Organization = orgID
		new_dataset, _ := json.Marshal(tt)

		w := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/datasets", bytes.NewBuffer(new_dataset))
		req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
		router.ServeHTTP(w, req)

		// Validate response - Response should contain UUIDv4
		if w.Code != 200 {
			panic(w.Body.String())
		}
		require.Equal(t, 200, w.Code)
	}

	// Test calling without any arguments isn't allowed as it will
	// return too many results
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/datasets", nil)
	req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
	router.ServeHTTP(w, req)

	require.Equal(t, 400, w.Code)
	require.Contains(t, w.Body.String(), "query parameter missing")

	// Test supplying a parameter that should not exist
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/datasets?doesnotexist=200.2", nil)
	req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
	router.ServeHTTP(w, req)

	require.Equal(t, 400, w.Code)
	require.Contains(t, w.Body.String(), "query parameter missing")

	/// --------------------------------------------
	// ---- Tests with invalid input format
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", `/datasets?q={ "Metadata" "One"`, nil)
	req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
	router.ServeHTTP(w, req)

	require.Equal(t, 400, w.Code)
	require.Contains(t, w.Body.String(), "not valid JSON")

	// Missing schema
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", `/datasets`, nil)

	q := req.URL.Query()
	q.Add("q", "{\"Metadata\":\"One\"}")
	req.URL.RawQuery = q.Encode()

	req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
	router.ServeHTTP(w, req)

	require.Equal(t, 400, w.Code)
	// The response should contain some JSON formatted array
	require.Contains(t, w.Body.String(), "error")

	// ---------------------------------------------
	// ---- Perform schema based search         ----
	// ---------------------------------------------
	// Search with no results
	code, body, _ := buildSearchRequest("{\"https://schemas.geodatahub.dk/empty.json\":{\"NotAKey\":\"NotAValue\"}}", router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[]")

	// Get all datasets of a single schema type (here empty.json)
	code, body, jsonObj := buildSearchRequest("{\"https://schemas.geodatahub.dk/empty.json\":\"\"}", router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Equal(t, 3, len(jsonObj))

	// Get all datasets of a single schema type using {} as empty (here empty.json) - otherwise same query as above
	code, body, jsonObj = buildSearchRequest("{\"https://schemas.geodatahub.dk/empty.json\":{}}", router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Equal(t, 3, len(jsonObj))

	// Search on a single parameter
	code, body, _ = buildSearchRequest("{\"https://schemas.geodatahub.dk/empty.json\":{\"One\":\"Two\"}}", router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")

	// Search on multiple values on a single schema
	code, body, _ = buildSearchRequest("{\"https://schemas.geodatahub.dk/empty.json\":{\"One\":\"Two\", \"Metadata\":\"One\"}}", router)
	// SQL: WHERE datadate='https:...empty.json" AND distribution_info @> '{"One":"Two"} AND distribution_info @> '{"Metadata":"One}"
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")

	// Search on multiple values on multiple schemas
	query := "{\"https://schemas.geodatahub.dk/empty.json\":{\"Metadata\":\"One\", \"One\":\"Two\"}, \"https://schemas.geodatahub.dk/test.json\":{\"parameter1\":\"One\"}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Contains(t, body, "Metadata")
	require.Contains(t, body, "test.json")
	require.Contains(t, body, "empty.json")
	require.Equal(t, 2, len(jsonObj))

	// Search with > and < logical operators
	// Find the FirstTestWell dataset since it has depth = 10
	query = "{\"https://schemas.geodatahub.dk/test.json\":{\"depth\":{\"$lt\": 15}}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Contains(t, body, "FirstTestWell")
	require.Equal(t, 1, len(jsonObj))

	// Search with > and < logical operators
	// Find the FirstTestWell dataset since it has depth = 10
	// And all datasets of type schema empty.json
	query = "{\"https://schemas.geodatahub.dk/test.json\":{\"depth\":{\"$lt\": 15}}, \"https://schemas.geodatahub.dk/empty.json\":\"\"}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Contains(t, body, "FirstTestWell")
	require.Equal(t, 4, len(jsonObj))

	// Search with > and < logical operators
	// Find the FirstTestWell dataset since it has depth = 10
	// And all datasets of type schema empty.json using the {} syntax - Same query as above but different syntax
	query = "{\"https://schemas.geodatahub.dk/test.json\":{\"depth\":{\"$lt\": 15}}, \"https://schemas.geodatahub.dk/empty.json\":{}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Contains(t, body, "FirstTestWell")
	require.Equal(t, 4, len(jsonObj))

	// Same as above but in reverse order
	query = "{\"https://schemas.geodatahub.dk/empty.json\":\"\", \"https://schemas.geodatahub.dk/test.json\":{\"depth\":{\"$lt\": 15}}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Contains(t, body, "FirstTestWell")
	require.Equal(t, len(jsonObj), 4)

	// Search with > and < logical operators
	// Find the SecondTestWell dataset with depth = 20
	query = "{\"https://schemas.geodatahub.dk/test.json\":{\"depth\":{\"$gt\": 15}}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Contains(t, body, "SecondTestWell")
	require.Equal(t, 1, len(jsonObj))

	// Search with > and < logical operators
	// Find the FirstTestWell and SecondTestWell dataset
	query = "{\"https://schemas.geodatahub.dk/test.json\":{\"depth\":{\"$gt\": 0}}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "[{")
	require.Contains(t, body, "SecondTestWell")
	require.Equal(t, 2, len(jsonObj))

	//
	// Geographic tests
	//

	// Search with an invalid geometry
	query = "{\"geometry\":{\"$contains\":\"52 23\"}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 400, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "$contains only supports polygon geometries")

	// Search with an empty geom
	query = "{\"geometry\":{\"$contains\":\"POLYGON()\"}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 400, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "invalid geometry")

	// Search with a Point geometry
	query = "{\"geometry\":{\"$contains\":\"POINT(52 23)\"}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 400, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "$contains only supports polygon geometries")

	// Search with multiple parameters
	query = "{\"geometry\":{\"$contains\":\"POINT(52 23)\", \"$radius\":24}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 400, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "Only one parameter is allowed")

	// Search with a Polygon geometry with invalid points
	// return an empty search results
	// Also ensure the case is insensitive
	query = "{\"geometry\":{\"$contains\":\"POLyGoN((9999.9 9999.4,9999.1 9999.4,9999.1 9999.6,9999.9 9999.6,9999.9 9999.4))\"}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	require.Contains(t, body, "[]")

	// Search with a Polygon geometry
	query = "{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "depth")
	require.Contains(t, body, "pressure")
	require.Equal(t, 3, len(jsonObj))

	// Search with a Polygon geometry and a filter that does not match any dataset within the area
	// should produce an empty result
	query = "{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}, \"https://schemas.geodatahub.dk/test.json\": {\"depth\": {\"$lt\":1}}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Equal(t, 0, len(jsonObj))

	// Search with a Polygon geometry and a filter that matches some of the datasets within the area
	// should produce an non-empty result
	// Note: Within the polygon exist two datasets with the depth parameters (depth = 10 and 20)
	query = "{\"geometry\":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}, \"https://schemas.geodatahub.dk/test.json\": {\"depth\": {\"$lt\":15}}}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.Contains(t, body, "depth")
	require.Contains(t, body, "pressure")
	require.Equal(t, 1, len(jsonObj))

	// Advanced query test - Within the polygon exits 5 datasets with schemas - Sognefjord (2), type1 (1), type2 (2)
	// No schemas dataset has sognefjord( depth > 100 and msl < 2 ) nor type1( depth < 200 )
	// Only the two datasets of schema type2 are returned
	query = "{\"https://schemas.geodatahub.dk/sognefjord.json\": {\"depth\": {\"$gt\":100}, \"msl\": {\"$lt\":2}}, \"geometry\":{\"$contains\":\"POLYGON ((6.23748779296875 61.06160800863787, 7.22900390625 61.06160800863787, 7.22900390625 61.16046282714536, 6.23748779296875 61.16046282714536, 6.23748779296875 61.06160800863787))\"}, \"https://schemas.geodatahub.dk/type1.json\":{\"depth\": {\"$lt\": 200}}, \"https://schemas.geodatahub.dk/type2.json\":\"\"}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	// The response should contain some JSON formatted array
	require.NotContains(t, body, "depth")
	require.NotContains(t, body, "msl")
	require.Equal(t, 2, len(jsonObj))

	// Advanced query test - Similar to the test above but the schemas are not present within the area
	query = "{\"https://schemas.geodatahub.dk/test.json\": {\"depth\": {\"$lt\":15}}, \"geometry\":{\"$contains\":\"POLYGON ((6.23748779296875 61.06160800863787, 7.22900390625 61.06160800863787, 7.22900390625 61.16046282714536, 6.23748779296875 61.16046282714536, 6.23748779296875 61.06160800863787))\"}, \"https://schemas.geodatahub.dk/empty.json\":\"\"}"
	code, body, jsonObj = buildSearchRequest(query, router)
	require.Equal(t, 200, code)
	require.Equal(t, "[]", body)
    require.Equal(t, 0, len(jsonObj))
}

// Create and send search request
func buildSearchRequest(query string, router *gin.Engine) (int, string, []map[string]interface{}) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", `/datasets`, nil)
	q := req.URL.Query()
	q.Add("q", query)
	req.URL.RawQuery = q.Encode()
	req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
	router.ServeHTTP(w, req)

	// Decode JSON to a response object
	if w.Code < 300 {
		var response []map[string]interface{}
		if err := json.Unmarshal([]byte(w.Body.String()), &response); err != nil {
			panic(err)
		}
		return w.Code, w.Body.String(), response
	}

	return w.Code, w.Body.String(), nil
}

func TestDatasetDeleteRoute(t *testing.T) {
	defer tests.LoadTestData(t, "remove_geocorp_datasets.sql", "remove_geocorp_users.sql")
	tests.LoadTestData(t, "add_geocorp_users.sql", "add_geocorp_datasets.sql")

	fakeAdmUser, fakeRealUser, _ := tests.GetFakeGeocorpUsers()

	var testcases = []struct {
		name           string
		uuid           string
		rqstUser       string

		// Expected
		code           int
		msg            string
	}{
		{
			"Invalid UUID",
			"10ee64d7-8f32-5186961b1a02",
			fakeRealUser,
			400,
			"UUIDv4",
		},
		{
			"Non-existing UUID",
			"10ee64d7-b603-4862-8f32-5186961b1a02",
			fakeRealUser,
			404,
			"record not found",
		},
		{
			"Anonymous user",
			"10000000-0000-0000-0000-000000000001",
			uuid.Nil.String(),
			401,
			"unauthorized",
		},
		{
			"Valid UUID but not allowed",
			"10000000-0000-0000-0000-000000000001",
			"00000000-0000-0000-0000-000000000003", // Fake ext corp user
			403,
			"not allowed",
		},
		{
			"Valid UUID",
			"10000000-0000-0000-0000-000000000001",
			fakeRealUser,
			200,
			"OK",
		},
		{
			"Jane can delete her private dataset",
			"10000000-1000-0000-0000-000000000001",
			fakeAdmUser,
			200,
			"OK",
		},
		{
			"Ext cannot delete a shared dataset",
			"10000000-0000-0000-0000-000000000003",
			"00000000-0000-0000-0000-000000000003", // Fake ext corp user
			403,
			"not allowed",
		},

	}

	db, _ := common.Init(
		os.Getenv("DBHOST"),
		"geodatahub", // Database
		"geodatahub", // User
		os.Getenv("DBNONADMPASSWORD"))
	defer db.Close()

	_, router := handleGin()

	// Perform table-driven test
	for _, tt := range testcases {
		// Call as the correct user
		testcontext.Authorizer.Claims.Username = tt.rqstUser

		// Craft request to API
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("DELETE", fmt.Sprintf("/datasets/%s", tt.uuid), nil)
		req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
		router.ServeHTTP(w, req)

		require.Equal(t, tt.code, w.Code, tt.name)

		// Ensure the data is actually gone
		if w.Code == 200 {
			w = httptest.NewRecorder()
			req, _ = http.NewRequest("GET", fmt.Sprintf("/datasets/%s", tt.uuid), nil)
			req.Header.Set("X-Golambdaproxy-Apigw-Context", generateHeader())
			router.ServeHTTP(w, req)

			var dataset dataset.Dataset
			json.NewEncoder(w).Encode(&dataset)

			require.Equal(t, 404, w.Code, tt.name)
		}
	}

}
